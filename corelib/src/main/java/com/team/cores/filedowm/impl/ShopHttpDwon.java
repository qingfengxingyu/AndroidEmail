package com.team.cores.filedowm.impl;

import android.content.Intent;

import com.team.cores.dbmode.download.DownloadStatus;
import com.team.cores.filedowm.cores.FileTask;
import com.team.cores.filedowm.cores.ShopFileTask;
import com.team.cores.filedowm.imp.HttpDownCore;
import com.team.cores.filedowm.utils.DaoFileUtils;
import com.team.cores.utils.CloseUtils;
import com.team.cores.utils.LogTools;
import com.team.cores.utils.tools.LogTeamUtils;

import org.apache.http.util.TextUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.RandomAccessFile;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/***
 * OkHttpClient 下载实现
 */
public class ShopHttpDwon implements HttpDownCore {
    private final static String TAG = "ShopHttpDwon_xmf";
    private RandomAccessFile inputFile;//断点写入文件
    private InputStream inputStream = null;
    private BufferedInputStream bis = null;
    private int UPDATE_SIZE = 50 * 1024; // The database is updated once every
    private long oldTime;
    final int maxResumNum = 3;
    int nowNum = 0;
    private float oldPercent = -1;
    private boolean downFlag;

    @Override
    public void httpDown(OkHttpClient httpClient, ShopFileTask mShopFileTask) {

        try {
            if (!downFlag) {
                return;
            }
            File outputFile = new File(mShopFileTask.getmBean().getSaveDirPath());
            if (!outputFile.exists()) {
                outputFile.createNewFile();
            }
            inputFile = new RandomAccessFile(outputFile, "rwd");
            nowNum++;
            if (nowNum > maxResumNum) {
                mShopFileTask.setErroCode(DownloadStatus.DOWNLOAD_ERROR_CODE_1);
                mShopFileTask.setErroMsg("服务器出错");
                mShopFileTask.getmBean().setDownloadStatus(DownloadStatus.DOWNLOAD_STATUS_ERROR);
                nowNum = 0;
                return;
            }
            Request request = null;
            request = new Request.Builder()
                    .url(mShopFileTask.getmBean().getUrl())
                    .header("Range", "bytes=" + mShopFileTask.getmBean().getCompletedSize() + "-")
                    .build();

            LogTeamUtils.e(TAG, "文件" + mShopFileTask.getmBean().getAppName() == null ? "" : mShopFileTask.getmBean().getAppName() + "开始下载___下载地址___:" + mShopFileTask.getmBean().getUrl());
            Response response = null;
            response = httpClient.newCall(request).execute();
            mShopFileTask.setErroCode(response.code());
            if (response != null) {
                LogTeamUtils.e(TAG, "response.isSuccessful():" + response.isSuccessful());
            } else {
                LogTeamUtils.e(TAG, "response is NUll:");
            }
            LogTeamUtils.e(TAG, "获取响应码 code is:" + mShopFileTask.getErroCode());
            if (response != null && response.isSuccessful() && (200 == mShopFileTask.getErroCode() || 206 == mShopFileTask.getErroCode())) {
                ResponseBody responseBody = response.body();
                if (responseBody != null) {
                    if (mShopFileTask.getmBean().getToolSize() <= 0 || mShopFileTask.getmBean().getToolSize() < responseBody.contentLength()) {
                        LogTeamUtils.e(TAG, "文件" + mShopFileTask.getmBean().getAppName() == null ? "" : mShopFileTask.getmBean().getAppName() + "总长度:" + responseBody.contentLength());
                        mShopFileTask.getmBean().setToolSize(responseBody.contentLength());
                        DaoFileUtils.getInstance().addDB(mShopFileTask.getmBean());
                    }
                    LogTeamUtils.e(TAG, "toolSize is :" + mShopFileTask.getmBean().getToolSize());
                    if (TextUtils.isEmpty(response.header("Content-Range"))) {
                        LogTeamUtils.e(TAG, "Content-Range is null.服务器不支持断点下载");
                        // 返回的没有Content-Range 不支持断点下载 需要重新下载
                        File alreadyDownloadedFile = new File(mShopFileTask.getmBean().getSaveDirPath());
                        if (alreadyDownloadedFile.exists()) {
                            alreadyDownloadedFile.delete();
                        }
                        inputFile = new RandomAccessFile(mShopFileTask.getmBean().getSaveDirPath(),
                                "rwd");
                        mShopFileTask.getmBean().setCompletedSize(0L);
                        DaoFileUtils.getInstance().addDB(mShopFileTask.getmBean());
                    }
                    inputFile.seek(mShopFileTask.getmBean().getCompletedSize());
                    inputStream = responseBody.byteStream();
                    bis = new BufferedInputStream(inputStream);
                    byte[] buffer = new byte[2 * 1024];
                    int length = 0;
                    int buffOffset = 0;// 当次下载的大小
                    long toalDownSize = 0;
                    oldTime = System.currentTimeMillis();
                    // 开始时间，放在循环外，求解的usedTime就是总时间
                    long startTime = System.currentTimeMillis();
                    LogTeamUtils.d(startTime);
                    while ((length = bis.read(buffer)) > 0
                            && mShopFileTask.getmBean().getDownloadStatus() == DownloadStatus.DOWNLOAD_STATUS_DOWNLOADING && downFlag) {
                        inputFile.write(buffer, 0, length);
                        mShopFileTask.getmBean().setCompletedSize(mShopFileTask.getmBean().getCompletedSize() + length);
                        DaoFileUtils.getInstance().addDB(mShopFileTask.getmBean());
                        buffOffset += length;
                        toalDownSize += length;
                        if (buffOffset >= UPDATE_SIZE) {
                            buffOffset = 0;
                        }
                        long usedTime = (int) ((System.currentTimeMillis() - startTime) / 1000);
                        if (usedTime < 1)
                            usedTime = 1;
                        LogTeamUtils.d("字节数:" + toalDownSize + ",时间" + usedTime);
                        mShopFileTask.setDownloadSpeed((int) (toalDownSize / usedTime));
                        if ((System.currentTimeMillis() - oldTime) >= 500) {//3秒更新一次UI
//                            LogTeamUtils.e(TAG, "inputFile.length() is:" + inputFile.length() + "_________" + Thread.currentThread().getName() + "___________" + mShopFileTask.getmBean().getCompletedSize());
                            oldTime = System.currentTimeMillis();
                            if (oldPercent != mShopFileTask.getPercent()) {
                                oldPercent = mShopFileTask.getPercent();
                                mShopFileTask.onDownloading();
                            }
                        }
                    }
                    if (mShopFileTask.getmBean().getCompletedSize() == mShopFileTask.getmBean().getToolSize()) {
                        mShopFileTask.onDownloading();
                    }
                } else {
                    LogTeamUtils.e(TAG, "responseBody is null");
                }
            } else {
                //解析请求判断是否是会话超时
                ResponseBody responseBody = response.body();
                if (responseBody != null) {
                    inputStream = responseBody.byteStream();
                    bis = new BufferedInputStream(inputStream);
                    String erroMsg = new String(DaoFileUtils.getInstance().readInputStream(bis), "UTF-8") + ".";
                    mShopFileTask.setErroMsg("服务器错误:" + erroMsg);
                    LogTeamUtils.e(TAG, erroMsg);
                } else {
                    mShopFileTask.setErroMsg("服务器未知错误");
                }
                mShopFileTask.setErroCode(response.code());
                mShopFileTask.getmBean().setDownloadStatus(DownloadStatus.DOWNLOAD_STATUS_ERROR);
            }
        } catch (Exception e) {
            LogTeamUtils.e(TAG, e.toString());
            String msg = e.toString();
            if (msg.indexOf("unexpected end of stream") > 0) {
                if (mShopFileTask.getmBean().getDownloadStatus() == DownloadStatus.DOWNLOAD_STATUS_DOWNLOADING && downFlag) {
                    httpDown(httpClient, mShopFileTask);
                }
            } else {
                downFlag = false;
                mShopFileTask.getmBean().setDownloadStatus(DownloadStatus.DOWNLOAD_STATUS_ERROR);
                mShopFileTask.setErroCode(DownloadStatus.DOWNLOAD_ERROR_CODE_1);
                mShopFileTask.setErroMsg(e.toString());
            }
            LogTeamUtils.e(TAG, "downloadTask downImg is erro is:" + e.toString());
        } finally {
            CloseUtils.closeQuietly(inputFile);
            CloseUtils.closeQuietly(inputStream);
            CloseUtils.closeQuietly(bis);
        }
    }

    @Override
    public void httpDown(ShopFileTask mShopFileTask) {

    }

    @Override
    public void setDownFlag(boolean downFlag) {
        this.downFlag = downFlag;
    }

    @Override
    public void httpDown(OkHttpClient httpClient, FileTask mTask) {

    }
}
