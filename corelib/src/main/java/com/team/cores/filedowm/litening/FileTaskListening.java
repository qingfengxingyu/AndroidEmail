package com.team.cores.filedowm.litening;

import com.team.cores.filedowm.cores.FileTask;

/**
 * author:xmf
 * date:2018/5/23 0023
 * description:普通文件文件下载监听
 */
public interface FileTaskListening {
    void onDownloading(FileTask mTask);

    void onPause(FileTask mTask);

    void onCancel(FileTask mTask);

    void onCompleted(FileTask mTask);

    void onError(FileTask mTask);


}
