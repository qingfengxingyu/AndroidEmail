package com.team.cores.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 *
 * Created by wfx on 2017/6/20.
 */

public class MoreIndexForScrollView extends GridView {
    public MoreIndexForScrollView(Context context) {
        super(context);
    }
    public MoreIndexForScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public MoreIndexForScrollView(Context context, AttributeSet attrs,
                                  int defStyle) {
        super(context, attrs, defStyle);
    }
    
    /**
     * 重写该方法，达到使GridView适应ScrollView的效果
     */
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
        MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}

