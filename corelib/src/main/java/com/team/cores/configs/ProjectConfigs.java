package com.team.cores.configs;

import android.app.Application;

/**
 * author:xmf
 * date:2018/7/19 0019
 * description:项目整体配置
 */
public class ProjectConfigs {
    /***
     * http是否为https
     */
    private boolean isSSL = false;
    private Application app;

    private ProjectConfigs() {

    }

    private static class ProejtctConfigsHodler {
        private static final ProjectConfigs mInstance = new ProjectConfigs();
    }

    public static ProjectConfigs getInstance() {
        return ProejtctConfigsHodler.mInstance;
    }

    /***
     * true https or false http
     * @return
     */
    public boolean isSSL() {
        return isSSL;
    }

    /***
     * true https or false http
     * @param diankeyuan_type
     */
    public void setDiankeyuan_type(boolean diankeyuan_type) {
        this.isSSL = diankeyuan_type;
    }

    public Application getApp() {
        return app;
    }

    public void setApp(Application app) {
        this.app = app;
    }
}
