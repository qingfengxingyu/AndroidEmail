package com.team.cores.httputils;

import org.json.JSONObject;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

//import ly.count.android.sdk.Countly;

/**
 * Created by Administrator on 2017/6/13 .
 * 网络请求公用框架
 */
public class HttpCommonFrame {

    /**
     * HttpClient POST请求
     *
     * @param flag true 老接口 false 新接口
     */
    public static CompositeSubscription postMetod(BaseMode baseMode, final HttpListingInfo httpListingInfo, final boolean flag) {
        return postMetod(baseMode, httpListingInfo, flag, null);
    }

    /**
     * HttpClient POST请求
     *
     * @param flag       true 老接口 false 新接口
     * @param headerJson 请求header
     */
    public static CompositeSubscription postMetod(BaseMode baseMode, final HttpListingInfo httpListingInfo, final boolean flag, final JSONObject headerJson) {
        CompositeSubscription mCompositeSubscription = new CompositeSubscription();
        try {
            mCompositeSubscription.add(HttpCommonFrame.postMetod(baseMode, flag, headerJson).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<Object>() {
                @Override
                public void onCompleted() {
                    httpListingInfo.onCompleted();
                }

                @Override
                public void onError(Throwable e) {
                    httpListingInfo.onError(e);
                }

                @Override
                public void onNext(Object object) {
                    httpListingInfo.onNext(object);
                }
            }));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mCompositeSubscription;
    }


    /**
     * HttpClient POST请求
     *
     * @param baseMode
     * @param flag       true 老接口 false 新接口
     * @param headerJson 请求header
     * @return
     */
    public static Observable<Object> postMetod(final BaseMode baseMode, final boolean flag, final JSONObject headerJson) {
        return Observable.create(new Observable.OnSubscribe<Object>() {
            @Override
            public void call(Subscriber<? super Object> subscriber) {
                if (!subscriber.isUnsubscribed()) {
//                    IRestClient restClient = null;
//                    try {
////                        restClient = RestClient.createDefault(baseMode.getBaseUrl(""));
////                        restClient = MobileService.createBaseClient();
////                            case 0://老服务
////                                restClient = RestClient.createDefault(PlatformConfig.getSingleton().getConsoleServiceUrl());
//                        String baseUrl = baseMode.getBaseUrl();
//                        LogTeamUtils.e("baseUrl is:" + baseUrl);
//                        restClient = RestClient.createDefault(baseUrl);
//                        restClient.setTimeout(MobileService.RPC_TIME_OUT);
//                        HttpUtils.commonHeader(restClient, headerJson);
//                        LogTeamUtils.e("postMetod requestUrl is：" + restClient.getBaseUrl() + baseMode.getUrl());
////                        CountlyPre(restClient.getBaseUrl() + baseMode.getUrl());//Countly埋点
//                        if (flag) {
//                            final RestResult result = restClient.post(baseMode.getUrl(), HttpUtils.changeParams(baseMode.getParams()));
//                            LogTeamUtils.e("result is：" + result.toString());
//                            baseMode.parseObject(result);
//                            subscriber.onNext(result);
////                            CountlyDelay(restClient.getBaseUrl() + baseMode.getUrl(),result.getStatusCode(),result.getMessage());//Countly埋点
//                        } else {
//                            ResponseBean responseBean = restClient.postForObject(baseMode.getUrl(), HttpUtils.changeFastJsonParams(baseMode.getParams()), ResponseBean.class, new ArrayList<NameValuePair>());
//                            baseMode.parseObject(responseBean);
//                            subscriber.onNext(responseBean);
//                            LogTeamUtils.e("result is：" + responseBean.toString());
////                            CountlyDelay(restClient.getBaseUrl() + baseMode.getUrl(),Integer.parseInt(responseBean.getRetcode()),responseBean.getRetmsg());//Countly埋点
//                        }

                        subscriber.onCompleted();//访问结束
//                    } catch (Exception e) {
//                        LogTeamUtils.e("http post is:" + e.toString());
//                        subscriber.onError(e);
//                    }
                }
            }
        });
    }

    /**
     * HttpClient Get请求
     *
     * @param flag true 老接口 false 新接口
     */
    public static CompositeSubscription getMetod(BaseMode baseMode, final HttpListingInfo httpListingInfo, final boolean flag) {
        return getMetod(baseMode, httpListingInfo, flag, null);
    }

    /**
     * HttpClient Get请求
     *
     * @param flag true 老接口 false 新接口
     */
    public static CompositeSubscription getMetod(BaseMode baseMode, final HttpListingInfo httpListingInfo, final boolean flag, final JSONObject headerJson) {
        CompositeSubscription mCompositeSubscription = new CompositeSubscription();
        try {
            mCompositeSubscription.add(HttpCommonFrame.getMetod(baseMode, flag, headerJson).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CustomSubscriber<Object>() {
                @Override
                public void onCompleted() {
                    httpListingInfo.onCompleted();
                }

                @Override
                public void onError(Throwable e) {
                    httpListingInfo.onError(e);
                }

                @Override
                public void onNext(Object object) {
                    httpListingInfo.onNext(object);
                }
            }));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mCompositeSubscription;
    }


    /**
     * HttpClient GET请求
     *
     * @param flag       true 老接口 false 新接口
     * @param headerJson 请求header
     * @return
     */
    public static Observable<Object> getMetod(final BaseMode baseMode, final boolean flag, final JSONObject headerJson) {
        return Observable.create(new Observable.OnSubscribe<Object>() {
            @Override
            public void call(Subscriber<? super Object> subscriber) {
                if (!subscriber.isUnsubscribed()) {
//                    IRestClient restClient = null;
//                    try {
//                        //            restClient = RestClient.createDefault(URLApiInfo.getBaseUrl(""));
////                        restClient = MobileService.createBaseClient();
//                        String baseUrl = baseMode.getBaseUrl();
//                        LogTeamUtils.e("baseUrl is:" + baseUrl);
//                        restClient = RestClient.createDefault(baseUrl);
//                        restClient.setTimeout(MobileService.RPC_TIME_OUT);
//                        HttpUtils.commonHeader(restClient, headerJson);
//                        LogTeamUtils.e("getMetod requestUrl is：" + restClient.getBaseUrl() + baseMode.getUrl());
////                        CountlyPre(restClient.getBaseUrl() + baseMode.getUrl());//Countly埋点
//                        if (flag) {
//                            final RestResult result = restClient.get(baseMode.getUrl(), HttpUtils.changeParams(baseMode.getParams()));
//                            LogTeamUtils.e("result is：" + result.toString());
//                            baseMode.parseObject(result);
//                            subscriber.onNext(result);
////                            CountlyDelay(restClient.getBaseUrl() + baseMode.getUrl(),result.getStatusCode(),result.getMessage());//Countly埋点
//                        } else {
//                            ResponseBean responseBean = restClient.getForObject(baseMode.getUrl(), ResponseBean.class, HttpUtils.changeParams(baseMode.getParams()));
//                            baseMode.parseObject(responseBean);
//                            subscriber.onNext(responseBean);
//                            LogTeamUtils.e("result is：" + responseBean.toString());
////                            CountlyDelay(restClient.getBaseUrl() + baseMode.getUrl(),Integer.parseInt(responseBean.getRetcode()),responseBean.getRetmsg());//Countly埋点
//                        }
                        subscriber.onCompleted();//访问结束
//                    } catch (Exception e) {
//                        LogTeamUtils.e("http get is:" + e.toString());
//                        subscriber.onError(e);
//                    }
                }
            }
        });
    }

//    private static void CountlyPre(String url){
//        Countly.sharedInstance().onNetRequestStart(url);
//    }
//    private static void CountlyDelay(String url,int code,String errorInfo){
//        Countly.sharedInstance().onNetRequestEnd(url, code, errorInfo, 50, code);
//    }

}
