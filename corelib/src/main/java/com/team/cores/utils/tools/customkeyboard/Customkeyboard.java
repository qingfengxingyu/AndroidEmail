package com.team.cores.utils.tools.customkeyboard;

import android.content.Context;
import android.widget.EditText;
/**
 * @Author: sj
 * @Date: 2018/4/20
 * @Description: 安全键盘
 *
 */

public class Customkeyboard  {

    private EditText mEditText;
    private KeyboardPopup mKeyboardPopup;
    private KeyboardPopup keyboardPopup;
    //输入框和键盘实例
    public Customkeyboard(Context context, EditText editText) {
        keyboardPopup = new KeyboardPopup(context);
        this.mEditText=editText;
        this.mKeyboardPopup = keyboardPopup;
    }
    public void initMoveKeyBoard() {
        mKeyboardPopup.setKeyBoardStateChangeListener(new KeyBoardStateListener());
        // monitor the finish or next Key
        mKeyboardPopup.setInputOverListener(new inputOverListener());
        mEditText.setOnTouchListener(new KeyboardTouchListener(mKeyboardPopup, KeyboardPopup.INPUTTYPE_ABC, -1));
    }

    class KeyBoardStateListener implements KeyboardPopup.KeyBoardStateChangeListener {

        @Override
        public void KeyBoardStateChange(int state, EditText editText) {
//            System.out.println("state" + state);
//            System.out.println("editText" + editText.getText().toString());
        }
    }

    class inputOverListener implements KeyboardPopup.InputFinishListener {

        @Override
        public void inputHasOver(int onclickType, EditText editText) {
//            System.out.println("onclickType" + onclickType);
//            System.out.println("editText" + editText.getText().toString());
        }
    }
}
