package mygreendao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

import com.team.cores.dbmode.bean.DownloadDBEntity;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "DOWNLOAD_DBENTITY".
*/
public class DownloadDBEntityDao extends AbstractDao<DownloadDBEntity, String> {

    public static final String TABLENAME = "DOWNLOAD_DBENTITY";

    /**
     * Properties of entity DownloadDBEntity.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property PackName = new Property(0, String.class, "packName", true, "PACK_NAME");
        public final static Property ToolSize = new Property(1, long.class, "toolSize", false, "TOOL_SIZE");
        public final static Property CompletedSize = new Property(2, long.class, "completedSize", false, "COMPLETED_SIZE");
        public final static Property Url = new Property(3, String.class, "url", false, "URL");
        public final static Property SaveDirPath = new Property(4, String.class, "saveDirPath", false, "SAVE_DIR_PATH");
        public final static Property FileName = new Property(5, String.class, "fileName", false, "FILE_NAME");
        public final static Property DownloadStatus = new Property(6, Integer.class, "downloadStatus", false, "DOWNLOAD_STATUS");
        public final static Property Versioncode = new Property(7, String.class, "versioncode", false, "VERSIONCODE");
        public final static Property VersionName = new Property(8, String.class, "versionName", false, "VERSION_NAME");
        public final static Property AppType = new Property(9, int.class, "appType", false, "APP_TYPE");
        public final static Property AppName = new Property(10, String.class, "appName", false, "APP_NAME");
        public final static Property IconUrl = new Property(11, String.class, "iconUrl", false, "ICON_URL");
        public final static Property AppContent = new Property(12, String.class, "appContent", false, "APP_CONTENT");
        public final static Property AppStoreDir = new Property(13, String.class, "appStoreDir", false, "APP_STORE_DIR");
        public final static Property Categorytype = new Property(14, int.class, "categorytype", false, "CATEGORYTYPE");
        public final static Property Totaldowncount = new Property(15, String.class, "totaldowncount", false, "TOTALDOWNCOUNT");
    };


    public DownloadDBEntityDao(DaoConfig config) {
        super(config);
    }
    
    public DownloadDBEntityDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"DOWNLOAD_DBENTITY\" (" + //
                "\"PACK_NAME\" TEXT PRIMARY KEY NOT NULL ," + // 0: packName
                "\"TOOL_SIZE\" INTEGER NOT NULL ," + // 1: toolSize
                "\"COMPLETED_SIZE\" INTEGER NOT NULL ," + // 2: completedSize
                "\"URL\" TEXT," + // 3: url
                "\"SAVE_DIR_PATH\" TEXT," + // 4: saveDirPath
                "\"FILE_NAME\" TEXT," + // 5: fileName
                "\"DOWNLOAD_STATUS\" INTEGER," + // 6: downloadStatus
                "\"VERSIONCODE\" TEXT," + // 7: versioncode
                "\"VERSION_NAME\" TEXT," + // 8: versionName
                "\"APP_TYPE\" INTEGER NOT NULL ," + // 9: appType
                "\"APP_NAME\" TEXT," + // 10: appName
                "\"ICON_URL\" TEXT," + // 11: iconUrl
                "\"APP_CONTENT\" TEXT," + // 12: appContent
                "\"APP_STORE_DIR\" TEXT," + // 13: appStoreDir
                "\"CATEGORYTYPE\" INTEGER NOT NULL ," + // 14: categorytype
                "\"TOTALDOWNCOUNT\" TEXT);"); // 15: totaldowncount
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"DOWNLOAD_DBENTITY\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, DownloadDBEntity entity) {
        stmt.clearBindings();
 
        String packName = entity.getPackName();
        if (packName != null) {
            stmt.bindString(1, packName);
        }
        stmt.bindLong(2, entity.getToolSize());
        stmt.bindLong(3, entity.getCompletedSize());
 
        String url = entity.getUrl();
        if (url != null) {
            stmt.bindString(4, url);
        }
 
        String saveDirPath = entity.getSaveDirPath();
        if (saveDirPath != null) {
            stmt.bindString(5, saveDirPath);
        }
 
        String fileName = entity.getFileName();
        if (fileName != null) {
            stmt.bindString(6, fileName);
        }
 
        Integer downloadStatus = entity.getDownloadStatus();
        if (downloadStatus != null) {
            stmt.bindLong(7, downloadStatus);
        }
 
        String versioncode = entity.getVersioncode();
        if (versioncode != null) {
            stmt.bindString(8, versioncode);
        }
 
        String versionName = entity.getVersionName();
        if (versionName != null) {
            stmt.bindString(9, versionName);
        }
        stmt.bindLong(10, entity.getAppType());
 
        String appName = entity.getAppName();
        if (appName != null) {
            stmt.bindString(11, appName);
        }
 
        String iconUrl = entity.getIconUrl();
        if (iconUrl != null) {
            stmt.bindString(12, iconUrl);
        }
 
        String appContent = entity.getAppContent();
        if (appContent != null) {
            stmt.bindString(13, appContent);
        }
 
        String appStoreDir = entity.getAppStoreDir();
        if (appStoreDir != null) {
            stmt.bindString(14, appStoreDir);
        }
        stmt.bindLong(15, entity.getCategorytype());
 
        String totaldowncount = entity.getTotaldowncount();
        if (totaldowncount != null) {
            stmt.bindString(16, totaldowncount);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, DownloadDBEntity entity) {
        stmt.clearBindings();
 
        String packName = entity.getPackName();
        if (packName != null) {
            stmt.bindString(1, packName);
        }
        stmt.bindLong(2, entity.getToolSize());
        stmt.bindLong(3, entity.getCompletedSize());
 
        String url = entity.getUrl();
        if (url != null) {
            stmt.bindString(4, url);
        }
 
        String saveDirPath = entity.getSaveDirPath();
        if (saveDirPath != null) {
            stmt.bindString(5, saveDirPath);
        }
 
        String fileName = entity.getFileName();
        if (fileName != null) {
            stmt.bindString(6, fileName);
        }
 
        Integer downloadStatus = entity.getDownloadStatus();
        if (downloadStatus != null) {
            stmt.bindLong(7, downloadStatus);
        }
 
        String versioncode = entity.getVersioncode();
        if (versioncode != null) {
            stmt.bindString(8, versioncode);
        }
 
        String versionName = entity.getVersionName();
        if (versionName != null) {
            stmt.bindString(9, versionName);
        }
        stmt.bindLong(10, entity.getAppType());
 
        String appName = entity.getAppName();
        if (appName != null) {
            stmt.bindString(11, appName);
        }
 
        String iconUrl = entity.getIconUrl();
        if (iconUrl != null) {
            stmt.bindString(12, iconUrl);
        }
 
        String appContent = entity.getAppContent();
        if (appContent != null) {
            stmt.bindString(13, appContent);
        }
 
        String appStoreDir = entity.getAppStoreDir();
        if (appStoreDir != null) {
            stmt.bindString(14, appStoreDir);
        }
        stmt.bindLong(15, entity.getCategorytype());
 
        String totaldowncount = entity.getTotaldowncount();
        if (totaldowncount != null) {
            stmt.bindString(16, totaldowncount);
        }
    }

    @Override
    public String readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0);
    }    

    @Override
    public DownloadDBEntity readEntity(Cursor cursor, int offset) {
        DownloadDBEntity entity = new DownloadDBEntity( //
            cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0), // packName
            cursor.getLong(offset + 1), // toolSize
            cursor.getLong(offset + 2), // completedSize
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // url
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // saveDirPath
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // fileName
            cursor.isNull(offset + 6) ? null : cursor.getInt(offset + 6), // downloadStatus
            cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7), // versioncode
            cursor.isNull(offset + 8) ? null : cursor.getString(offset + 8), // versionName
            cursor.getInt(offset + 9), // appType
            cursor.isNull(offset + 10) ? null : cursor.getString(offset + 10), // appName
            cursor.isNull(offset + 11) ? null : cursor.getString(offset + 11), // iconUrl
            cursor.isNull(offset + 12) ? null : cursor.getString(offset + 12), // appContent
            cursor.isNull(offset + 13) ? null : cursor.getString(offset + 13), // appStoreDir
            cursor.getInt(offset + 14), // categorytype
            cursor.isNull(offset + 15) ? null : cursor.getString(offset + 15) // totaldowncount
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, DownloadDBEntity entity, int offset) {
        entity.setPackName(cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0));
        entity.setToolSize(cursor.getLong(offset + 1));
        entity.setCompletedSize(cursor.getLong(offset + 2));
        entity.setUrl(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setSaveDirPath(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setFileName(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setDownloadStatus(cursor.isNull(offset + 6) ? null : cursor.getInt(offset + 6));
        entity.setVersioncode(cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7));
        entity.setVersionName(cursor.isNull(offset + 8) ? null : cursor.getString(offset + 8));
        entity.setAppType(cursor.getInt(offset + 9));
        entity.setAppName(cursor.isNull(offset + 10) ? null : cursor.getString(offset + 10));
        entity.setIconUrl(cursor.isNull(offset + 11) ? null : cursor.getString(offset + 11));
        entity.setAppContent(cursor.isNull(offset + 12) ? null : cursor.getString(offset + 12));
        entity.setAppStoreDir(cursor.isNull(offset + 13) ? null : cursor.getString(offset + 13));
        entity.setCategorytype(cursor.getInt(offset + 14));
        entity.setTotaldowncount(cursor.isNull(offset + 15) ? null : cursor.getString(offset + 15));
     }
    
    @Override
    protected final String updateKeyAfterInsert(DownloadDBEntity entity, long rowId) {
        return entity.getPackName();
    }
    
    @Override
    public String getKey(DownloadDBEntity entity) {
        if(entity != null) {
            return entity.getPackName();
        } else {
            return null;
        }
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
}
