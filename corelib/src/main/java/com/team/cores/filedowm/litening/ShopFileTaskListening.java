package com.team.cores.filedowm.litening;

import com.team.cores.filedowm.cores.ShopFileTask;

/**
 * author:xmf
 * date:2018/5/23 0023
 * description:文件下载监听
 */
public interface ShopFileTaskListening {
    void onDownloading(ShopFileTask mTask);

    void onPause(ShopFileTask mTask);

    void onCancel(ShopFileTask mTask);

    void onCompleted(ShopFileTask mTask);

    void onError(ShopFileTask mTask);


}
