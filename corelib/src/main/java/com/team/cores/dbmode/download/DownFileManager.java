package com.team.cores.dbmode.download;

import com.team.cores.configs.ProjectConfigs;
import com.team.cores.dbmode.bean.DownloadDBEntity;
import com.team.cores.dbmode.tools.GreenDaoHelper;
import com.team.cores.utils.FileSaveTool;
import com.team.cores.utils.tools.LogTeamUtils;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import mygreendao.DownloadDBEntityDao;
import okhttp3.OkHttpClient;

/***
 * 下载文件管理器
 *
 * @author xmf
 */
public class DownFileManager {
    private static DownFileManager downloadManager;
    public static DownloadDBEntityDao downloadDao;
    private int mPoolSize = 20;
    private ExecutorService executorService;
    @SuppressWarnings("rawtypes")
    private Map<String, Future> futureMap;
    private OkHttpClient client;

    public static DownFileManager getInstance() {
        return getInstance(ProjectConfigs.getInstance().isSSL());
    }

    /**
     * @param isSafe true签名 false不签名认证
     * @return
     */
    public static DownFileManager getInstance(boolean isSafe) {
        if (downloadManager == null) {
            downloadManager = new DownFileManager(isSafe);
        }
        return downloadManager;
    }

    public Map<String, DownloadTask> getCurrentTaskList() {
        return currentTaskList;
    }

    private Map<String, DownloadTask> currentTaskList = new HashMap<String, DownloadTask>();

    @SuppressWarnings("rawtypes")
    private void init(OkHttpClient okHttpClient, boolean isSafe) {
        executorService = Executors.newFixedThreadPool(mPoolSize);
        futureMap = new HashMap<String, Future>();
        GreenDaoHelper.getInstance().initDatabase(ProjectConfigs.getInstance().getApp());
        downloadDao = GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao();
        if (okHttpClient != null) {
            client = okHttpClient;
        } else {
            OkHttpClient.Builder buider = new OkHttpClient.Builder();
            buider.connectTimeout(20, TimeUnit.SECONDS);
            buider.writeTimeout(20, TimeUnit.SECONDS);
            buider.readTimeout(90, TimeUnit.SECONDS);
            if (isSafe) {
                final HostnameVerifier hostnameVerifier = new HostnameVerifier() {
                    @Override
                    public boolean verify(final String hostname,
                                          final SSLSession session) {
                        return true;
                    }
                };
                buider.hostnameVerifier(hostnameVerifier);
                buider.sslSocketFactory(initCertificates());
            }
            client = buider.build();
        }
    }

    public static SSLSocketFactory initCertificates() {
        CertificateFactory certificateFactory;

        SSLContext sslContext = null;
        try {
            certificateFactory = CertificateFactory.getInstance("X.509");
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(null);
            String certificateAlias = Integer.toString(0);
            keyStore.setCertificateEntry(
                    certificateAlias,
                    certificateFactory.generateCertificate(ProjectConfigs.getInstance().getApp().getAssets().open("mip_server.cer")));// 拷贝好的证书

            sslContext = SSLContext.getInstance("TLS");

            TrustManagerFactory trustManagerFactory = TrustManagerFactory
                    .getInstance(TrustManagerFactory.getDefaultAlgorithm());

            trustManagerFactory.init(keyStore);
            sslContext.init(null, trustManagerFactory.getTrustManagers(),
                    new SecureRandom());
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (sslContext != null) {
            return sslContext.getSocketFactory();
        }
        return null;

    }

    private DownFileManager(boolean isSafe) {
        init(null, isSafe);
    }

    /**
     * if task already exist,return the task,else return null
     *
     * @param task
     * @param listener
     * @return
     */
    public DownloadTask addDownloadTask(DownloadTask task,
                                        DownloadTaskListener listener) {
        DownloadTask downloadTask = currentTaskList.get(task.getmDownBean().getPackName());
        if (null != downloadTask
                && downloadTask.getDownloadStatus() != DownloadStatus.DOWNLOAD_STATUS_CANCEL) {
            if (null != listener) {
                listener.onCompleted(downloadTask);
            } else {
                downloadTask.statuNum = DownloadStatus.DOWNLOAD_STATUS_COMPLETED;
                EventBus.getDefault().post(downloadTask);
            }
            return downloadTask;
        }
        currentTaskList.put(task.getmDownBean().getPackName(), task);
        task.setDownloadStatus(DownloadStatus.DOWNLOAD_STATUS_DOWNLOADING);
        task.setHttpClient(client);
        task.addDownloadListener(listener);
        if (getDBTaskById(task.getmDownBean().getPackName()) == null) {
            task.getmDownBean().setToolSize(0L);
            task.getmDownBean().setCompletedSize(0L);
            task.getmDownBean().setDownloadStatus(task.getDownloadStatus());
            DownloadDBEntity dbEntity = task.getmDownBean();
            downloadDao.insertOrReplace(dbEntity);
        }
        Future future = executorService.submit(task);
        futureMap.put(task.getmDownBean().getPackName(), future);
        return null;
    }

    /**
     * if return null,the task does not exist
     *
     * @param taskId
     * @return
     */
    public DownloadTask resume(String taskId) {
        DownloadTask downloadTask = getCurrentTaskById(taskId);
        if (downloadTask != null) {
            if (downloadTask.getDownloadStatus() == DownloadStatus.DOWNLOAD_STATUS_PAUSE || downloadTask.getDownloadStatus() == DownloadStatus.DOWNLOAD_STATUS_ERROR) {
                downloadTask
                        .setDownloadStatus(DownloadStatus.DOWNLOAD_STATUS_INIT);
//                if (downloadTask.getDownloadStatus() == DownloadStatus.DOWNLOAD_STATUS_ERROR) {
                downloadTask.setHttpClient(client);
//                }
                @SuppressWarnings("rawtypes")
                Future future = executorService.submit(downloadTask);
                futureMap.put(downloadTask.getmDownBean().getPackName(), future);
            }

        } else {
            downloadTask = getDBTaskById(taskId);
            if (downloadTask != null) {
                downloadTask
                        .setDownloadStatus(DownloadStatus.DOWNLOAD_STATUS_INIT);
//                if (downloadTask.getDownloadStatus() == DownloadStatus.DOWNLOAD_STATUS_ERROR) {
                downloadTask.setHttpClient(client);
//                }
                currentTaskList.put(taskId, downloadTask);
                @SuppressWarnings("rawtypes")
                Future future = executorService.submit(downloadTask);
                futureMap.put(downloadTask.getmDownBean().getPackName(), future);
            }
        }
        return downloadTask;
    }

    public void addDownloadListener(DownloadTask task,
                                    DownloadTaskListener listener) {
        task.addDownloadListener(listener);
    }

    public void removeDownloadListener(DownloadTask task,
                                       DownloadTaskListener listener) {
        task.removeDownloadListener(listener);
    }

    public DownloadTask addDownloadTask(DownloadTask task) {
        return addDownloadTask(task, null);
    }


    /***
     * *必须传参--
     * 下载应用--- id    唯一标识---应用id appType     应用类型 packName    应用包名 url         应用下载地址
     */
    public DownloadTask createDownloadTask(DownloadDBEntity mDownBean) {
        DownloadTask downloadTask = new DownloadTask(mDownBean);
//        File outputFile = new File(PathUtil.combine(Platform.getCurrent().getAppContext().getExternalFilesDir("MIP/APPS/APKS/") + mDownBean.getAppStoreDir()));
        switch (mDownBean.getAppType()) {
            case 1://zip
                downloadTask.getmDownBean().setFileName("/" + mDownBean.getPackName() + ".zip");
                break;
            default://apk
                downloadTask.getmDownBean().setFileName("/" + mDownBean.getPackName() + ".apk");
                break;
        }
        downloadTask.getmDownBean().setSaveDirPath(FileSaveTool.getInstance().getApkSd());
        downloadTask.setDownloadStatus(DownloadStatus.DOWNLOAD_STATUS_DOWNLOADING);
        DownFileManager.getInstance().addDownloadTask(downloadTask);
        return downloadTask;
    }


    public void cancel(DownloadTask task) {
        downloadDao.deleteByKey(task.getmDownBean().getPackName());
        task.cancel();
        currentTaskList.remove(task.getmDownBean().getPackName());
        futureMap.remove(task.getmDownBean().getPackName());
    }

    public void removeTask(String tid) {
        if (null != currentTaskList && currentTaskList.size() > 0) {
            currentTaskList.remove(tid);
            futureMap.remove(tid);
        }

    }

    public void cancel(String taskId) {
        DownloadTask task = getTaskById(taskId);
        if (task != null) {
            cancel(task);
        }
    }


    public void pause(DownloadTask task) {
        task.setDownloadStatus(DownloadStatus.DOWNLOAD_STATUS_PAUSE);
    }

    public void pause(String taskId) {
        DownloadTask task = getTaskById(taskId);
        if (task != null) {
            pause(task);
        }
    }

    public List<DownloadDBEntity> loadAllDownloadEntityFromDB() {
        return downloadDao.loadAll();
    }

    /**
     * if not exists return null the task maybe not in the running task list,
     * you can add{@link #addDownloadTask(DownloadTask) addDownloadTask}
     *
     * @return
     */
    public List<DownloadTask> loadAllDownloadTaskFromDB() {
        List<DownloadDBEntity> list = loadAllDownloadEntityFromDB();
        List<DownloadTask> downloadTaskList = null;
        if (list != null && !list.isEmpty()) {
            downloadTaskList = new ArrayList<DownloadTask>();
            for (DownloadDBEntity entity : list) {
                downloadTaskList.add(DownloadTask.parse(entity));
            }
        }
        return downloadTaskList;
    }

    /**
     * return all task include running and db
     *
     * @return
     */
    public List<DownloadTask> loadAllTask() {
        List<DownloadTask> list = loadAllDownloadTaskFromDB();
        Map<String, DownloadTask> currentTaskMap = getCurrentTaskList();
        List<DownloadTask> currentList = new ArrayList<DownloadTask>();
        if (currentTaskMap != null) {
            currentList.addAll(currentTaskMap.values());
        }
        if (!currentList.isEmpty() && list != null) {
            for (DownloadTask task : list) {
                if (!currentList.contains(task)) {
                    currentList.add(task);
                }
            }
        } else {
            if (list != null)
                currentList.addAll(list);
        }
        return currentList;
    }

    /**
     * return the task in the running task list
     *
     * @param taskId
     * @return
     */
    public DownloadTask getCurrentTaskById(String taskId) {
        return currentTaskList.get(taskId);
    }

    /**
     * if not exists return null the task maybe not in the running task list,
     * you can add{@link #addDownloadTask(DownloadTask) addDownloadTask}
     *
     * @param taskId
     * @return
     */
    public DownloadTask getTaskById(String taskId) {
        DownloadTask task = null;
        task = getCurrentTaskById(taskId);
        if (task != null) {
            return task;
        }
        return getDBTaskById(taskId);
    }

    /**
     * if not exists return null the task maybe not in the running task list,
     * you can add{@link #addDownloadTask(DownloadTask) addDownloadTask}
     *
     * @param taskId
     * @return
     */
    public DownloadTask getDBTaskById(String taskId) {
        DownloadDBEntity entity = downloadDao.load(taskId);
        if (entity != null) {
            return DownloadTask.parse(entity);
        }
        return null;
    }

    public void printAllTask() {
        LogTeamUtils.e("task_xmf", "currentTaskList is:" + currentTaskList.size());
        for (String in : currentTaskList.keySet()) {
            DownloadTask str = currentTaskList.get(in);//得到每个key多对用value的值
            LogTeamUtils.e("task_xmf", in + "  --packName--   " + str.getmDownBean().getPackName() + "  --appName--   " + str.getmDownBean().getAppName() + "--statues--" + str.getmDownBean().getDownloadStatus());
        }
    }

    /***
     * 下载所有进行中的下载任务
     */
    public void runLastAllDown() {
        List<DownloadDBEntity> dataList = GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().queryBuilder().where(DownloadDBEntityDao.Properties.DownloadStatus.eq(DownloadStatus.DOWNLOAD_STATUS_DOWNLOADING)).list();
        if (null != dataList && dataList.size() > 0) {
            for (DownloadDBEntity iBean : dataList) {
                DownFileManager.getInstance().resume(iBean.getPackName());
//                DownloadTask downloadTask = DownFileManager.getInstance().createDownloadTask(iBean);
                LogTeamUtils.e("task_xmf", "downloadTask is:" + iBean.getAppName());
            }
        }
    }
}
