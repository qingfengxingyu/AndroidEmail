package com.xing.email.utils

import me.imid.swipebacklayout.lib.SwipeBackLayout

class ToolUtils {
    companion object {
        /***
         * 右划关闭当前页面
         * @param mSwipeBackLayout
         */
        fun settingSwipeBack(mSwipeBackLayout: SwipeBackLayout?) {
            mSwipeBackLayout?.setEdgeTrackingEnabled(SwipeBackLayout.EDGE_LEFT)
        }
    }
}