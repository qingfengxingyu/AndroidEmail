package com.team.cores.filedowm.utils;

/**
 * Description:App应用类型
 * Data: 2017/11/1 0001 下午 2:20
 *
 * @author: xmf
 */
public class AppType {
    /***
     * apk应用
     */
    public static final int APK_TYPE = 2;
    /***
     * html之zip应用
     */
    public static final int HTML_TYPE = 1;
    /***
     * 安装360框架的apk应用
     */
    public static final int APK_360_TYPE = 4;
    /***
     * 网址链接
     */
    public static final int URL_YTPE = 3;
}
