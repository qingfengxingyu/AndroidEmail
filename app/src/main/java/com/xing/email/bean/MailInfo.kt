package com.xing.email.bean

import java.util.*

/**
 * author:xmf
 * date:2018/11/6 0006
 * description:代表一个即将被发送的邮件
 */
class MailInfo {

    var mailServerHost: String? = null// 发送邮件的服务器的IP
    var mailServerPort: String? = null// 发送邮件的服务器的端口
    var fromAddress: String? = null// 邮件发送者的地址
    var toAddress: String? = null   // 邮件接收者的地址
    var userName: String? = null// 登陆邮件发送服务器的用户名
    var password: String? = null// 登陆邮件发送服务器的密码
    var isValidate = true// 是否需要身份验证
    var subject: String? = null// 邮件主题
    var content: String? = null// 邮件的文本内容
    var attachFileNames: Array<String>? = null// 邮件附件的文件名
    var getServerHost: String? = null//拉取邮件服务器的IP
    var getServerPort: String? = null// 拉取邮件服务器的端口
    /**
     * 获得邮件会话属性
     */
    val properties: Properties
        get() {
            val p = Properties()
            p["mail.smtp.host"] = this.mailServerHost
            p["mail.smtp.port"] = this.mailServerPort
            p["mail.smtp.auth"] = if (isValidate) "true" else "false"
            return p
        }

    /**
     * 获取邮件的会话属性
     */
    val POP3_properties: Properties
        get() {
            val p = Properties()
            p["mail.pop3.host"] = getServerHost
            p["mail.pop3.port"] = getServerPort
            p["mail.pop3.ssl.enable"] = if (isValidate) "true" else "false"
            return p
        }
}

