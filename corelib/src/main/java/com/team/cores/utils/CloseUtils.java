package com.team.cores.utils;

import java.io.Closeable;
import java.io.IOException;

/**
 * author:xmf
 * date:2018/5/24 0024
 * description:统一关闭Closeable对象---针对IO的
 */
public class CloseUtils {
    private CloseUtils() {
    }

    /***
     * 关闭Closeable对象
     * @param closeable
     */
    public static void closeQuietly(Closeable closeable) {
        if (null != closeable) {
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
