package com.team.cores.filedowm.utils;

import android.content.Context;
import android.os.Environment;

import com.team.cores.dbmode.bean.DownloadDBEntity;
import com.team.cores.dbmode.download.DownloadStatus;
import com.team.cores.dbmode.tools.GreenDaoHelper;
import com.team.cores.filedowm.ShopFileDownManager;
import com.team.cores.utils.FileSaveTool;
import com.team.cores.utils.LogTools;
import com.team.cores.utils.MD5;
import com.team.cores.utils.ToolUtils;
import com.team.cores.utils.tools.AppUtils;
import com.team.cores.utils.tools.LogTeamUtils;
import com.team.cores.utils.tools.StringUtils;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * author:xmf
 * date:2018/5/25 0025
 * description:下载文件数据库操作辅助类
 */
public class DaoFileUtils {
    final static String TAG = "DaoUtils_xmf";

    private DaoFileUtils() {
    }

    private static class DaoUtilsInstance {
        private static final DaoFileUtils mInstance = new DaoFileUtils();
    }

    public static DaoFileUtils getInstance() {
        return DaoUtilsInstance.mInstance;
    }

    /***
     * 新建任务添加入库
     * @param mBean
     */
    public DownloadDBEntity addDB(DownloadDBEntity mBean) {
        if (null == mBean) {
            LogTeamUtils.e(TAG, "addDB___TaskDownBean不能为空");
            return null;
        }
        mBean.setDownloadStatus(DownloadStatus.DOWNLOAD_STATUS_DOWNLOADING); //下载状态
        DownloadDBEntity dbEntity = GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().load(mBean.getPackName());
        if (null == dbEntity) {
            mBean.setCompletedSize(0L);//当前下载大小
            switch (mBean.getAppType()) {
                case 1://zip
                    if (ShopFileDownManager.getInstance().getmFileDownConfig().getMd5()) {
                        mBean.setFileName(File.separator + MD5.compile(mBean.getPackName()) + ".zip");
                    } else {
                        mBean.setFileName(File.separator + mBean.getPackName() + ".zip");
                    }
                    break;
                default://apk
                    if (ShopFileDownManager.getInstance().getmFileDownConfig().getMd5()) {
                        mBean.setFileName(File.separator + MD5.compile(mBean.getPackName() + "_" + mBean.getVersionName()) + ".apk");
                    } else {
                        mBean.setFileName(File.separator + mBean.getPackName() + "_" + mBean.getVersionName() + ".apk");
                    }

                    break;
            }
            mBean.setSaveDirPath(FileSaveTool.getInstance().getApkSd() + mBean.getFileName());//sd卡存放地址
            mBean.setDownloadStatus(DownloadStatus.DOWNLOAD_STATUS_DOWNLOADING); //下载状态
            GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().insertOrReplace(mBean);//添加入库
            return mBean;
        } else {
            if (mBean.getToolSize() > 0) {
                dbEntity.setToolSize(mBean.getToolSize());
            }
            if (mBean.getCompletedSize() > 0) {
                dbEntity.setCompletedSize(mBean.getCompletedSize());
            }
            dbEntity.setVersionName(mBean.getVersionName());
            dbEntity.setVersioncode(mBean.getVersioncode());
            switch (dbEntity.getAppType()) {
                case 1://zip
                    if (ShopFileDownManager.getInstance().getmFileDownConfig().getMd5()) {
                        dbEntity.setFileName(File.separator + MD5.compile(dbEntity.getPackName()) + ".zip");
                    } else {
                        dbEntity.setFileName(File.separator + dbEntity.getPackName() + ".zip");
                    }
                    break;
                default://apk
                    if (ShopFileDownManager.getInstance().getmFileDownConfig().getMd5()) {
                        dbEntity.setFileName(File.separator + MD5.compile(dbEntity.getPackName() + "_" + dbEntity.getVersionName()) + ".apk");
                    } else {
                        dbEntity.setFileName(File.separator + dbEntity.getPackName() + "_" + dbEntity.getVersionName() + ".apk");
                    }

                    break;
            }
            dbEntity.setSaveDirPath(FileSaveTool.getInstance().getApkSd() + dbEntity.getFileName());//sd卡存放地址
            dbEntity.setDownloadStatus(mBean.getDownloadStatus());
            if (!StringUtils.isEmpty(mBean.getUrl())) {
                dbEntity.setUrl(mBean.getUrl());
            }
            GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().update(dbEntity);//添加入库
            return dbEntity;
        }

    }


    /**
     * 删除数据库记录
     *
     * @param downloadId
     */
    public void delDB(String downloadId) {
        DownloadDBEntity dbEntity = GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().load(downloadId);
        if (null != dbEntity) {//删除下载文件
            File temp = new File(dbEntity.getSaveDirPath());
            if (temp.exists())
                temp.delete();
            if (dbEntity.getAppType() == 1){
                File file = new File(Environment
                        .getExternalStorageDirectory() + "/nari_mip/downfiles/zips/" + dbEntity.getPackName());
                if (file.exists()){
                    file.delete();
                }
            }
        }
        GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().deleteByKey(downloadId);//移除数据
    }

    /***
     * 修改当前下载文件的实际状态
     * @param downloadId
     * @param downStatues
     */
    public void updateDownStatues(String downloadId, int downStatues) {
        DownloadDBEntity mBean = GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().load(downloadId);
        mBean.setDownloadStatus(downStatues);
        GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().update(mBean);
    }

    public byte[] readInputStream(BufferedInputStream inStream)
            throws Exception {
        // 构造一个ByteArrayOutputStream
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        // 设置一个缓冲区
        byte[] buffer = new byte[1024];
        int len = 0;
        // 判断输入流长度是否等于-1，即非空
        while ((len = inStream.read(buffer)) != -1) {
            // 把缓冲区的内容写入到输出流中，从0开始读取，长度为len
            outStream.write(buffer, 0, len);
        }
        // 关闭输入流
        inStream.close();
        return outStream.toByteArray();
    }

    /***
     * 获得所有下载任务
     * @return
     */
    public List<DownloadDBEntity> getDownAppListAll() {
        List<DownloadDBEntity> mList = new ArrayList<>();
        List<DownloadDBEntity> mTeampList = GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().loadAll();
        if (null != mTeampList && mTeampList.size() > 0) {
            DownloadDBEntity temp = new DownloadDBEntity();
            temp.setAppName("所有任务");
            temp.setCategorytype(3);
            mList.add(temp);
            mList.addAll(mTeampList);
        }
        return mList;
    }

    /***
     * 获得安装管理下载任务，并区分安装任务和已安装任务
     * @return
     */
    public List<DownloadDBEntity> getDownAppList(Context mContext) {
        List<DownloadDBEntity> mList = new ArrayList<>();
        List<DownloadDBEntity> mTeampList = GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().loadAll();
        List<DownloadDBEntity> mNowList = new ArrayList<>();//已经安装
        if (null != mTeampList && mTeampList.size() > 0) {
            DownloadDBEntity temp = new DownloadDBEntity();
            temp.setAppName("安装任务");
            temp.setCategorytype(3);
            mList.add(temp);
            for (DownloadDBEntity mItem : mTeampList) {
                checkStatue(mContext, mItem, mList, mNowList, false);
            }
            if (mList.size() <= 1) {
                mList.clear();
            }
            if (null != mNowList && mNowList.size() > 0) {
                DownloadDBEntity temp2 = new DownloadDBEntity();
                temp2.setAppName("已安装任务");
                temp2.setCategorytype(3);
                mList.add(temp2);
                mList.addAll(mNowList);
            }
        }
        return mList;
    }

    /**
     * 添加更新应用
     */
    public void addUpdateAppMsg(DownloadDBEntity mBean) {
        DownloadDBEntity dbEntity = GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().load(mBean.getPackName());
        if (null == dbEntity) {
            switch (mBean.getAppType()) {
                case 1://zip
                    if (ShopFileDownManager.getInstance().getmFileDownConfig().getMd5()) {
                        mBean.setFileName(File.separator + MD5.compile(mBean.getPackName()) + ".zip");
                    } else {
                        mBean.setFileName(File.separator + mBean.getPackName() + ".zip");
                    }
                    break;
                default://apk
                    if (ShopFileDownManager.getInstance().getmFileDownConfig().getMd5()) {
                        mBean.setFileName(File.separator + MD5.compile(mBean.getPackName() + "_" + mBean.getVersionName()) + ".apk");
                    } else {
                        mBean.setFileName(File.separator + mBean.getPackName() + "_" + mBean.getVersionName() + ".apk");
                    }

                    break;
            }
            mBean.setSaveDirPath(FileSaveTool.getInstance().getApkSd() + mBean.getFileName());//sd卡存放地址
            mBean.setDownloadStatus(DownloadStatus.DOWNLOAD_STATUS_UPDATE); //更新状态
            GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().insertOrReplace(mBean);
        } else {
            dbEntity.setVersioncode(mBean.getVersioncode());
            dbEntity.setVersionName(mBean.getVersionName());
            dbEntity.setAppContent(mBean.getAppContent());
            dbEntity.setAppStoreDir(mBean.getAppStoreDir());
            GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().update(dbEntity);
        }

    }


    /***
     * 获得更新下载任务
     * @return
     */
    public List<DownloadDBEntity> getDownUpdateAppList(Context mContext) {
        List<DownloadDBEntity> mList = new ArrayList<>();
        List<DownloadDBEntity> mTeampList = GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().loadAll();
        if (null != mTeampList && mTeampList.size() > 0) {
            DownloadDBEntity temp = new DownloadDBEntity();
            temp.setAppName("推荐更新");
            temp.setCategorytype(3);
            mList.add(temp);
            for (DownloadDBEntity mItem : mTeampList) {
                checkStatue(mContext, mItem, mList, null, true);
            }
            if (mList.size() <= 1) {
                mList.clear();
            }
        }
        return mList;
    }


    public void checkStatue(Context mContext, DownloadDBEntity mItem, List<DownloadDBEntity> mList, List<DownloadDBEntity> mNowList, boolean updateFlag) {//判断当前下载任务属于哪种状态
        File mFile = null;
        switch (mItem.getAppType()) {
            case AppType.HTML_TYPE:
                mFile = new File(mItem.getSaveDirPath());
                if (null != mFile && mFile.exists() && mFile.length() >= mItem.getToolSize()) {
                    if (ToolUtils.needUpdate_htmlZip(FileSaveTool.getInstance().getZipSd()+ File.separator + mItem.getPackName() + File.separator + mItem.getVersionName(), mItem.getVersionName())) {//需要更新
                        if (updateFlag) {
                            mItem.setCategorytype(2);
                            mList.add(mItem);
                        }
                    } else {
                        if (!updateFlag) {
                            mItem.setCategorytype(1);
                            mNowList.add(mItem);
                        }

                    }
                } else {
                    if (!updateFlag) {
                        mItem.setCategorytype(0);
                        mList.add(mItem);
                    }
                }
                break;
            case AppType.APK_360_TYPE:
                mFile = new File(mItem.getSaveDirPath());
                if (null != mFile && mFile.exists() && mFile.length() >= mItem.getToolSize()) {
                    if (ToolUtils.needUpdate(AppUtils.getApkVersionName(mContext, mItem.getSaveDirPath()), mItem.getVersionName(), AppUtils.getApkVersionCode(mContext, mItem.getSaveDirPath()) + "", mItem.getVersioncode())) {//需要更新
                        if (updateFlag) {
                            mItem.setCategorytype(2);
                            mList.add(mItem);
                        }
                    } else {
                        if (!updateFlag) {
                            mItem.setCategorytype(1);
                            mNowList.add(mItem);
                        }
                    }
                } else {
                    if (!updateFlag) {
                        mItem.setCategorytype(0);
                        mList.add(mItem);
                    }
                }
                break;
            case AppType.APK_TYPE:
                if (AppUtils.isInstallApp(mContext, mItem.getPackName())) {//已安装任务并且不需要更新
                    if (ToolUtils.needUpdate(AppUtils.getAppVersionName(mContext, mItem.getPackName()), mItem.getVersionName(), AppUtils.getAppVersionCode(mContext, mItem.getPackName()) + "", mItem.getVersioncode())) {//需要更新
                        if (updateFlag) {
                            mItem.setCategorytype(2);
                            mList.add(mItem);
                        }

                    } else {
                        if (!updateFlag) {
                            mItem.setCategorytype(1);
                            mNowList.add(mItem);
                        }
                    }
                } else {
                    if (!updateFlag) {
                        mItem.setCategorytype(0);
                        mList.add(mItem);
                    }
                }
                break;
        }
    }
}
