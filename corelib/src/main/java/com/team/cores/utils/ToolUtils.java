package com.team.cores.utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TabLayout;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.team.cores.R;
import com.team.cores.filedowm.utils.AppType;
import com.team.cores.filedowm.utils.ConfigBean;
import com.team.cores.utils.tools.AppUtils;
import com.team.cores.utils.tools.LogTeamUtils;
import com.team.cores.utils.tools.StringUtils;

import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class ToolUtils {
    /**
     * 判断软键盘是否显示
     *
     * @param ctx
     * @return
     */
    public static boolean isInputShow(final Context ctx) {
        return ((Activity) ctx).getWindow().getAttributes().softInputMode == WindowManager.LayoutParams.SOFT_INPUT_STATE_UNSPECIFIED;
    }

    /**
     * 隐藏软键盘
     *
     * @param ctx
     * @return void
     */
    public static void closeInput(final Context ctx) {
        InputMethodManager imm = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(((Activity) ctx).getCurrentFocus()
                .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    /**
     * 显示软键盘
     *
     * @param ctx
     * @param contentET
     * @return void
     */
    public static void showInput(final Context ctx, EditText contentET) {
        InputMethodManager imm = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(contentET, 0);
    }

    /**
     * 显示软键盘
     *
     * @return void
     */
    public static void showInput(final Context con) {
        InputMethodManager imm = (InputMethodManager) con
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,
                InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    /**
     * 延迟显示软键盘
     *
     * @param edit
     * @param delay
     */
    public static void autoInput(final EditText edit, int delay) {
        edit.setFocusable(true);
        edit.setFocusableInTouchMode(true);
        edit.requestFocus();
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                InputMethodManager inputManager = (InputMethodManager) edit
                        .getContext().getSystemService(
                                Context.INPUT_METHOD_SERVICE);
                inputManager.showSoftInput(edit, 0);
            }
        }, delay);
        return;
    }

    /**
     * 实现文本复制功能 add by wangqianzhou
     *
     * @param content
     */
    @SuppressWarnings("deprecation")
    public static void copy(String content, Context context) {
        // 得到剪贴板管理器
        ClipboardManager cmb = (ClipboardManager) context
                .getSystemService(Context.CLIPBOARD_SERVICE);
        cmb.setText(content.trim());
    }

    /**
     * 实现粘贴功能 add by wangqianzhou
     *
     * @param context
     * @return
     */
    @SuppressWarnings("deprecation")
    public static String paste(Context context) {
        // 得到剪贴板管理器
        ClipboardManager cmb = (ClipboardManager) context
                .getSystemService(Context.CLIPBOARD_SERVICE);
        return cmb.getText().toString().trim();
    }

    /****
     * 判断List是否为空
     *
     * @param dataList
     * @return true list为空或没有值；false list不为空，并有值
     */
    public static boolean isListNull(List<?> dataList) {
        if (null != dataList && dataList.size() > 0) {
            return false;
        } else {
            return true;
        }

    }

    /***
     * 判断当前Activity是否关闭
     * true 关闭 false 没有关闭
     *
     * @return
     */
    public static boolean isActivityISNull(Activity context) {
        if (null != context && !context.isFinishing()) {
            return false;
        }
        return true;
    }

    /***
     * 判断是否存在SD卡 true：存在 false：不存在
     *
     * @return
     */
    public static boolean isSD() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return true;
        } else {
            return false;
        }
    }

    public static String uuid32len() {
        return java.util.UUID.randomUUID().toString().replace("-", "");
    }


    /***
     * 获得请求时间
     *
     * @param startTime
     * @return
     */
    public static String getRequestTimes(long startTime) {
        String requetTime = "";
        long nowTime = System.currentTimeMillis();
        if (nowTime - startTime > 1000) {
            requetTime = (nowTime - startTime) / 1000 + " s";
        } else {
            requetTime = (nowTime - startTime) + " ms";
        }
        return requetTime;
    }

    /***
     * 请求的公共参数
     *
     * @param params
     * @return
     */
    public static JSONObject getStatisticalData(JSONObject params) {
        try {
            params.put("channelid", "145");
            params.put("platform", "android");
            params.put("version", "3.0.0");
            params.put("access_token", "x5wWhVpWBuUc7h4V");
            params.put("channelname", "Mocuz");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }


    /**
     * @param context     上下文
     * @param appPackName 第三方应用包名
     * @return 第三方应用图标的Drable对象
     */
    public static Drawable getThirdAppIcon(Context context, String appPackName) {
        PackageManager packageManager = context.getPackageManager();
        Drawable icon = null;

        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(appPackName, PackageManager.GET_META_DATA);
            icon = packageManager.getApplicationIcon(applicationInfo);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return icon;
    }

    public static String getTimeStyle(String oldTime) {
        return getTimeStyle(oldTime, false);
    }

    /***
     * 时间显示格式化
     *
     * @param oldTime 原始时间
     * @param falg    true---oldTime不变 false--- oldTime除以1000
     * @return
     */
    public static String getTimeStyle(String oldTime, boolean falg) {
        String time = "";
        if (StringUtils.isEmpty(oldTime)) {
            return "刚刚";
        }
        try {
            int dTime = 0;
            if (falg) {
                dTime = (int) (System.currentTimeMillis() / 1000 - Long
                        .parseLong(oldTime)) / 60;
            } else {
                dTime = (int) (System.currentTimeMillis() / 1000 - Long
                        .parseLong(oldTime) / 1000) / 60;

            }
            if (dTime <= 0) {
                time = "刚刚";
            } else if (dTime <= 60) {
                time = dTime + "分钟之前";
            } else if (60 <= dTime && dTime <= 1440) {
                time = dTime / 60 + "小时之前";
            } else {
                time = dTime / 60 / 24 + "天之前";
            }
            if ("0分钟之前".equals(time)) {
                time = "刚刚";
            }
        } catch (Exception e) {
            e.printStackTrace();
            time = oldTime;
        }
        return time;
    }

    /***
     * 时间显示格式化
     *
     * @param oldTime
     * @return
     */
    public static String getDateTimeStyle(String oldTime) {
        if (StringUtils.isEmpty(oldTime)) {
            return "刚刚";
        }
        return getTimeStyle(dateToStamp(oldTime), true);
    }

    /*
     * 将时间转换为秒
     */
    public static String dateToStamp(String s) {
        String res = "";
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = simpleDateFormat.parse(s);
            long ts = date.getTime();
            res = ts / 1000 + "";
        } catch (Exception e) {
            LogTeamUtils.e(e.toString());
        }
        return res;
    }

//    public static void msgShowRing() {
//        Vibrator mVibrator = (Vibrator) Platform.getCurrent().getAppContext().getSystemService(Context.VIBRATOR_SERVICE);
//        mVibrator.vibrate(new long[]{500, 500, 500, 500,}, -1);//0.5S间隔振铃
//        try {
//            MediaPlayer player = new MediaPlayer();
//            AssetManager assetManager = Platform.getCurrent().getAppContext().getAssets();
//            AssetFileDescriptor fileDescriptor = assetManager.openFd("abc.wav");
//            player.setDataSource(fileDescriptor.getFileDescriptor(), fileDescriptor.getStartOffset(),
//                    fileDescriptor.getStartOffset());
//            player.prepare();
//            player.start();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }


    /***
     * 添加传递参数
     *
     * @param intentJson
     * @return
     */
    public static void changeParams(String intentJson, Intent intent) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(intentJson);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (null == jsonObject) {
            return;
        }
        LogTeamUtils.e("jsonObject is:" + jsonObject);
        String key = "";
        String values = "";
        Iterator<String> sIterator = jsonObject.keys();
        while (sIterator.hasNext()) {
            try {
                key = sIterator.next();
                values = jsonObject.getString(key);
                intent.putExtra(key, values);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /***
     * 最新获得下载文件路径
     *
     * @param webUrl
     * @return
     */
    public static String getDownFile(String webUrl) {
        return "clientdownload?info=" + webUrl;
    }


    /***
     * 比较本地版本和服务器版本的方法
     * @param localVersion 本地versionName
     * @param netVersion 服务器最新versionName
     * @param localCode 本地versionCode
     * @param netCode   服务器最新versionCode
     * @return true 需要更新 false不需要更新
     */
    public static boolean needUpdate(String localVersion, String netVersion, String
            localCode, String netCode) {
        if (localCode != null && netCode != null && !localCode.equals("") && !netCode.equals("") &&
                Integer.parseInt(localCode) < Integer.parseInt(netCode)) {
            return true;
        } else {
            try {
                String[] local = localVersion == null ? "1.0.0".split("\\.") : localVersion.split("\\.");
                String[] net = netVersion == null ? "1.0.0".split("\\.") : netVersion.split("\\.");
                if (local.length < net.length) {
                    return true;
                } else {
                    for (int i = 0; i < local.length; i++) {
                        if (Integer.parseInt(local[i]) < Integer.parseInt(net[i])) {
                            return true;
                        } else if (Integer.parseInt(local[i]) > Integer.parseInt(net[i])) {
                            return false;
                        }
                    }
                }
            } catch (Exception e) {
                LogTeamUtils.e(e.toString());
            }
            return false;
        }
    }

    /**
     * 获取本地Html类型应用信息
     *
     * @param path
     * @return
     */
    public static ConfigBean getLocalHtmlInfo(String path) {
        File file = new File(path, "config.xml");
        ConfigBean config = null;
        if (file.exists()) {
            try {
                config = new ConfigBean();
                InputStream stream = new FileInputStream(file.getAbsolutePath());
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(true);
                XmlPullParser xpp = factory.newPullParser();
                xpp.setInput(stream, "UTF-8");
                int type = xpp.getEventType();
                while (type != XmlPullParser.END_DOCUMENT) {
                    if (type == XmlPullParser.START_TAG) {
                        if ("widget".equalsIgnoreCase(xpp.getName())) {
                            int count = xpp.getAttributeCount();
                            for (int i = 0; i < count; i++) {
                                String name = xpp.getAttributeName(i);
                                String value = xpp.getAttributeValue(i);
                                if (name.equalsIgnoreCase("widgetId")) {
                                    config.setWidgetId(value);
                                } else if (name.equalsIgnoreCase("pid")) {
                                    config.setPid(value);
                                } else if (name.equalsIgnoreCase("appId")) {
                                    config.setAppId(value);
                                } else if (name.equalsIgnoreCase("channelCode")) {
                                    config.setChannelCode(value);
                                } else if (name.equalsIgnoreCase("version")) {
                                    config.setVersion(value);
                                } else if (name.equalsIgnoreCase("viewmode")) {
                                    config.setViewmode(value);
                                } else if (name.equalsIgnoreCase("width")) {
                                    config.setWidth(value);
                                } else if (name.equalsIgnoreCase("height")) {
                                    config.setHeight(value);
                                } else if (name.equalsIgnoreCase("sreensize")) {
                                    config.setSreensize(value);
                                }
                            }
                        }
                    }
                    type = xpp.next();
                }
                stream.close();
            } catch (Exception e) {
                e.printStackTrace();
                config = null;
            }
        }
        return config;
    }

    /**
     * 判断H5类型应用是否需要更新
     *
     * @param path       本地文件夹路径
     * @param netVersion 服务器versionName
     * @return true需要更新  false不需要更新
     */
    public static boolean needUpdate_htmlZip(String path, String netVersion) {
        if (StringUtils.isEmpty(path) || StringUtils.isEmpty(netVersion)) {
            return false;
        }
        ConfigBean mConfig = getLocalHtmlInfo(path);
        if (null == mConfig) {
            return false;
        }
        return needUpdate(mConfig.getVersion(), netVersion, null, null);
    }

    /***
     * 获得下载数率的转换
     * @param fileS
     * @return
     */
    public static String getDownSpeed(int fileS) {
        DecimalFormat df = new DecimalFormat("0.00");
        String speed = "";
        if (fileS < 1024) {
            speed = df.format((double) fileS) + "B/S";
        } else if (fileS < 1048576) {
            speed = df.format((double) fileS / 1024) + "Kb/S";
        } else if (fileS < 1073741824) {
            speed = df.format((double) fileS / 1048576) + "Mb/S";
        } else {
            speed = df.format((double) fileS / 1073741824) + "G/S";
        }
        return speed;
    }

    /***
     * 根据应用类型获得当前应用本地versinoname
     * @param mContext 上下文
     * @param apptype 应用类型
     * @param sdPath sd卡存放路径
     * @param packName 应用包名 没有可不传
     * @return
     */
    public static String getAppVersionName(Context mContext, int apptype, String sdPath, String packName) {
        String versionName = "100";
        try {
            switch (apptype) {
                case AppType.HTML_TYPE:
                    ConfigBean configBean = getLocalHtmlInfo(FileSaveTool.getInstance().getZipSd() + File.separator + packName);
                    if (null != configBean) {
                        versionName = configBean.getVersion();
                    }
                    break;
                case AppType.APK_360_TYPE:
                    versionName = AppUtils.getApkVersionName(mContext, sdPath);
                    break;
                case AppType.APK_TYPE:
                    versionName = AppUtils.getAppVersionName(mContext, packName);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return versionName;
    }

    /***
     * TabLayout设置下划线(Indicator)宽度
     * @param tabLayout
     */
    public static void reflex(Context mContext, TabLayout tabLayout) {
        try {
            //了解源码得知 线的宽度是根据 tabView的宽度来设置的
            LinearLayout mTabStrip = (LinearLayout) tabLayout.getChildAt(0);
            for (int i = 0; i < mTabStrip.getChildCount(); i++) {
                View tabView = mTabStrip.getChildAt(i);
                //拿到tabView的mTextView属性  tab的字数不固定一定用反射取mTextView
                Field mTextViewField =
                        tabView.getClass().getDeclaredField("mTextView");
                mTextViewField.setAccessible(true);
                TextView mTextView = (TextView) mTextViewField.get(tabView);
                tabView.setPadding(0, 0, 0, 0);
                //因为我想要的效果是   字多宽线就多宽，所以测量mTextView的宽度
                int width = 0;
                width = mTextView.getWidth();
                if (width == 0) {
                    mTextView.measure(0, 0);
                    width = mTextView.getMeasuredWidth();
                }
                DisplayMetrics dm = mContext.getResources().getDisplayMetrics();
                int nowWidth = (dm.widthPixels / 2 - width) / 2;
                //设置tab左右间距为10dp  注意这里不能使用Padding
                // 因为源码中线的宽度是根据 tabView的宽度来设置的
                LinearLayout.LayoutParams params =
                        (LinearLayout.LayoutParams) tabView.getLayoutParams();
                params.leftMargin = nowWidth;
                params.rightMargin = nowWidth;
                tabView.setLayoutParams(params);
                tabView.invalidate();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}