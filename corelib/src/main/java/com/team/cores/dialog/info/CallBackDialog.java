package com.team.cores.dialog.info;

public interface CallBackDialog {

    public void yes_btn(String msg);

    public void no_btn(String msg);
}
