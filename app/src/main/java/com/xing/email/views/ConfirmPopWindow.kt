package com.xing.email.views


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup.LayoutParams
import android.view.WindowManager
import android.widget.PopupWindow
import com.xing.email.R
import com.xing.email.app.MyApp
import com.xing.email.ui.LoginUI
import com.xing.email.ui.SendEmailUI


/**
 * 弹窗视图
 */
class ConfirmPopWindow(private val context: Context) : PopupWindow(context), View.OnClickListener {
    private var sendEmail: View? = null
    private var setting: View? = null

    init {
        initalize()
    }

    private fun initalize() {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.confirm_dialog, null)
        sendEmail = view.findViewById(R.id.sendEmail)//发邮件
        setting = view.findViewById(R.id.setting)//设置
        sendEmail!!.setOnClickListener(this)
        setting!!.setOnClickListener(this)
        contentView = view
        initWindow()
    }

    private fun initWindow() {
        val d = context.resources.displayMetrics
        this.width = (d.widthPixels * 0.35).toInt()
        this.height = LayoutParams.WRAP_CONTENT
        this.isFocusable = true
        this.isOutsideTouchable = true
        this.update()
        //实例化一个ColorDrawable颜色为半透明
        val dw = ColorDrawable(0x00000000)
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw)
        backgroundAlpha(context as Activity, 0.8f)//0.0-1.0
        this.setOnDismissListener { backgroundAlpha(context, 1f) }
    }

    //设置添加屏幕的背景透明度
    fun backgroundAlpha(context: Activity, bgAlpha: Float) {
        val lp = context.window.attributes
        lp.alpha = bgAlpha
        context.window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        context.window.attributes = lp
    }

    fun showAtBottom(view: View) {
        //弹窗位置设置
        showAsDropDown(view, Math.abs((view.width - width) / 2), 10)
        //showAtLocation(view, Gravity.TOP | Gravity.RIGHT, 10, 110);//有偏差
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.sendEmail -> {
                context.startActivity(Intent(context, SendEmailUI::class.java))
                this.dismiss()
            }
            R.id.setting -> {
                val intent = Intent(context, LoginUI::class.java).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                context.startActivity(intent)
                MyApp.session = null
                this.dismiss()
            }
        }
    }

}
