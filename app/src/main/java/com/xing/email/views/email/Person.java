package com.xing.email.views.email;

import android.os.Parcel;
import android.os.Parcelable;

public class Person {
    private String name;
    private String email;

    Person(String n, String e) {
        name = n;
        email = e;
    }


    public String getName() {
        return name;
    }

    String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return name;
    }

    private Person(Parcel in) {
        this.name = in.readString();
        this.email = in.readString();
    }

}