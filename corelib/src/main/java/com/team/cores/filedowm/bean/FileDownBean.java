package com.team.cores.filedowm.bean;

import com.team.cores.utils.ConfigTools;
import com.team.cores.utils.FileSaveTool;
import com.team.cores.utils.MD5;

/**
 * author:xmf
 * date:2018/5/29 0029
 * description:普通文件---单个文件下载需要的实体类
 */
public class FileDownBean {
    /***
     * 文件下载路径
     */
    private String url;
    /**
     * SD卡存储路径
     */
    private String saveDirPath;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSaveDirPath() {
        if (null == saveDirPath || "".equals(saveDirPath)) {
            return FileSaveTool.getInstance().getApkSd() + MD5.compile(url);
        }
        return saveDirPath;
    }

    /***
     * 不是必传，不传存储默认路径，文件名没有后缀名
     * @param saveDirPath
     */
    public void setSaveDirPath(String saveDirPath) {
        this.saveDirPath = saveDirPath;
    }

    public String getDownloadId() {
        return MD5.compile(url);
    }
}
