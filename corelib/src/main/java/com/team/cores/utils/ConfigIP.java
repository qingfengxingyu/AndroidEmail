package com.team.cores.utils;

/**
 * 解析登录配置信息实体
 * Created by xmf on 2018/3/16 0016.
 */
public class ConfigIP {
    public String ip;
    public int port;
    public String typeNums;// NONE---不使用安全网 VPN ---南瑞信通VPN网关ISCP---智研院ISCP网关
    public String safeIp;
    public int safePort;
}
