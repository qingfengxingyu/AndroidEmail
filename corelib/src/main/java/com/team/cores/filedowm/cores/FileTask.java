package com.team.cores.filedowm.cores;

import android.os.Handler;

import com.team.cores.dbmode.download.DownloadStatus;
import com.team.cores.filedowm.FileDownManager;
import com.team.cores.filedowm.bean.FileDownBean;
import com.team.cores.filedowm.imp.HttpDownCore;
import com.team.cores.filedowm.impl.FileHttpDwon;
import com.team.cores.filedowm.litening.FileTaskListening;
import com.team.cores.utils.ConfigTools;

import java.io.File;

import okhttp3.OkHttpClient;

/**
 * author:xmf
 * date:2018/5/23 0023
 * description:普通文件---用于单个文件的断点下载支持，不需要数据库
 */
public class FileTask implements Runnable {
    private FileDownBean mBean;
    private FileTaskListening mListening;//下载监听
    private OkHttpClient mOkHttpClient;
    private int erroCode;//下载线程出错错误码
    private String erroMsg;//下载线程出错原因描述
    private HttpDownCore mHttpDownCore = new FileHttpDwon();//默认使用okhttp下载文件
    private Long toolSize = 0L;//文件总大小
    private Long completedSize = 0L;//当前下载大小
    private Handler mHandler = new Handler();

    public FileTask(FileDownBean mBean) {
        this.mBean = mBean;
    }

    @Override
    public void run() {
        try {
            File outputFile = new File(mBean.getSaveDirPath());
            if (!outputFile.exists()) {
                outputFile.createNewFile();
            }
            completedSize = outputFile.length();
            mHttpDownCore.setDownFlag(true);
            mHttpDownCore.httpDown(mOkHttpClient, FileTask.this);//进入网络请求断点下载文件
        } catch (Exception ex) {
            setErroCode(DownloadStatus.DOWNLOAD_ERROR_CODE_1);
            setErroMsg(ex.toString());
        } finally {
            if (erroCode == DownloadStatus.DOWNLOAD_ERROR_CODE_1) {
                onError();
            }
        }

    }

    public void onDownloading() {
        if (null != mListening) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mListening.onDownloading(FileTask.this);
                }
            });
        }
    }

    public void onPause() {
        mHttpDownCore.setDownFlag(false);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mListening.onPause(FileTask.this);
            }
        });
    }

    public void onCancel() {
        mHttpDownCore.setDownFlag(false);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mListening.onCancel(FileTask.this);
            }
        });
    }

    public void onCompleted() {
        mHttpDownCore.setDownFlag(false);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mListening.onCompleted(FileTask.this);
            }
        });
        FileDownManager.getInstance().getmTaskList().remove(mBean.getDownloadId());
    }

    public void onError() {
        if (null != mListening) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mListening.onError(FileTask.this);
                }
            });

        }
        FileDownManager.getInstance().getmTaskList().remove(mBean.getDownloadId());
    }

    public void setListening(FileTaskListening mListening) {
        this.mListening = mListening;
    }

    public void setHttpClient(OkHttpClient mOkHttpClient) {
        if (null == this.mOkHttpClient) {
            this.mOkHttpClient = mOkHttpClient;
        }
    }

    public int getErroCode() {
        return erroCode;
    }

    public void setErroCode(int erroCode) {
        this.erroCode = erroCode;
    }

    public String getErroMsg() {
        return erroMsg;
    }

    public void setErroMsg(String erroMsg) {
        this.erroMsg = erroMsg;
    }

    public FileDownBean getmBean() {
        return mBean;
    }

    public void setmBean(FileDownBean mBean) {
        this.mBean = mBean;
    }

    public HttpDownCore getmHttpDownCore() {
        return mHttpDownCore;
    }

    public void setmHttpDownCore(HttpDownCore mHttpDownCore) {
        this.mHttpDownCore = mHttpDownCore;
    }

    /***
     * 获得进度数
     * @return
     */
    public float getPercent() {
        return toolSize == null || toolSize <= 0 ? 0 : completedSize * 100 / toolSize;
    }


    public Long getToolSize() {
        return null == toolSize ? 0 : toolSize;
    }

    public void setToolSize(Long toolSize) {
        this.toolSize = toolSize;
    }

    public Long getCompletedSize() {
        return null == completedSize ? 0 : completedSize;
    }

    public void setCompletedSize(Long completedSize) {
        this.completedSize = completedSize;
    }
}
