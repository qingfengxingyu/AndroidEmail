package com.team.cores.basecommon.recyclerview;

import android.content.Context;

import com.team.cores.basecommon.recyclerview.base.MultiTypeSupport;
import com.team.cores.basecommon.recyclerview.base.ViewHolder;

import java.util.Collection;
import java.util.List;

/**
 * Created by xmf on 2017/6/21
 * RecyclerView 基类封装
 */
public abstract class BaseRvAdapter<T> extends MultiItemTypeAdapter<T> {

    public BaseRvAdapter(Context context, Collection<T> data, int layoutId) {//单布局
        super(context, data, layoutId);
    }

    public BaseRvAdapter(Context context, Collection<T> data, MultiTypeSupport<T> multiTypeSupport) {//多布局
        super(context, data, multiTypeSupport);
    }

    /**
     * 设置数据
     *
     * @param datas
     */
    public void setDataList(Collection<T> datas) {
        mDatas.clear();
        mDatas.addAll(datas);
        notifyDataSetChanged();
    }

    /**
     * 添加数据
     *
     * @param list
     */
    public void addAll(Collection<T> list) {
        int lastIndex = this.mDatas.size();
        if (this.mDatas.addAll(list)) {
            notifyItemRangeInserted(lastIndex, list.size());
        }
    }

    /**
     * 移除某个位置的数据
     *
     * @param position
     */
    public void remove(int position) {
        if (null != mDatas && mDatas.size() > 0) {
            mDatas.remove(position);
            //删除动画
            notifyItemRemoved(position);
            //解决notifyItemRemoved postion错位的问题
            //原因是移除之后，没有重新刷新onBindViewHolder，导致监听的postion还是之前的。
            //解决：
            //在移动动画之后，重新刷新onBindViewHolder，使用notifyItemRangeChanged方法
            if (position != mDatas.size()) {
                notifyItemRangeChanged(position, mDatas.size() - position);
            }
        }
    }

    /**
     * 移除某个对象
     *
     * @param object
     */
    public void remove(Object object) {
        if (this.mDatas.size() > 0) {
            mDatas.remove(object);
            notifyDataSetChanged();
        }
    }

    /**
     * 清空数据
     */
    public void clear() {
        mDatas.clear();
        notifyDataSetChanged();
    }

    public void initItem(ViewHolder holder, T item, int position) {
    }

    public void initItem(ViewHolder holder, T item) {
    }
}
