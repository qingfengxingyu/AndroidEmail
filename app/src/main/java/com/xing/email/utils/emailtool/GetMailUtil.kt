package com.xing.email.utils.emailtool

import java.util.Date
import java.util.Properties

import javax.mail.Folder
import javax.mail.Message
import javax.mail.MessagingException
import javax.mail.NoSuchProviderException
import javax.mail.Session
import javax.mail.Store

/**
 * author:xmf
 * date:2018/11/7 0007
 * description:收取邮件
 */
object GetMailUtil {
    fun getEmail() {
        val protocol = "pop3"
        val isSSL = true
        val host = "pop.qq.com"
        val port = 995
        val username = "1282061691@qq.com"
        val password = "retwitgvfcalfffc"
        val props = Properties()
        props["mail.pop3.ssl.enable"] = isSSL
        props["mail.pop3.host"] = host
        props["mail.pop3.port"] = port
        val session = Session.getDefaultInstance(props)
        var store: Store? = null
        var folder: Folder? = null
        try {
            store = session.getStore(protocol)
            store!!.connect(username, password)

            folder = store.getFolder("INBOX")
            folder!!.open(Folder.READ_ONLY)

            val size = folder.messageCount
            val message = folder.getMessage(size)

            val from = message.from[0].toString()
            val subject = message.subject
            val date = message.sentDate

            println("From: $from")
            println("Subject: $subject")
            println("Date: $date")
        } catch (e: NoSuchProviderException) {
            e.printStackTrace()
        } catch (e: MessagingException) {
            e.printStackTrace()
        } finally {
            try {
                folder?.close(false)
                store?.close()
            } catch (e: MessagingException) {
                e.printStackTrace()
            }

        }

        println("接收完毕！")
    }
}