package com.team.cores.filedowm.impl;

import com.team.cores.dbmode.download.DownloadStatus;
import com.team.cores.filedowm.cores.FileTask;
import com.team.cores.filedowm.cores.ShopFileTask;
import com.team.cores.filedowm.imp.HttpDownCore;
import com.team.cores.filedowm.utils.DaoFileUtils;
import com.team.cores.utils.CloseUtils;
import com.team.cores.utils.LogTools;
import com.team.cores.utils.tools.LogTeamUtils;

import org.apache.http.util.TextUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.RandomAccessFile;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/***
 *普通文件 OkHttpClient 下载实现
 */
public class FileHttpDwon implements HttpDownCore {
    private final static String TAG = "HttpFileCommon_xmf";
    private RandomAccessFile inputFile;//断点写入文件
    private InputStream inputStream = null;
    private BufferedInputStream bis = null;
    private int UPDATE_SIZE = 50 * 1024; // The database is updated once every
    private long oldTime;
    final int maxResumNum = 3;
    int nowNum = 0;
    private float oldPercent = -1;
    private boolean downFlag;


    @Override
    public void httpDown(OkHttpClient httpClient, ShopFileTask mTask) {
    }

    @Override
    public void httpDown(ShopFileTask mTask) {

    }

    @Override
    public void setDownFlag(boolean downFlag) {
        this.downFlag = downFlag;
    }

    @Override
    public void httpDown(OkHttpClient httpClient, FileTask mTask) {
        try {
            if (!downFlag) {
                return;
            }
            File outputFile = new File(mTask.getmBean().getSaveDirPath());
            if (!outputFile.exists()) {
                outputFile.createNewFile();
            }
            inputFile = new RandomAccessFile(outputFile, "rwd");
            nowNum++;
            if (nowNum > maxResumNum) {
                mTask.setErroCode(DownloadStatus.DOWNLOAD_ERROR_CODE_1);
                mTask.setErroMsg("服务器出错");
                nowNum = 0;
                return;
            }
            Request request = new Request.Builder()
                    .url(mTask.getmBean().getUrl())
                    .header("Range", "bytes=" + mTask.getCompletedSize() + "-")
                    .build();

            LogTeamUtils.e(TAG, "文件开始下载___下载地址___:" + mTask.getmBean().getUrl());
            Response response = null;
            response = httpClient.newCall(request).execute();
            mTask.setErroCode(response.code());
            if (response != null) {
                LogTeamUtils.e(TAG, "response.isSuccessful():" + response.isSuccessful());
            } else {
                LogTeamUtils.e(TAG, "response is NUll:");
            }
            LogTeamUtils.e(TAG, "获取响应码 code is:" + mTask.getErroCode());
            if (response != null && response.isSuccessful() && (200 == mTask.getErroCode() || 206 == mTask.getErroCode())) {
                ResponseBody responseBody = response.body();
                if (responseBody != null) {
                    if (mTask.getToolSize() <= 0) {
                        mTask.setToolSize(responseBody.contentLength());
                    }
                    LogTeamUtils.e(TAG, "toolSize is :" + mTask.getToolSize());
                    if (mTask.getToolSize() > 0) {
                        if (mTask.getCompletedSize() < mTask.getToolSize()) {
                            if (TextUtils.isEmpty(response.header("Content-Range"))) {
                                LogTeamUtils.e(TAG, "Content-Range is null.服务器不支持断点下载");
                                // 返回的没有Content-Range 不支持断点下载 需要重新下载
                                File alreadyDownloadedFile = new File(mTask.getmBean().getSaveDirPath());
                                if (alreadyDownloadedFile.exists()) {
                                    alreadyDownloadedFile.delete();
                                }
                                inputFile = new RandomAccessFile(mTask.getmBean().getSaveDirPath(),
                                        "rwd");
                                mTask.setCompletedSize(0L);
                            }
                            inputFile.seek(mTask.getCompletedSize());
                            inputStream = responseBody.byteStream();
                            bis = new BufferedInputStream(inputStream);
                            byte[] buffer = new byte[2 * 1024];
                            int length = 0;
                            int buffOffset = 0;
                            oldTime = System.currentTimeMillis();
                            while ((length = bis.read(buffer)) > 0 && downFlag) {
                                inputFile.write(buffer, 0, length);
                                mTask.setCompletedSize(mTask.getCompletedSize() + length);
                                buffOffset += length;
                                if (buffOffset >= UPDATE_SIZE) {
                                    buffOffset = 0;
                                }
                                if ((System.currentTimeMillis() - oldTime) >= 500) {//3秒更新一次UI
                                    oldTime = System.currentTimeMillis();
                                    if (oldPercent != mTask.getPercent()) {
                                        oldPercent = mTask.getPercent();
                                        mTask.onDownloading();
                                    }
                                }
                            }
                        }
                        mTask.onDownloading();
                        mTask.onCompleted();

                    } else {
                        mTask.setErroCode(DownloadStatus.DOWNLOAD_ERROR_CODE_1);
                        mTask.setErroMsg("toolSize is :" + mTask.getToolSize());
                    }

                } else {
                    mTask.setErroCode(DownloadStatus.DOWNLOAD_ERROR_CODE_1);
                    mTask.setErroMsg("responseBody is null");
                    LogTeamUtils.e(TAG, "responseBody is null");
                }
            } else {
                ResponseBody responseBody = response.body();
                if (responseBody != null) {
                    inputStream = responseBody.byteStream();
                    bis = new BufferedInputStream(inputStream);
                    String erroMsg = new String(DaoFileUtils.getInstance().readInputStream(bis), "UTF-8") + ".";
                    mTask.setErroMsg(erroMsg);
                    LogTeamUtils.e(TAG, erroMsg);
                } else {
                    mTask.setErroMsg("服务器未知错误");
                }
                mTask.setErroCode(DownloadStatus.DOWNLOAD_ERROR_CODE_1);
            }
        } catch (Exception e) {
            LogTeamUtils.e(TAG, e.toString());
            String msg = e.toString();
            if (msg.indexOf("unexpected end of stream") > 0) {
                if (downFlag) {
                    httpDown(httpClient, mTask);
                }
            } else {
                downFlag = false;
                mTask.setErroCode(DownloadStatus.DOWNLOAD_ERROR_CODE_1);
                mTask.setErroMsg(e.toString());
            }
            LogTeamUtils.e(TAG, "downloadTask downImg is erro is:" + e.toString());
        } finally {
            CloseUtils.closeQuietly(inputFile);
            CloseUtils.closeQuietly(inputStream);
            CloseUtils.closeQuietly(bis);
        }
    }
}
