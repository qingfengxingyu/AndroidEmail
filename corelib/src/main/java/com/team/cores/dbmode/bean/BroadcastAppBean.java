package com.team.cores.dbmode.bean;

import java.io.Serializable;

/**
 * Description:应用的安装与卸载
 * Data: 2017/10/12 0012 下午 3:49
 *
 * @author: xmf
 */
public class BroadcastAppBean implements Serializable {
    private static final long serialVersionUID = 1111111;
    public int downNum;//1安装2卸载3更新卸载
    public String packName;//包名
}
