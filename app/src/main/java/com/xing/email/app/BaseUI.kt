package com.xing.email.app

import android.app.ProgressDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import com.xing.email.utils.Preference
import com.xing.email.utils.SPKeyConstants
import com.xing.email.utils.ToolUtils
import me.imid.swipebacklayout.lib.app.SwipeBackActivity


abstract class BaseUI : SwipeBackActivity() {
    protected var mContext: Context = this
    protected var loadingDialog: ProgressDialog? = null
    protected lateinit var sp: SharedPreferences
    protected var email_sp: String by Preference(this, SPKeyConstants.EMAIL_SP, "")//存放最近一次邮箱地址
    protected var pwd_sp: String by Preference(this, SPKeyConstants.PWD_SP, "")//存放最近一次密码
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MyApp.getInstance()?.addActivity(this)
        sp = getSharedPreferences("mail", Context.MODE_PRIVATE);
        //右划关闭当前页面
        ToolUtils.settingSwipeBack(swipeBackLayout)
        getSupportActionBar()?.hide();
        actionBar?.hide()
        mContext = this
    }

    override fun onDestroy() {
        super.onDestroy()
        if (null != loadingDialog) {
            loadingDialog = null
        }
        MyApp.getInstance()?.removeActivity(this)
    }

    fun initAll() {
        initData()
        initEven()
    }

    /***
     * 初始化数据
     */
    abstract fun initData()

    /***
     * 初始化事件
     */
    abstract fun initEven()

    /**
     * 获取加载对话框
     *
     */
    protected fun showProgressDialog() {
        showProgressDialog("系统提示", "等待中...")
    }

    /***
     * 获取加载对话框
     */
    protected fun showProgressDialog(msg: String) {
        showProgressDialog("系统提示", msg)
    }

    /***
     *  获取加载对话框
     *  title ---标题
     *  msg---消息提示内容
     */
    protected fun showProgressDialog(title: String, msg: String) {
        if (null == loadingDialog) {
            loadingDialog = ProgressDialog(this@BaseUI)
            loadingDialog?.setTitle(title)
            loadingDialog?.setMessage(msg)
            loadingDialog?.isIndeterminate = true
            loadingDialog?.setCancelable(false)
        }
        loadingDialog?.show()
    }

    /**
     * 关闭加载对话框
     */
    protected fun hideProgressDialog() {
        loadingDialog?.dismiss()
    }

}
// 代码中得到dp值
// final int pageMargin = (int) TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());