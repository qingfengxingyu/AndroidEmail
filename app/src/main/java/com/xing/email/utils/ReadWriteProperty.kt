package com.xing.email.utils

import kotlin.reflect.KProperty

/**
 *author:xmf
 *date:2018/7/3 0003
 *description:SharedPreferences工具接口类
 */
public interface ReadWriteProperty<in R, T> {

    public operator fun getValue(thisRef: R, property: KProperty<*>): T

    public operator fun setValue(thisRef: R, property: KProperty<*>, value: T)
}