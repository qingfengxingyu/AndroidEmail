package com.team.cores.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;

import com.team.cores.theme.ThemeUtils;

import com.team.cores.R;
import com.team.cores.dialog.info.CallBackDialog;

/***
 * 错误信息dialog确认框
 */
public class Error_Dialog extends Dialog {
    private TextView ye_btn;
    private TextView msg_neirong;
    private TextView msg_title;
    private Context context;
    private CallBackDialog callBackDialog;
    private View line1;


    public void setCallBackDialog(CallBackDialog callBackDialog) {
        this.callBackDialog = callBackDialog;
    }

    /***
     * 设置按钮的字值
     *
     * @param yesText 确定
     */
    public void setTwo_btn(String yesText) {
        ye_btn.setText(yesText);
    }

    /***
     * 设置标题
     *
     * @param myTitile
     */
    public void setMyTitile(String myTitile) {
        msg_title.setText(myTitile);
    }

    /***
     * 设置提示信息
     *
     * @param myContent
     */
    public void setErrorMsg(String myContent) {
        if (null == myContent || "".equals(myContent)) {
            msg_neirong.setText("");
            return;
        }
        msg_neirong.setText(myContent);
    }

    public Error_Dialog(Context context) {
        super(context, R.style.teams_dialog);
        this.context = context;
        show();
    }

    public Error_Dialog(Context context, int theme) {
        super(context, theme);
        this.context = context;
        show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View v = getLayoutInflater().inflate(R.layout.erro_dialog, null);
        LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        setContentView(v, lp);
        this.getWindow().setBackgroundDrawable(new ColorDrawable());//解决5.0dialog背景黑框问题
        ye_btn = (TextView) v.findViewById(R.id.ye_btn);
        msg_title = (TextView) v.findViewById(R.id.msg_title);
        line1 = (View) v.findViewById(R.id.line1);
        ThemeUtils.setTextViewText(msg_title);
        ThemeUtils.setTextViewBag(line1);
        msg_neirong = (TextView) v.findViewById(R.id.msg_neirong);
        msg_neirong.setMovementMethod(ScrollingMovementMethod.getInstance());
        ye_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dismiss();
                if (null != callBackDialog) {
                    callBackDialog.yes_btn("");
                }
            }
        });

    }


}
