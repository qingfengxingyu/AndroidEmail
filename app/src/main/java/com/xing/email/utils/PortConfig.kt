package com.xing.email.utils

/**
 *author:xmf
 *date:2018/11/6 0006
 *description:三种类型账户的默认端口
 */
object PortConfig {
    //POP3
    val popPort = "110";
    val popPortSsl = "995";
    //IMAP
    val imapPort = "143";
    val imapPortSsl = "993";
    //exchange
    val exchangePort = "80";
    val exchangePortSsl = "443";
    //SMTP
    val smtpPort = "25";
    val smtpPortSsl = "465";
}