package com.team.cores.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.GridView;

/**
 * 不能滚动的GridView
 * Created by wfx on 2017/7/13.
 */

public class MoreIndexNoSlideGridView extends GridView {
    public MoreIndexNoSlideGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if(ev.getAction() == MotionEvent.ACTION_MOVE){
        return true;//true:禁止滚动  
        }
        return super.dispatchTouchEvent(ev);
    }
}
