package com.team.cores.httputils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.team.cores.httputils.jsonbean.PaginationBean;
import com.team.cores.httputils.jsonbean.ResponseBean;
import com.team.cores.utils.tools.LogTeamUtils;

import org.json.JSONObject;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import rx.subscriptions.CompositeSubscription;

/****
 * mode基类
 */
public abstract class BaseMode<T> {

    public Object body;
    public List<T> mDataList = new ArrayList<>();
    public T mData;
    public PaginationBean pagination;//翻页数据
    public Gson gson = new Gson();
    /***
     * 请求的返回结果
     */
    public JSONObject relust = null;
    /***
     * 返回结果1成功
     */
    public String retcode = "";
    /***
     * 错误信息提示
     */
    public String retmsg = "";

    /**
     * 获得baseUrl请求地址
     */
    public String getBaseUrl() {
        String baseUrl = "";
        return baseUrl;
    }


    /**
     * 获得请求地址
     */
    public abstract String getUrl();

    /**
     * 获得请求参数获得
     */
    public abstract JSONObject getParams();

    /**
     * 请求结果body解析
     */
    public abstract void setData(Object jsonObject);


    /**
     * 请求结果统一解析
     */
    public void parseObject(ResponseBean responseBean) {
        try {
            retcode = responseBean.getRetcode();
            retmsg = responseBean.getRetmsg();
//            if ((HttpUtils.Token_GuoQi + "").equals(retcode)) {
//                if (null != ProjectConfigs.getInstance().getApp()) {
//                    Intent intent = new Intent(MipConstant.ACTION_SESSION_TIMEOUT);
//                    intent.putExtra("mine", true);
//                    ProjectConfigs.getInstance().getApp().sendBroadcast(intent, MipConstant.PEMISSION);
//                    return;
//                }
//            }
            if ("1".equals(retcode)) {
                if (null != responseBean.getPagination()) {
                    pagination = gson.fromJson(responseBean.getPagination().toString(),
                            PaginationBean.class);
                }
            }
            body = responseBean.getBody();
            setData(body);
        } catch (Exception e) {
            LogTeamUtils.e(e.toString());
        }
    }

    /**
     * 请求结果统一解析(老接口)
     */
    public void parseObject(Object object) {
        try {
//            if (object instanceof RestResult) {
//                RestResult restResult = (RestResult) object;
//                if (HttpUtils.Token_GuoQi == restResult.getStatusCode()) {
//                    if (null != ProjectConfigs.getInstance().getApp()) {
//                        Intent intent = new Intent(MipConstant.ACTION_SESSION_TIMEOUT);
//                        intent.putExtra("mine", true);
//                        ProjectConfigs.getInstance().getApp().sendBroadcast(intent, MipConstant.PEMISSION);
//                        return;
//                    }
//                }
//                if (restResult.isSuccessful()) {
//                    setData(restResult.getJSONObject());
//                }
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     * 获得List实体
     */
    public void setMDataList() {
        try {
            if (null != body) {
//                Type type = new TypeToken<List<T>>() {
//                }.getType();
                JsonArray arry = new JsonParser().parse(body.toString()).getAsJsonArray();
                //获取泛型实际的Class类型
                Class clz = this.getClass();
                ParameterizedType ty = (ParameterizedType) clz.getGenericSuperclass();
                Type[] types = ty.getActualTypeArguments();
                clz = (Class<T>) types[0];

                mDataList.clear();
                for (JsonElement jsonElement : arry) {
                    mDataList.add((T) gson.fromJson(jsonElement, clz));
                }
            }
        } catch (Exception e) {
            LogTeamUtils.e(e.toString());
        }
    }

    /***
     * 获得Bean实体
     */
    public void setMDataValue() {
        try {
            if (null != body) {
                mData = gson.fromJson(body.toString(), (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]);
            }
        } catch (Exception e) {
            LogTeamUtils.e(e.toString());
        }
    }

    /***
     * Get请求数据
     *
     * @param httpListingInfo
     * @return
     */
    public CompositeSubscription getHttp(HttpListingInfo httpListingInfo) {
        return getHttp(httpListingInfo, false);
    }

    /***
     * Get请求数据
     *
     * @param httpListingInfo
     * @param headerJson      请求header
     * @return
     */
    public CompositeSubscription getHttp(HttpListingInfo httpListingInfo, final JSONObject headerJson) {
        return getHttp(httpListingInfo, false, headerJson);
    }

    /***
     * Get请求数据
     *
     * @param httpListingInfo
     * @param flag            true 老接口 false 新接口
     * @return
     */
    public CompositeSubscription getHttp(HttpListingInfo httpListingInfo, boolean flag) {
        return HttpCommonFrame.getMetod(this, httpListingInfo, flag);
    }

    /***
     * Get请求数据
     *
     * @param httpListingInfo
     * @param flag            true 老接口 false 新接口
     * @param headerJson      请求header
     * @return
     */
    public CompositeSubscription getHttp(HttpListingInfo httpListingInfo, boolean flag, final JSONObject headerJson) {
        return HttpCommonFrame.getMetod(this, httpListingInfo, flag, headerJson);
    }

    /***
     * POST请求数据
     *
     * @param httpListingInfo
     * @return
     */
    public CompositeSubscription postHttp(HttpListingInfo httpListingInfo) {
        return postHttp(httpListingInfo, false);
    }

    /***
     * POST请求数据
     *
     * @param httpListingInfo
     * @param headerJson      请求header
     * @return
     */
    public CompositeSubscription postHttp(HttpListingInfo httpListingInfo, final JSONObject headerJson) {
        return postHttp(httpListingInfo, false, headerJson);
    }

    /***
     * POST请求数据
     *
     * @param httpListingInfo
     * @param flag            true 老接口 false 新接口
     * @return
     */
    public CompositeSubscription postHttp(HttpListingInfo httpListingInfo, boolean flag) {
        return HttpCommonFrame.postMetod(this, httpListingInfo, flag);
    }

    /***
     * POST请求数据
     *
     * @param httpListingInfo
     * @param flag            true 老接口 false 新接口
     * @param headerJson      请求header
     * @return
     */
    public CompositeSubscription postHttp(HttpListingInfo httpListingInfo, boolean flag, final JSONObject headerJson) {
        return HttpCommonFrame.postMetod(this, httpListingInfo, flag, headerJson);
    }
}
