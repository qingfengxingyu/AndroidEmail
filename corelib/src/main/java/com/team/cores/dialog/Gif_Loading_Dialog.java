package com.team.cores.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

import com.team.cores.R;
import com.team.cores.views.LodingCircleView;

/***
 * Created by xmf on 2017/06/19.
 * 上传图片进度条
 */
public class Gif_Loading_Dialog extends Dialog {
    public Context context;
    private LodingCircleView loading_view;

    public Gif_Loading_Dialog(Context context, int theme) {
        super(context, theme);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View v = getLayoutInflater().inflate(R.layout.gif_loading_layout, null);
        loading_view = (LodingCircleView) v.findViewById(R.id.loading_view);
        LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        setContentView(v, lp);
    }

    /***
     * 启动动画
     */
    public void startAnimAutomatic() {
        if (null != loading_view) {
            loading_view.startAnimAutomatic(true);
        }
    }

    /***
     * 设置动画进度和停止动画
     *
     * @param progerss
     * @param fillIn
     */
    public void setProgerss(int progerss, boolean fillIn) {
        if (null != loading_view) {
            loading_view.setProgerss(progerss, fillIn);
        }
    }

}
