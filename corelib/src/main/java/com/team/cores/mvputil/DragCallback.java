package com.team.cores.mvputil;
/**
 * 首页更多页面的接口
 * Created by wfx on 2017/6/23.
 *
 */

public interface DragCallback {
	void startDrag(int position);
	void endDrag(int position);
}
