package com.team.cores.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.team.cores.R;
import com.team.cores.utils.tools.LogTeamUtils;
import com.team.cores.utils.tools.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


@SuppressLint("NewApi")
public class ImageLoadUtils {
    /***
     * 图片的最大值(单位KB)
     */
    public static final int maxSizeForImg = 200;

    public static Bitmap minYaSuo(Bitmap bmp, String picName) {
        LogTeamUtils.e("minYaSuo size is--" + bmp.getByteCount() / 1024
                + ";all--" + bmp.getByteCount() + ";width--" + bmp.getWidth()
                + ";height--" + bmp.getHeight());
        saveBitmap(bmp, picName);
        if (null != bmp) {
            bmp.recycle();
            bmp = null;
            System.gc();
        }
        return getImgBitmap(FileSaveTool.getInstance().getBaseFile() + File.separator + picName);
    }

    public static Bitmap crop_minYaSuo(Bitmap bmp, String picName) {
        saveBitmap(bmp, picName);
        if (null != bmp) {
            bmp.recycle();
            bmp = null;
            System.gc();
        }
        return getImgBitmap(FileSaveTool.getInstance().getBaseFile() + File.separator + picName);
    }

    /***
     * 图片按比例大小压缩方法（根据路径获取图片并压缩）
     *
     * @param srcPath
     * @return
     */
    public static Bitmap getImgBitmap(String srcPath) {
        if (StringUtils.isEmpty(srcPath)) {
            return null;
        }
        BitmapFactory.Options newOpts = new BitmapFactory.Options();
        // 开始读入图片，此时把options.inJustDecodeBounds 设回true了
        newOpts.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(srcPath, newOpts);// 此时返回bm为空

        newOpts.inJustDecodeBounds = false;
        int w = newOpts.outWidth;
        int h = newOpts.outHeight;
        float ww = 720f;// 这里设置宽度为720f
        float hh = 1280f;// 这里设置高度为1280f
        // 缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
        int be = 1;// be=1表示不缩放
        if (w > h && w > ww) {// 如果宽度大的话根据宽度固定大小缩放
            be = (int) (newOpts.outWidth / ww);
        } else if (w < h && h > hh) {// 如果高度高的话根据宽度固定大小缩放
            be = (int) (newOpts.outHeight / hh);
        }
        if (be <= 0)
            be = 1;
        newOpts.inSampleSize = be;// 设置缩放比例
        // 重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
        bitmap = BitmapFactory.decodeFile(srcPath, newOpts);
        return compressImage(bitmap);// 压缩好比例大小后再进行质量压缩
    }

    /***
     * 图片按比例大小压缩方法（根据路径获取图片并压缩）
     *
     * @param srcPath
     * @return
     */
    public static Bitmap getImgBitmap(String srcPath, int width, int heghit) {
        if (StringUtils.isEmpty(srcPath)) {
            return null;
        }
        BitmapFactory.Options newOpts = new BitmapFactory.Options();
        // 开始读入图片，此时把options.inJustDecodeBounds 设回true了
        newOpts.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(srcPath, newOpts);// 此时返回bm为空

        newOpts.inJustDecodeBounds = false;
        int w = newOpts.outWidth;
        int h = newOpts.outHeight;
        float ww = width;// 这里设置宽度为720f
        float hh = heghit;// 这里设置高度为1280f
        // 缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
        int be = 1;// be=1表示不缩放
        if (w > h && w > ww) {// 如果宽度大的话根据宽度固定大小缩放
            be = (int) (newOpts.outWidth / ww);
        } else if (w < h && h > hh) {// 如果高度高的话根据宽度固定大小缩放
            be = (int) (newOpts.outHeight / hh);
        }
        if (be <= 0)
            be = 1;
        newOpts.inSampleSize = be;// 设置缩放比例
        // 重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
        bitmap = BitmapFactory.decodeFile(srcPath, newOpts);
        return compressImage(bitmap);// 压缩好比例大小后再进行质量压缩
    }

    /***
     * 质量压缩法
     *
     * @param image
     * @return
     */
    public static Bitmap compressImage(Bitmap image) {
        if (null == image) {
            return null;
        }
        LogTeamUtils.e("begin size is--" + image.getByteCount() / 1024
                + ";all--" + image.getByteCount() + ";width--"
                + image.getWidth() + ";height--" + image.getHeight());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(CompressFormat.JPEG, 100, baos);// 质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
        LogTeamUtils.e("baos begin size is--" + baos.toByteArray().length
                / 1024 + ";all--" + baos.toByteArray().length);
        int options = 100;
        if (baos.toByteArray().length / 1024 > 1024) {// 判断如果图片大于1M,进行压缩避免在生成图片（BitmapFactory.decodeStream）时溢出
            baos.reset();// 重置baos即清空baos
            options = 50;
            image.compress(CompressFormat.JPEG, options, baos);// 这里压缩50%，把压缩后的数据存放到baos中
        }
        while (baos.toByteArray().length / 1024 > maxSizeForImg) { // 循环判断如果压缩后图片是否大于maxSizeForImgkb,大于继续压缩
            baos.reset();// 重置baos即清空baos
            options -= 10;// 每次都减少5
            image.compress(CompressFormat.JPEG, options, baos);// 这里压缩options%，把压缩后的数据存放到baos中

        }
        if (null != image) {
            image.recycle();
            image = null;
            System.gc();
        }
        LogTeamUtils.e("options is--" + options);
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());// 把压缩后的数据baos存放到ByteArrayInputStream中
        LogTeamUtils.e("baos end size is--" + baos.toByteArray().length
                / 1024 + ";all--" + baos.toByteArray().length);
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);// 把ByteArrayInputStream数据生成图片
        saveBitmap(bitmap, "002.jpg");
        LogTeamUtils.e("end size is--" + bitmap.getByteCount() / 1024
                + ";all--" + bitmap.getByteCount() + ";width--"
                + bitmap.getWidth() + ";height--" + bitmap.getHeight());

        if (null != isBm) {
            try {
                isBm.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return bitmap;
    }

    /**
     * 保存方法
     */
    public static void saveBitmap(Bitmap bmp, String picName) {
        if (null == bmp) {
            return;
        }
        File f = new File(FileSaveTool.getInstance().getBaseFile(), picName);
        if (f.exists()) {
            f.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(f);
            bmp.compress(CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (f.exists()) {
                LogTeamUtils.e("saveBitmap size is--" + f.length() / 1024
                        + ";all--" + f.length());
            }
        }

    }

    /***
     * 质量压缩法
     *
     * @param image
     * @param maxSize 图片的最大值(单位KB)
     * @return
     */
    public static Bitmap compressImage(Bitmap image, int maxSize) {
        if (null == image) {
            return null;
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(CompressFormat.JPEG, 100, baos);// 质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
        int options = 100;
        LogTeamUtils.e("begin size is--" + baos.toByteArray().length);
        if (baos.toByteArray().length / 1024 > 1024) {// 判断如果图片大于1M,进行压缩避免在生成图片（BitmapFactory.decodeStream）时溢出
            baos.reset();// 重置baos即清空baos
            options = 50;
            image.compress(CompressFormat.JPEG, options, baos);// 这里压缩50%，把压缩后的数据存放到baos中
        }
        while (baos.toByteArray().length / 1024 > maxSize) { // 循环判断如果压缩后图片是否大于maxSizeForImgkb,大于继续压缩
            baos.reset();// 重置baos即清空baos
            options -= 5;// 每次都减少5
            image.compress(CompressFormat.JPEG, options, baos);// 这里压缩options%，把压缩后的数据存放到baos中

        }
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());// 把压缩后的数据baos存放到ByteArrayInputStream中
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);// 把ByteArrayInputStream数据生成图片
        if (null != image) {
            image.recycle();
            image = null;
            System.gc();
        }
        LogTeamUtils.e("end size is--" + bitmap.getByteCount());
        return bitmap;
    }

    /***
     * 图片按比例大小压缩方法（根据Bitmap图片压缩）
     *
     * @param image
     * @return
     */
    public Bitmap comp(Bitmap image) {
        if (null == image) {
            return null;
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(CompressFormat.JPEG, 100, baos);
        if (baos.toByteArray().length / 1024 > 1024) {// 判断如果图片大于1M,进行压缩避免在生成图片（BitmapFactory.decodeStream）时溢出
            baos.reset();// 重置baos即清空baos
            image.compress(CompressFormat.JPEG, 50, baos);// 这里压缩50%，把压缩后的数据存放到baos中
        }
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());
        BitmapFactory.Options newOpts = new BitmapFactory.Options();
        // 开始读入图片，此时把options.inJustDecodeBounds 设回true了
        newOpts.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, newOpts);
        newOpts.inJustDecodeBounds = false;
        int w = newOpts.outWidth;
        int h = newOpts.outHeight;
        // 现在主流手机比较多是800*480分辨率，所以高和宽我们设置为
        float hh = 800f;// 这里设置高度为800f
        float ww = 480f;// 这里设置宽度为480f
        // 缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
        int be = 1;// be=1表示不缩放
        if (w > h && w > ww) {// 如果宽度大的话根据宽度固定大小缩放
            be = (int) (newOpts.outWidth / ww);
        } else if (w < h && h > hh) {// 如果高度高的话根据宽度固定大小缩放
            be = (int) (newOpts.outHeight / hh);
        }
        if (be <= 0)
            be = 1;
        newOpts.inSampleSize = be;// 设置缩放比例
        newOpts.inPreferredConfig = Config.RGB_565;// 降低图片从ARGB888到RGB565
        // 重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
        isBm = new ByteArrayInputStream(baos.toByteArray());
        bitmap = BitmapFactory.decodeStream(isBm, null, newOpts);
        return compressImage(bitmap);// 压缩好比例大小后再进行质量压缩
    }

    public static Bitmap resizeImage(Bitmap bitmap, int w, int h) {
        if (null == bitmap) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        float scaleWidth = w / width;
        float scaleHeight = h / height;

        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);

        return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
    }

    /***
     * 将字符串转换成Bitmap类型
     *
     * @param string
     * @return
     */
    public static Bitmap stringtoBitmap(String string) {
        if (StringUtils.isEmpty(string)) {
            return null;
        }
        Bitmap bitmap = null;
        try {
            byte[] bitmapArray;
            bitmapArray = Base64.decode(string, Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0,
                    bitmapArray.length);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    /***
     * 将Bitmap转换成字符串
     *
     * @param bitmap
     * @return
     */
    public static String bitmaptoString(Bitmap bitmap) {
        if (null == bitmap) {
            return null;
        }
        String string = null;
        ByteArrayOutputStream bStream = new ByteArrayOutputStream();
        bitmap.compress(CompressFormat.PNG, 100, bStream);
        byte[] bytes = bStream.toByteArray();
        string = Base64.encodeToString(bytes, Base64.DEFAULT);
        return string;
    }

    /****
     * 裁剪背景图片
     *
     * @param btm
     * @param scaleWidth
     * @param scaleHeight
     * @return
     */
    public static Bitmap changeBtp(Bitmap btm, int scaleWidth, int scaleHeight) {
        if (null == btm) {
            return null;
        }
        btm = Bitmap.createScaledBitmap(btm, scaleHeight * 5 / 8, scaleHeight,
                true);
        btm = Bitmap.createBitmap(btm, 0, 0, scaleWidth, scaleHeight);
        return btm;
    }

    public static int getUserWidth(int scaleWidth) {
        return 31 * scaleWidth / 120;
    }

    public static int getUserHeigth(int scaleWidth) {
        return 31 * 8 * scaleWidth / 600;
    }

    /****
     * 裁剪头像图片
     *
     * @param btm
     * @param scaleWidth
     * @param scaleHeight
     * @return
     */
    public static Bitmap changeUserBtp(Bitmap btm, int scaleWidth,
                                       int scaleHeight) {
        if (null == btm) {
            return null;
        }
        btm = Bitmap.createScaledBitmap(btm, getUserWidth(scaleWidth),
                getUserHeigth(scaleWidth), true);
        // btm = Bitmap.createBitmap(btm, 0, 0, scaleWidth, scaleHeight);
        return btm;
    }

    /***
     * 根据url获得手机sd卡中的图片
     *
     * @return
     */
    public static Bitmap queryBitmap_Sd(String path) {
        if (StringUtils.isEmpty(path)) {
            return null;
        }
        try {

            if (null != path && !"".equals(path)) {
                File picture = new File(path);
                if (picture.exists()) {
                    return ((BitmapDrawable) Drawable.createFromPath(path))
                            .getBitmap();
                }
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /***
     * 判断类是否存在
     *
     * @param className 类名全称
     * @return true 类存在 false 类不存在
     */
    public static boolean checkClass(String className) {
        boolean classFlag = true;
        try {
            Class.forName(className);
        } catch (Exception e) {
            e.printStackTrace();
            classFlag = false;
        }
        return classFlag;
    }

    public static float maxTextSize = 2;

    /**
     * 将sp值转换为px值，保证文字大小不变
     *
     * @param spValue
     * @return
     */
    public static int sp2px(Context context, float spValue) {
        float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        // if (fontScale > maxTextSize) {
        // fontScale = maxTextSize;
        // }
        LogTeamUtils.e("xmf", "spValue---" + spValue + ";fontScale--" + fontScale
                + ";sp--" + (spValue * fontScale + 0.5f));
        return (int) (spValue * fontScale + 0.5f);
    }

    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */
    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /***
     * 根据指定的尺寸加载Bitmap
     *
     * @param reqWidth
     * @param reqHeight
     * @return
     */
    public static synchronized Bitmap decodeSampledBitmapFromStream(
            String srcPath, int reqWidth, int reqHeight) {
        if (StringUtils.isEmpty(srcPath)) {
            return null;
        }
        File file = new File(srcPath);
        if (!file.exists()) {
            return null;
        }
        InputStream in = null;
        try {
            in = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(in, null, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);
        options.inJustDecodeBounds = false;
        Bitmap bitmap = BitmapFactory.decodeStream(in, null, options);
        if (null != in) {
            try {
                in.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return bitmap;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        // 先根据宽度进行缩小
        while (width / inSampleSize > reqWidth) {
            inSampleSize++;
        }
        // 然后根据高度进行缩小
        while (height / inSampleSize > reqHeight) {
            inSampleSize++;
        }
        return inSampleSize;
    }

    /***
     * 加载图片_无默认图片
     *
     * @param mContext  上下文
     * @param imgUr
     * @param imageView
     */
    public static void loadImage(Context mContext, String imgUr, ImageView imageView) {
        if (null != imageView) {
            LazyHeaders headers = new LazyHeaders.Builder().addHeader("sessionId", "sessionId").build();
            Glide.with(mContext).load(new GlideUrl(ToolUtils.getDownFile(imgUr), headers)).error(R.drawable.default_app_icon).into(imageView);
        }
    }

    /***
     * 加载图片_有默认图片_默认不裁剪
     *
     * @param mContext  上下文
     * @param imgUr
     * @param imageView
     */
    public static void loadImage_defualtImg(Context mContext, String imgUr, ImageView imageView) {
        loadImage_defualtImg(mContext, imgUr, imageView, R.drawable.default_app_icon, R.drawable.default_app_icon, 0);
    }

    /***
     * 加载图片_有默认图片
     *
     * @param mContext  上下文
     * @param imgUr
     * @param centerNum 是否裁剪 0 不裁剪 1 centerCrop裁剪 2 fitCenter裁剪
     */
    public static void loadImage_defualtImg(Context mContext, String imgUr, ImageView imageView, int centerNum) {
        loadImage_defualtImg(mContext, imgUr, imageView, R.drawable.default_app_icon, R.drawable.default_app_icon, centerNum);
    }

    /***
     * 加载图片_自定义默认图片_默认不裁剪图片
     * @param mContext
     * @param imgUr 下载地址
     * @param imageView 显示控件
     * @param placeholderID 加载过程资源图片ID
     * @param errorID 加载失败显示资源图片ID
     */
    public static void loadImage_defualtImg(Context mContext, String imgUr, ImageView imageView, int placeholderID, int errorID) {
        loadImage_defualtImg(mContext, imgUr, imageView, placeholderID, errorID, 0);
    }

    /***
     * 加载图片_自定义默认图片
     * @param mContext
     * @param imgUr 下载地址
     * @param imageView 显示控件
     * @param placeholderID 加载过程资源图片ID
     * @param errorID 加载失败显示资源图片ID
     * @param centerNum 是否裁剪 0 不裁剪 1 centerCrop裁剪 2 fitCenter裁剪
     */
    public static void loadImage_defualtImg(Context mContext, String imgUr, ImageView imageView, int placeholderID, int errorID, int centerNum) {
        if (null != imageView) {
            LazyHeaders headers = new LazyHeaders.Builder().addHeader("sessionId", "sessionId").build();
            switch (centerNum) {
                case 0://不裁剪
                    Glide.with(mContext).load(new GlideUrl(imgUr, headers)).dontAnimate().placeholder(placeholderID).error(errorID).into(imageView);
                    break;
                case 1:
                    Glide.with(mContext).load(new GlideUrl(imgUr, headers)).placeholder(placeholderID).error(errorID).centerCrop().into(imageView);
                    break;
                case 2:
                    Glide.with(mContext).load(new GlideUrl(imgUr, headers)).placeholder(placeholderID).error(errorID).fitCenter().into(imageView);
                    break;
                default://不裁剪
                    Glide.with(mContext).load(new GlideUrl(imgUr, headers)).placeholder(placeholderID).error(errorID).into(imageView);
                    break;
            }
        }
    }
}
