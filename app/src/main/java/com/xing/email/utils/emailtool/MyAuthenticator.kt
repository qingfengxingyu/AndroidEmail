package com.xing.email.utils.emailtool

import javax.mail.Authenticator
import javax.mail.PasswordAuthentication

/**
 * author:xmf
 * date:2018/11/6 0006
 * description:认证类
 */
class MyAuthenticator : Authenticator {
    internal var userName: String? = null
    internal var password: String? = null

    constructor() {}

    constructor(username: String?, password: String?) {
        this.userName = username
        this.password = password
    }

    override fun getPasswordAuthentication(): PasswordAuthentication {
        return PasswordAuthentication(userName, password)
    }
}

