package com.xing.email.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.View
import com.luck.picture.lib.PictureSelector
import com.luck.picture.lib.config.PictureConfig
import com.luck.picture.lib.entity.LocalMedia
import com.team.cores.dialog.Email_Cancle_Dialog
import com.team.cores.utils.tools.LogTeamUtils
import com.xing.email.R
import com.xing.email.adapter.GridImageAdapter
import com.xing.email.app.BaseUI
import com.xing.email.app.MyApp
import com.xing.email.utils.emailtool.MailTools
import com.xing.email.views.SpaceItemDecoration
import com.xing.email.views.email.Person
import com.xing.email.views.email.TokenCompleteTextView
import kotlinx.android.synthetic.main.ui_send_email.*
import java.io.File
import java.util.*

/**
 *author:xmf
 *date:2018/11/6 0006
 *description:发送邮件页面
 */
class SendEmailUI : BaseUI() {
    var fileFlag = false
    var adapter: GridImageAdapter? = null
    var maxSelectNum = -1
    var selectList: List<LocalMedia> = ArrayList()//已选择照片
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ui_send_email)
        initAll()
    }

    override fun initData() {
        myTopBar.showLeftBtn("")
        myTopBar.showRightTextBtn("发送")
        myTopBar.left_btn.setOnClickListener {
            cancleShow()
        }

        sendUser.text = MyApp.mailInfo.userName
        emial_shou.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Select)
        val ms = LinearLayoutManager(this)
        ms.setOrientation(LinearLayoutManager.HORIZONTAL)// 设置 recyclerview 布局方式为横向布局
        //设置item间距，30dp
        recycler.addItemDecoration(SpaceItemDecoration(30));
        recycler.setLayoutManager(ms)
        adapter = GridImageAdapter(this@SendEmailUI)
        adapter?.setList(selectList)
        adapter?.setSelectMax(maxSelectNum)
        recycler.setAdapter(adapter)
    }

    override fun initEven() {
        myTopBar.right_action.setOnClickListener {
            var toAdd = collect_ren.text.toString()
            var subject = send_title.text.toString()
            var content = send_contect.text.toString()
            if (TextUtils.isEmpty(toAdd)) {
                collect_ren.error = "收件人不能为空"
                collect_ren?.requestFocus()
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(subject)) {
                send_title.error = "主题不能为空"
                send_title?.requestFocus()
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(content)) {
                send_contect.error = "邮件内容不能为空"
                send_contect?.requestFocus()
                return@setOnClickListener
            }
            var fileList: ArrayList<File> = ArrayList()
            if (null != selectList && selectList.size > 0) {
                for (item in selectList) {
                    fileList.add(File(item.path))
                }
            }
            if (fileList.size > 0) {//发送带附件的邮件
                MailTools.send(fileList, toAdd, subject, content);
            } else {//发送不带附件的邮件
                MailTools.send(toAdd, subject, content);
            }
            finish()
        }
//        get_email.setOnClickListener {
//            doAsync {
//                MailTools.getEmail()
//            }
//        }
        file_btn_layout.setOnClickListener {
            if (fileFlag) {
                fileFlag = false
                file_btn_layout.setBackgroundResource(R.drawable.btn_border_1)
                file_show_layout.visibility = View.GONE
                file_btn_img.setBackgroundResource(R.drawable.file_icon)
            } else {
                fileFlag = true
                file_show_layout.visibility = View.VISIBLE
                file_btn_layout.setBackgroundResource(R.drawable.btn_border_2)
                file_btn_img.setBackgroundResource(R.drawable.file_icon_touming)
            }
        }
        emial_shou.setTokenListener(object : TokenCompleteTextView.TokenListener<Person> {
            override fun onTokenAdded(token: Person) {
                updateTokenConfirmation()
            }

            override fun onTokenRemoved(token: Person) {
                updateTokenConfirmation()
            }

            override fun onDuplicateRemoved(token: Person) {

            }
        })
        img_file_btn.setOnClickListener { PictureSelector.chosseAll(this@SendEmailUI, maxSelectNum, selectList) }
    }


    private fun updateTokenConfirmation() {
        val sb = StringBuilder("Current tokens:\n")
        LogTeamUtils.e(emial_shou.getObjects().toString())
        for (token in emial_shou.getObjects()) {
            sb.append(token.toString())
            sb.append("\n")
        }
        LogTeamUtils.e(emial_shou.textForAccessibility)
        LogTeamUtils.e(sb)
    }

    fun cancleShow() {
        val myDialog = Email_Cancle_Dialog(mContext)
        myDialog.save_email.setOnClickListener {
            finish()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                PictureConfig.CHOOSE_REQUEST -> {
                    // 图片选择结果回调
                    selectList = PictureSelector.obtainMultipleResult(data)
                    // 例如 LocalMedia 里面返回三种path
                    // 1.media.getPath(); 为原图path
                    // 2.media.getCutPath();为裁剪后path，需判断media.isCut();是否为true
                    // 3.media.getCompressPath();为压缩后path，需判断media.isCompressed();是否为true
                    // 如果裁剪并压缩了，已取压缩路径为准，因为是先裁剪后压缩的
                    for (media in selectList) {
                        LogTeamUtils.e("图片-----》" + media.path)
                    }
                    adapter?.setList(selectList)
                    adapter?.notifyDataSetChanged()
                }
            }
        }
    }

    override fun onBackPressed() {
        cancleShow()
    }

}
