package com.team.cores.basecommon.recyclerview.base;

/**
 * Created by Administrator on 2017/6/21 0021.
 * Description:  多布局支持接口
 */
public interface MultiTypeSupport<T> {
    // 根据当前位置或者条目数据返回布局id
    public int getLayoutId(T item, int position);
}