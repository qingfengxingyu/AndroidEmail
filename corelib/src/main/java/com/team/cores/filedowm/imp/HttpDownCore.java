package com.team.cores.filedowm.imp;

import com.team.cores.filedowm.cores.FileTask;
import com.team.cores.filedowm.cores.ShopFileTask;

import okhttp3.OkHttpClient;

public interface HttpDownCore {

    /***
     *
     * @param httpClient
     * @param mTask
     */
    public void httpDown(OkHttpClient httpClient, ShopFileTask mTask);

    /***
     *
     * @param httpClient
     * @param mTask
     */
    public void httpDown(OkHttpClient httpClient, FileTask mTask);

    /***
     *
     * @param mTask
     */
    public void httpDown(ShopFileTask mTask);

    /***
     *
     * @param downFlag true 下载 false 不下载
     */
    public void setDownFlag(boolean downFlag);
}
