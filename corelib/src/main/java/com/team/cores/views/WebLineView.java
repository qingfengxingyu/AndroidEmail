package com.team.cores.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.team.cores.R;


/**
 * Description:Webview进度条
 * Data: 2017/8/8 0008 下午 1:47
 *
 * @author: xmf
 */
@SuppressLint("Recycle")
public class WebLineView extends LinearLayout {

    @SuppressWarnings("unused")
    private Context context;
    /**
     * 中间控件的高度
     */
    @SuppressWarnings("unused")
    private float inHeghtS;
    private TextView one_text, two_text;

    public WebLineView(Context context, AttributeSet attr) {
        super(context, attr);
        initView(context, attr);
    }

    @SuppressLint("NewApi")
    public WebLineView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attr) {
        this.context = context;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.webline_layout, null);
        this.addView(view, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        one_text = (TextView) findViewById(R.id.one_text);
        two_text = (TextView) findViewById(R.id.two_text);
        TypedArray mTypedArray = context.obtainStyledAttributes(attr,
                R.styleable.LevelView);
        int inHeghtS = mTypedArray.getDimensionPixelOffset(
                R.styleable.LevelView_inheght, 3);
        one_text.setHeight(inHeghtS);
        two_text.setHeight(inHeghtS);
    }

    /***
     * 设置比例
     *
     * @param currentExp 当前进度
     * @param needExp    总进度
     */
    public void setLevels(int currentExp, int needExp) {
        LayoutParams lp = new LayoutParams(0,
                LayoutParams.WRAP_CONTENT);
        lp.weight = currentExp;
        one_text.setLayoutParams(lp);
        LayoutParams lp2 = new LayoutParams(0,
                LayoutParams.WRAP_CONTENT);
        lp2.weight = needExp - currentExp;
        two_text.setLayoutParams(lp2);
    }
}