package com.team.cores.utils;

import android.content.Context;

import com.team.cores.utils.tools.LogTeamUtils;


/**
 * author:xmf
 * date:2018/10/11 0011
 * description:初始化所有需要初始化的工具类集合
 */
public class InitAllTool {

    public static void initAll(Context mContext) {
        if (mContext == null) return;
        FileSaveTool.getInstance().initContext(mContext);//初始化文件
        LogTeamUtils.init(mContext, true, false, 'v', "mip_xmf");//初始化Log日志
    }

}
