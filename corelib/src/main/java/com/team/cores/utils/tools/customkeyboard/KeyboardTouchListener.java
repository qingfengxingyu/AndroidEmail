package com.team.cores.utils.tools.customkeyboard;

import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

public class KeyboardTouchListener implements View.OnTouchListener {
    private KeyboardPopup keyboardPopup;
    private int keyboardType = 1;
    private int scrollTo = -1;

    public KeyboardTouchListener(KeyboardPopup util, int keyboardType, int scrollTo){
        this.keyboardPopup = util;
        this.keyboardType = keyboardType;
        this.scrollTo = scrollTo;
    }
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (keyboardPopup != null && keyboardPopup.getEd() !=null &&v.getId() != keyboardPopup.getEd().getId())
                keyboardPopup.showKeyBoardLayout((EditText) v,keyboardType,scrollTo);
            else if(keyboardPopup != null && keyboardPopup.getEd() ==null){
                keyboardPopup.showKeyBoardLayout((EditText) v,keyboardType,scrollTo);
            }else{
//                Log.d("KeyboardTouchListener", "v.getId():" + v.getId());
//                Log.d("KeyboardTouchListener", "keyboardPopup.getEd().getId():" + keyboardPopup.getEd().getId());
                    if (keyboardPopup != null) {
                        keyboardPopup.setKeyBoardCursorNew((EditText) v);
                }
            }
        }
        return false;
    }
}
