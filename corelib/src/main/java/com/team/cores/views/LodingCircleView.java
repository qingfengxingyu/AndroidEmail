package com.team.cores.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import com.team.cores.R;

/**
 * Created by xmf on 2017/06/19.
 * 类似微信视频加载进度条
 */
public class LodingCircleView extends View {


    private Paint paintBgCircle;


    private Paint paintCircle;

    private Paint paintProgressCircle;


    private float startAngle = -90f;//开始角度

    private float sweepAngle = 0;//结束

    private int progressCirclePadding = 0;//进度圆与背景圆的间距


    private boolean fillIn = false;//进度圆是否填充

    private int animDuration = 6000;


    private LodingCircleViewAnim mLodingCircleViewAnim;//动画效果

    private TypedArray typedArray;

    public LodingCircleView(Context context) {
        super(context);
        init(context);
    }

    public LodingCircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        typedArray = context.obtainStyledAttributes(attrs,
                R.styleable.CricleViewStyle);
        init(context);
    }

    public LodingCircleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        typedArray = context.obtainStyledAttributes(attrs,
                R.styleable.CricleViewStyle);
        init(context);
    }

    public int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }


    private void init(Context context) {
        mLodingCircleViewAnim = new LodingCircleViewAnim();
        mLodingCircleViewAnim.setDuration(animDuration);
        progressCirclePadding = dip2px(getContext(), 3);

        paintBgCircle = new Paint();//边框色
        paintBgCircle.setAntiAlias(true);
        paintBgCircle.setStyle(Paint.Style.FILL);


        paintCircle = new Paint();//背景色
        paintCircle.setAntiAlias(true);
        paintCircle.setStyle(Paint.Style.FILL);


        paintProgressCircle = new Paint();//动画色
        paintProgressCircle.setAntiAlias(true);
        paintProgressCircle.setStyle(Paint.Style.FILL);


        if (null == typedArray) {
            paintBgCircle.setColor(Color.WHITE);
            paintCircle.setColor(context.getResources().getColor(R.color.color_loding_circle));
            paintProgressCircle.setColor(Color.WHITE);
        } else {
            paintBgCircle.setColor(typedArray.getColor(R.styleable.CricleViewStyle_biankuang, Color.WHITE));//边框
            paintCircle.setColor(typedArray.getColor(R.styleable.CricleViewStyle_beijing, context.getResources().getColor(R.color.color_loding_circle)));//背景
            paintProgressCircle.setColor(typedArray.getColor(R.styleable.CricleViewStyle_donghua, Color.WHITE));//动画色
        }

    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle(getMeasuredWidth() / 2, getMeasuredWidth() / 2, getMeasuredWidth() / 2, paintBgCircle);
        canvas.drawCircle(getMeasuredWidth() / 2, getMeasuredWidth() / 2, getMeasuredWidth() / 2 - progressCirclePadding / 2, paintCircle);
        RectF f = new RectF(progressCirclePadding, progressCirclePadding, getMeasuredWidth() - progressCirclePadding, getMeasuredWidth() - progressCirclePadding);
        canvas.drawArc(f, startAngle, sweepAngle, true, paintProgressCircle);
        if (!fillIn)
            canvas.drawCircle(getMeasuredWidth() / 2, getMeasuredWidth() / 2, getMeasuredWidth() / 2 - progressCirclePadding * 2, paintCircle);


    }


    public void startAnimAutomatic(boolean fillIn) {
        this.fillIn = fillIn;
        if (mLodingCircleViewAnim != null)
            clearAnimation();
        startAnimation(mLodingCircleViewAnim);
    }

    public void stopAnimAutomatic() {
        if (mLodingCircleViewAnim != null)
            clearAnimation();
    }


    public void setProgerss(int progerss, boolean fillIn) {
        this.fillIn = fillIn;
        sweepAngle = (float) (360 / 100.0 * progerss);
        invalidate();
    }


    private class LodingCircleViewAnim extends Animation {
        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);
            if (interpolatedTime < 1.0f) {
                sweepAngle = 360 * interpolatedTime;
                invalidate();
            } else {
                startAnimAutomatic(fillIn);
            }

        }
    }
}