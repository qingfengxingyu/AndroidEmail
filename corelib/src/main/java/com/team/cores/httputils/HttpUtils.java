package com.team.cores.httputils;

import com.team.cores.utils.tools.LogTeamUtils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Created by xmf on 2017/6/13 0013.
 */
public class HttpUtils {
    /***
     * 令牌最新统一名称
     */
    public static final String SESSIONID = "SessionId";
    /***
     * 令牌过期错误码---511
     */
    public static final int Token_GuoQi = 511;

    /***
     * 转换请求参数
     *
     * @param jsonObject
     * @return
     */
    public static List<NameValuePair> changeParams(JSONObject jsonObject) {
        LogTeamUtils.e("jsonObject is:" + jsonObject);
        List<NameValuePair> p_params = new ArrayList<NameValuePair>();
        if (null == jsonObject || "{}".equals(jsonObject.toString())) {
            return p_params;
        }
        addCommonRequestData(jsonObject);
        NameValuePair nameValuePair = null;
        String key = "";
        Iterator<String> sIterator = jsonObject.keys();
        while (sIterator.hasNext()) {
            try {
                key = sIterator.next();
                nameValuePair = new BasicNameValuePair(key, jsonObject.getString(key));
                p_params.add(nameValuePair);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        return p_params;
    }

//    /***
//     * Json转换成支付宝的FastJson请求参数
//     *
//     * @param jsonObject
//     * @return
//     */
//    public static com.alibaba.fastjson.JSONObject changeFastJsonParams(JSONObject jsonObject) {
//        com.alibaba.fastjson.JSONObject fastJson = new com.alibaba.fastjson.JSONObject();
//        if (null == jsonObject) {
//            return fastJson;
//        }
//        addCommonRequestData(jsonObject);
//        String key = "";
//        Iterator<String> sIterator = jsonObject.keys();
//        while (sIterator.hasNext()) {
//            try {
//                key = sIterator.next();
//                fastJson.put(key, jsonObject.getString(key));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        LogTeamUtils.e("fastJson is:" + fastJson);
//        return fastJson;
//    }

//    public static void addAllHeaders(IRestClient restClient, JSONObject headerJson) {
//        try {
//            if (null != headerJson) {
//                String key = "";
//                Iterator<String> sIterator = headerJson.keys();
//                while (sIterator.hasNext()) {
//                    try {
//                        key = sIterator.next();
//                        restClient.addHeader(key, headerJson.getString(key));
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void commonHeader(IRestClient restClient, JSONObject headerJson) {
//        try {
//            if (null != restClient) {
//                restClient.addHeader("X-Cache", 10 * 1000 + "");
//                addAllHeaders(restClient, headerJson);
//                if (null != Platform.getCurrent()) {
//                    if (null != Platform.getCurrent().getMembership() && null != Platform.getCurrent().getMembership().getToken()) {
//                        restClient.addHeader(SESSIONID, Platform.getCurrent().getMembership().getToken());
//                        restClient.addHeader("sessionId", Platform.getCurrent().getMembership().getToken());
//                        restClient.addHeader("Token", Platform.getCurrent().getMembership().getToken());
//                    }
//                    if (null != Platform.getCurrent().getEnvironment() && null != Platform.getCurrent().getEnvironment()
//                            .getPlatformVersionName()) {
//                        restClient.addHeader("versionname", Platform.getCurrent().getEnvironment().getPlatformVersionName());
//                    }
//                    if (null != Platform.getCurrent().getMembership() && null != Platform.getCurrent().getMembership().getCurrentUser() && null != Platform.getCurrent().getMembership().getCurrentUser().getUserName()) {
//                        restClient.addHeader("mipuser", Platform.getCurrent().getMembership().getCurrentUser().getUserName());
//                    }
//                    if (null != Platform.getCurrent().getDevice()) {
//                        String esn = Platform.getCurrent().getDevice().getESN();
//                        if (StringUtils.isEmpty(esn)) {
//                            esn = "";
//                        }
//                        String imei = Platform.getCurrent().getDevice().getESN();
//                        if (StringUtils.isEmpty(imei)) {
//                            imei = "";
//                        }
//                        String esnimei = esn + imei;
//                        restClient.addHeader("esn", esn);
//                        restClient.addHeader("imei", imei);
//                        restClient.addHeader("esnimei", esnimei);
//                    }
//
//                }
//
//
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            LogTeamUtils.e("commonHeader is:" + e.toString());
//        }
//    }

    /***
     * 请求的公共参数
     *
     * @param params
     * @return
     */
    public static JSONObject addCommonRequestData(JSONObject params) {
//        try {
//            if (null != Platform.getCurrent() && null != Platform.getCurrent().getMembership() && null != Platform.getCurrent().getMembership().getToken()) {
//                params.put("Token", Platform.getCurrent().getMembership().getToken());
//                params.put(SESSIONID, Platform.getCurrent().getMembership().getToken());
//                params.put("sessionId", Platform.getCurrent().getMembership().getToken());
//            }
//            if (null != Platform.getCurrent() && null != Platform.getCurrent().getMembership() && null != Platform.getCurrent().getMembership().getCurrentUser() && null != Platform.getCurrent().getMembership().getCurrentUser().getUserName()) {
//                params.put("mipuser", Platform.getCurrent().getMembership().getCurrentUser().getUserName());
//            }
//            if (null != Platform.getCurrent() && null != Platform.getCurrent().getEnvironment() && null != Platform.getCurrent().getEnvironment()
//                    .getPlatformVersionName()) {
//                params.put("versionname", Platform.getCurrent().getEnvironment().getPlatformVersionName());
//            }
//            if (null != PlatformConfig.getSingleton().getValue("netflag")) {
//                params.put("netflag", PlatformConfig.getSingleton().getValue("netflag"));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            LogTeamUtils.e("addCommonRequestData is:" + e.toString());
//        }
            return params;
        }

//        /***
//         * 获得服务器设定的访问地址
//         *
//         * @param values
//         * @return
//         */
//        public static String getSelfBaseUrl (String values){
//            String baseUrl = "";
//            try {
//                JSONObject job = new JSONObject(Platform.getCurrent().getSSOService().getSSOInfo().getCustomParams());
//                LogTeamUtils.e("custom is:" + job.toString());
//                baseUrl = job.optString(values);
//            } catch (Exception e) {
//                LogTeamUtils.e(e.toString());
//            }
//            return baseUrl;
//        }
    }
