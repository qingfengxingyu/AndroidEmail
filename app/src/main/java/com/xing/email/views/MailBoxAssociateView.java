package com.xing.email.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;

/**
 * author:xmf
 * date:2018/11/13 0013
 * description:邮箱的自动补全功能
 */
public class MailBoxAssociateView extends android.support.v7.widget.AppCompatMultiAutoCompleteTextView {
    private String[] email_sufixs = new String[]{"@qq.com", "@163.com", "@126.com", "@gmail.com", "@sina.com", "@hotmail.com",
            "@yahoo.cn", "@sohu.com", "@foxmail.com", "@139.com", "@yeah.net", "@vip.qq.com", "@vip.sina.com"};

    public MailBoxAssociateView(Context context) {
        super(context);
        initData(context);
    }

    public MailBoxAssociateView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initData(context);
    }

    public MailBoxAssociateView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initData(context);
    }

    private void initData(Context context) {
        MailBoxAssociateTokenizer mailBoxAssociateTokenizer = new MailBoxAssociateTokenizer();
        ArrayAdapter<String> adapter = new ArrayAdapter(context,
                android.R.layout.simple_dropdown_item_1line, email_sufixs);
        setAdapter(adapter);
        setTokenizer(mailBoxAssociateTokenizer);

    }

    @Override
    public boolean enoughToFilter() {
        // 如果字符中包含'@'并且不在第一位，则满足条件
        return getText().toString().contains("@") && getText().toString().indexOf("@") > 0;
    }
}


