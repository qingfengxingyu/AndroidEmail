package com.xing.email.ui

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import com.xing.email.R
import com.xing.email.app.BaseUI
import com.xing.email.app.MyApp
import com.xing.email.utils.emailtool.MailTools
import kotlinx.android.synthetic.main.ui_new_build_account.*
import org.jetbrains.anko.activityUiThread
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast

/**
 *author:xmf
 *date:2018/11/5 0005
 *description:新建账户页面
 */
class LoginUI : BaseUI() {
    var passwordFlag = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ui_login)
        initAll()
        if(null!= MyApp.session){
            startActivity(Intent(mContext, AccountIndexUI::class.java))
            finish()
        }
    }

    override fun initData() {
        if (TextUtils.isEmpty(email_sp)) {
            email_sp = "1282061691@qq.com"
        }
        if (TextUtils.isEmpty(pwd_sp)) {
            pwd_sp = "retwitgvfcalfffc"
        }
        email.setText(email_sp)
        email.setSelection(email_sp.length)
        password.setText(pwd_sp)
    }

    override fun initEven() {
        del_email.setOnClickListener {
            email.setText("")
        }
        show_mima.setOnClickListener {
            passwordFlag = !passwordFlag
            password.setTransformationMethod(if (passwordFlag) HideReturnsTransformationMethod.getInstance() else PasswordTransformationMethod.getInstance())
            yan_icon.setBackgroundResource(if (passwordFlag) R.drawable.eyes_opend else R.drawable.eyes_closed)
            password.setSelection(password.text.length)
        }
        next_btn.setOnClickListener {
            attemptNext()
        }
    }

    fun attemptNext() {//下一步
        var cancel = false
        var focusView: View? = null
        email.error = null
        password.error = null
        val emailStr = email.text.toString()
        val passwordStr = password.text.toString()
        if (TextUtils.isEmpty(emailStr)) {
            email.error = getString(R.string.error_field_required)
            focusView = email
            cancel = true
        } else if (!isEmailValid(emailStr)) {
            email.error = getString(R.string.error_invalid_email)
            focusView = email
            cancel = true
        } else if (TextUtils.isEmpty(passwordStr)) {
            password.error = getString(R.string.error_invalid_password)
            focusView = password
            cancel = true
        }
        if (cancel) {
            focusView?.requestFocus()
        } else {
            email_sp = emailStr
            pwd_sp = passwordStr
            MyApp.mailInfo.mailServerHost = "smtp.qq.com"
            MyApp.mailInfo.mailServerPort = "587"
            MyApp.mailInfo.getServerHost = "pop.qq.com"
            MyApp.mailInfo.getServerPort = "995"
            MyApp.mailInfo.fromAddress = emailStr
            MyApp.mailInfo.userName = emailStr
            MyApp.mailInfo.password = passwordStr
            showProgressDialog("登录提示", "正在登入，请稍后... ...")
            doAsync {
                MailTools.login()
                activityUiThread {
                    hideProgressDialog()
                    if (MyApp.session == null) {
                        toast("账号或密码错误")
                    } else {
                        startActivity(Intent(mContext, AccountIndexUI::class.java))
                        finish()
                    }
                }
            }

        }
    }

    private fun isEmailValid(email: String): Boolean {//邮箱验证
        //TODO: Replace this with your own logic
        return email.contains("@")
    }
}
