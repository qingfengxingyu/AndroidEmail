package com.team.cores.filedowm.utils;

/**
 * Html类型的配置实体类
 */
public class ConfigBean {

    private String widgetId;
    private String pid;
    private String appId;
    private String channelCode;
    private String version;
    private String viewmode;
    private String width;
    private String height;
    private String sreensize;

    public String getWidgetId() {
        return widgetId;
    }

    public void setWidgetId(String widgetId) {
        this.widgetId = widgetId;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getViewmode() {
        return viewmode;
    }

    public void setViewmode(String viewmode) {
        this.viewmode = viewmode;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getSreensize() {
        return sreensize;
    }

    public void setSreensize(String sreensize) {
        this.sreensize = sreensize;
    }

    @Override
    public String toString() {
        return "ConfigBean{" +
                "widgetId='" + widgetId + '\'' +
                ", pid='" + pid + '\'' +
                ", appId='" + appId + '\'' +
                ", channelCode='" + channelCode + '\'' +
                ", version='" + version + '\'' +
                ", viewmode='" + viewmode + '\'' +
                ", width='" + width + '\'' +
                ", height='" + height + '\'' +
                ", sreensize='" + sreensize + '\'' +
                '}';
    }
}
