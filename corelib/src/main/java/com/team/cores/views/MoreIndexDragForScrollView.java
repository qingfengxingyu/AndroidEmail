package com.team.cores.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

/**
 *
 * Created by wfx on 2017/6/20.
 */

public class MoreIndexDragForScrollView extends ScrollView {
    private boolean isDrag;
	public MoreIndexDragForScrollView(Context context) {
        super(context);
    }
    public MoreIndexDragForScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public MoreIndexDragForScrollView(Context context, AttributeSet attrs,
                                      int defStyle) {
        super(context, attrs, defStyle);
    }
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN||ev.getAction() ==MotionEvent.ACTION_UP||ev.getAction() ==MotionEvent.ACTION_MOVE) {
            if (isDrag) {
                return false;
            } else {
            }
        }
        return super.onInterceptTouchEvent(ev);
    }
    public void startDrag(int position) {
		isDrag = true;
	}
    public void endDrag(int position) {
		isDrag = false;
	}
}

