package com.team.cores.safetykeyboard;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class ABEditText extends AppCompatEditText {

    private Context context;

    private ABKeyBoard mPopupWindow;

    public ABEditText(Context context) {
        super(context);
        init(context);
    }

    public ABEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public ABEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        this.context = context;
        setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!view.isFocused() && mPopupWindow != null) {
                    mPopupWindow.dismiss();
                    mPopupWindow = null;
                }
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            boolean isOpen = imm.isActive();//isOpen若返回true，则表示输入法打开
            if (isOpen) {
                imm.hideSoftInputFromWindow(getWindowToken(), 0);
            }
        }
        if (mPopupWindow != null && mPopupWindow.isShowing()) {
            return false;
        } else {
            requestFocus();
            setSelection(getText().length());
            mPopupWindow = new ABKeyBoard(context, this);
            mPopupWindow.showAtLocation(this, Gravity.BOTTOM, 0, 0);
        }
        return false;
    }

    public void isCloseRandomKey(Boolean isRandom) {
        mPopupWindow.setCloseRandomKey(isRandom);
    }
}
