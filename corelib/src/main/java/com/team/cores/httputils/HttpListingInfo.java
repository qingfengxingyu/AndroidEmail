package com.team.cores.httputils;

/**
 * Created by xmf on 2017/6/15 0015.
 * RX请求回调
 */
public interface HttpListingInfo {
    void onCompleted();

    void onError(Throwable e);

    void onNext(Object object);
}
