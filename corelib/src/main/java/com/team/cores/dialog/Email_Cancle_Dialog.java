package com.team.cores.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.TextView;

import com.team.cores.R;

/**
 * author:xmf
 * date:2018/11/13 0013
 * description:发送邮件取消弹出框
 */
public class Email_Cancle_Dialog extends Dialog {
    public TextView save_email;
    private TextView cancle_btn, go_btn;
    private TextView msg_neirong;
    private TextView msg_title;
    private Context context;
    private View line1;

    public Email_Cancle_Dialog(Context context) {
        super(context, R.style.teams_dialog);
        this.context = context;
        show();
    }

    public Email_Cancle_Dialog(Context context, int theme) {
        super(context, theme);
        this.context = context;
        show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View v = getLayoutInflater().inflate(R.layout.emial_send_dialog, null);
        LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT);
        setContentView(v, lp);
        this.getWindow().setBackgroundDrawable(new ColorDrawable());//解决5.0dialog背景黑框问题
        cancle_btn = (TextView) v.findViewById(R.id.cancle_btn);
        save_email = (TextView) v.findViewById(R.id.save_email);
        go_btn = (TextView) v.findViewById(R.id.go_btn);
        msg_title = (TextView) v.findViewById(R.id.msg_title);
        line1 = (View) v.findViewById(R.id.line1);
        msg_neirong = (TextView) v.findViewById(R.id.msg_neirong);
        msg_neirong.setMovementMethod(ScrollingMovementMethod.getInstance());
        cancle_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dismiss();
            }
        });
        go_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != context) {
                    ((Activity) context).finish();
                }
            }
        });

    }

    @Override
    public void show() {
        super.show();
        /**
         * 设置宽度全屏，要设置在show的后面
         */
        WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
        layoutParams.gravity = Gravity.CENTER;
        layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
        layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, context.getResources().getDisplayMetrics());
        getWindow().getDecorView().setPadding(pageMargin, 0, pageMargin, 0);
        getWindow().setAttributes(layoutParams);
    }
}
