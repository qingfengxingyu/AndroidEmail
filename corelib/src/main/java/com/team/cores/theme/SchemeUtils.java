package com.team.cores.theme;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;

import com.team.cores.utils.LogTools;
import com.team.cores.utils.tools.LogTeamUtils;
import com.team.cores.utils.tools.StringUtils;

import org.json.JSONObject;

import java.util.Iterator;
import java.util.List;

/**
 * Description:Activity通过URL跳转工具类
 * Data: 2017/6/29 0029 下午 3:39
 *
 * @author: xmf
 */
public class SchemeUtils {
    public Context mContext;
    private static volatile SchemeUtils instance;//推荐使用volatile关键字，将对像直接写入主内存，会影响性能，但是会保证程序的准确性。

    public static SchemeUtils getInstance() {
        return SchemeUtilsHolder.instance;
    }

    //静态内部类
    private static class SchemeUtilsHolder {
        private static final SchemeUtils instance = new SchemeUtils();
    }

    /***
     * 判断Scheme是否有效 true 有效 false 无效
     *
     * @param mUrl
     */
    public boolean checkScheme(String mUrl) {
        if (StringUtils.isEmpty(mUrl)) return false;
        PackageManager packageManager = mContext.getPackageManager();
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mUrl));
        List<ResolveInfo> activities = packageManager.queryIntentActivities(intent, 0);
        return !activities.isEmpty();
    }

    /***
     * 跳转Activity
     *
     * @param mUrl
     */
    public void jumpToActivity(String mUrl) {
        if (checkScheme(mUrl)) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mUrl));
            mContext.startActivity(intent);
        }
    }

    /***
     * 跳转Activity  scheme://host:port/path?goodsId=10011002
     *
     * @param mUrl
     * @param params 参数
     */
    public void jumpToActivity(String mUrl, String params) {
        if (checkScheme(mUrl)) {
            String msg = changParams(params);
            if (!StringUtils.isEmpty(msg)) {
                if (mUrl.indexOf("?") > 0) {
                    if (mUrl.indexOf("&") > 0) {
                        mUrl = mUrl + "&" + params;
                    } else {
                        mUrl = mUrl + params;
                    }
                } else {
                    mUrl = mUrl + "?" + params;
                }
            }
            LogTeamUtils.e("mUrl is:" + mUrl);
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mUrl));
            mContext.startActivity(intent);
        }
    }

    /***
     * 转换 请求参数
     *
     * @param params
     * @return
     */
    private String changParams(String params) {
        String result = "";
        if (StringUtils.isEmpty(params)) {
            return result;
        }
        try {
            JSONObject jsonObject = new JSONObject(params);
            Iterator<String> sIterator = jsonObject.keys();
            String key = "";
            String values = "";
            while (sIterator.hasNext()) {
                try {
                    key = sIterator.next();
                    values = jsonObject.getString(key);
                    if (StringUtils.isEmpty(result)) {
                        result = key + "=" + values;
                    } else {
                        result = result + "&" + key + "=" + values;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
//    获取Scheme跳转的参数---xl://goods:8888/goodsDetail?goodsId=10011002
//    //获取指定参数值
//    String goodsId = uri.getQueryParameter("goodsId");
}
