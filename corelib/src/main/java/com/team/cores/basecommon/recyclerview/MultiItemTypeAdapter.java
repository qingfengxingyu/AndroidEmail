package com.team.cores.basecommon.recyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.team.cores.R;
import com.team.cores.basecommon.recyclerview.base.MultiTypeSupport;
import com.team.cores.basecommon.recyclerview.base.OnItemClickListener;
import com.team.cores.basecommon.recyclerview.base.ViewHolder;
import com.team.cores.dialog.Error_Dialog;
import com.team.cores.utils.tools.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by xmf on 2017/6/21
 * RecyclerView 基类封装
 */
public abstract class MultiItemTypeAdapter<T> extends RecyclerView.Adapter<ViewHolder> {

    protected Context mContext;
    protected LayoutInflater mInflater;
    //数据怎么办？
    public List<T> mDatas = new ArrayList<>();
    // 布局怎么办？
    private int mLayoutId;
    private int ITEM_TYPE_EMPTY = 123456;//报错显示或空数据布局
    // 多布局支持
    private MultiTypeSupport<T> mMultiTypeSupport;
    private int hh;
    private int typeNum;
    private String erro_msg = "";
    private boolean errorFlag;
    public final int TYPE_ERROR = 1;
    public final int TYPE_NO_NET = 2;
    public final int TYPE_NO_DATA = 3;

    public MultiItemTypeAdapter(Context context, Collection<T> data, int layoutId) {
        this.mDatas.addAll(data);
        this.mLayoutId = layoutId;
        initParam(context);
    }


    /**
     * 多布局支持
     */
    public MultiItemTypeAdapter(Context context, Collection<T> data, MultiTypeSupport<T> multiTypeSupport) {
        this(context, data, -1);
        this.mMultiTypeSupport = multiTypeSupport;
        initParam(context);
    }

    private void initParam(Context context) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(mContext);
        int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, context.getResources().getDisplayMetrics());
        try {
            DisplayMetrics dm = context.getResources().getDisplayMetrics();
            hh = dm.heightPixels - pageMargin - context.getResources().getDimensionPixelSize(context.getResources().getIdentifier("status_bar_height", "dimen", "android"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据当前位置获取不同的viewType
     */
    @Override
    public int getItemViewType(int position) {
        if (null == mDatas || mDatas.size() <= 0) {
            return ITEM_TYPE_EMPTY;
        }
        errorFlag = false;
        // 多布局支持
        if (mMultiTypeSupport != null) {
            return mMultiTypeSupport.getLayoutId(mDatas.get(position), position);
        }
        return super.getItemViewType(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder = null;
        if (ITEM_TYPE_EMPTY == viewType) {
            // 先inflate数据
            View itemView = mInflater.inflate(R.layout.errorview_layout, parent, false);
            // 返回ViewHolder
            holder = new ViewHolder(itemView);
        } else {
            // 多布局支持
            if (mMultiTypeSupport != null) {
                mLayoutId = viewType;
            }
            // 先inflate数据
            View itemView = mInflater.inflate(mLayoutId, parent, false);
            // 返回ViewHolder
            holder = new ViewHolder(itemView);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if (ITEM_TYPE_EMPTY != getItemViewType(position)) {
            // 设置点击和长按事件
            if (mItemClickListener != null) {
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.onItemClick(v, holder, position);
                    }
                });
                holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return mItemClickListener.onItemLongClick(v, holder, position);
                    }
                });
            }
            // 绑定回传
            convert(holder, mDatas.get(position));
            convert(holder, mDatas.get(position), position);
        } else {
            showErrorUI(holder);
        }
    }

    /***
     * 默认加载失败
     *
     * @param erro_msg 错误信息
     */
    public void showErrorUI(String erro_msg) {
        errorFlag = true;
        this.typeNum = 1;
        this.erro_msg = erro_msg;
    }

    /***
     * @param typeNum  1加载失败 2 没有网络 4无数据
     * @param erro_msg 错误信息
     */
    public void showErrorUI(int typeNum, String erro_msg) {
        mDatas.clear();
        notifyDataSetChanged();
        errorFlag = true;
        this.typeNum = typeNum;
        this.erro_msg = erro_msg;
    }

    /***
     * @param typeNum  1加载失败 2 没有网络 4无数据
     * @param erro_msg 错误信息
     * @param hh       报错页面的高度
     */
    public void showErrorUI(int typeNum, String erro_msg, int hh) {
        mDatas.clear();
        notifyDataSetChanged();
        errorFlag = true;
        this.typeNum = typeNum;
        this.erro_msg = erro_msg;
        this.hh = hh;
    }

    /***
     * 设置报错信息显示
     */
    private void showErrorUI(ViewHolder holder) {
        if (!errorFlag) {
            holder.getView(R.id.erro_layout).setVisibility(View.GONE);
            return;
        }
        String msg = "";
        holder.getView(R.id.erro_layout).setVisibility(View.VISIBLE);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, hh);
        holder.getView(R.id.erro_layout).setLayoutParams(lp);
        switch (typeNum) {
            case TYPE_NO_NET:
                ((ImageView) holder.getView(R.id.imageView)).setImageResource(R.drawable.no_net1);
                if (StringUtils.isEmpty(erro_msg)) {
                    msg = "没有网络,请检查您的当前网络设置!";
                } else {
                    msg = erro_msg;
                }
                break;
            case TYPE_NO_DATA:
                ((ImageView) holder.getView(R.id.imageView)).setImageResource(R.drawable.no_data1);
                if (StringUtils.isEmpty(erro_msg)) {
                    msg = "暂无数据,请下拉刷新!";
                } else {
                    msg = erro_msg;
                }

                break;
            case TYPE_ERROR:
            default:
                ((ImageView) holder.getView(R.id.imageView)).setImageResource(R.drawable.erro1);
                msg = "加载失败,请下拉重新加载!";
                holder.getView(R.id.imageView).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cilick3();

                    }
                });
                break;
        }
        ((TextView) holder.getView(R.id.erro_msg)).setText(msg);
    }


    /**
     * 利用抽象方法回传出去，每个不一样的Adapter去设置
     *
     * @param item 当前的数据
     */
    public abstract void convert(ViewHolder holder, T item);

    /**
     * 利用抽象方法回传出去，每个不一样的Adapter去设置
     *
     * @param item     当前的数据
     * @param position 当前的位置
     */
    public void convert(ViewHolder holder, T item, int position) {
    }

    @Override
    public int getItemCount() {
        int itemCount = 0;
        if (null != mDatas && mDatas.size() > 0) {
            itemCount = mDatas.size();
        } else if (itemCount == 0) itemCount++;
        return itemCount;
    }

    /***************
     * 设置条目点击和长按事件
     *********************/
    public OnItemClickListener mItemClickListener;

    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        this.mItemClickListener = itemClickListener;
    }

    Timer timer;
    TimerTask mTimerTask;
    int clickCount = 0;

    /***
     * 点击大于三次才能看到错误日志弹出
     */
    public void cilick3() {
        clickCount++;
        if (timer != null) {
            return;
        }
        timer = new Timer();
        mTimerTask = new TimerTask() {
            int count = 0;

            @Override
            public void run() {
                //500ms之后若点击次数发生改变则更新点击次数，否则显示点击次数，定时结束并一切清零
                if (count != clickCount) {
                    count = clickCount;
                    if (count >= 3) {
                        Observable.create(new Observable.OnSubscribe<String>() {
                            @Override
                            public void call(Subscriber<? super String> subscriber) {
                                subscriber.onNext("");
                                subscriber.onCompleted();
                            }
                        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Object>() {
                            @Override
                            public void onCompleted() {
                            }

                            @Override
                            public void onError(Throwable e) {
                            }

                            @Override
                            public void onNext(Object object) {
                                Error_Dialog myDialog = new Error_Dialog(mContext,
                                        R.style.teams_dialog);
                                myDialog.setErrorMsg(erro_msg);
                            }
                        });
                    }
                    clickCount = 0;
                    this.cancel();
                    timer.cancel();
                    mTimerTask = null;
                    timer = null;
                } else {
                    clickCount = 0;
                    this.cancel();
                    timer.cancel();
                    mTimerTask = null;
                    timer = null;
                }
            }
        };
        timer.schedule(mTimerTask, 500, 500);
    }
}
