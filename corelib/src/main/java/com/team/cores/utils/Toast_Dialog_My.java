package com.team.cores.utils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Gravity;
import android.widget.Toast;

import com.team.cores.utils.tools.StringUtils;

/***
 * 自己的Toast工具类和dialog类合集
 *
 * @author xmf
 */
public class Toast_Dialog_My {
    private Context context;

    public Toast_Dialog_My(Context context) {
        this.context = context;
    }

    /***
     * Toast
     *
     * @param tiShiMsg   提示信息
     * @param defaultMsg 默认提示信息
     */
    public void toshow(String tiShiMsg, String defaultMsg) {
        Toast.makeText(context,
                StringUtils.isEmpty(tiShiMsg) ? defaultMsg : tiShiMsg,
                Toast.LENGTH_SHORT).show();
    }

    /***
     * Toast
     *
     * @param defaultMsg 提示信息
     */
    public void toshow(String defaultMsg) {
        Toast.makeText(context, defaultMsg, Toast.LENGTH_SHORT).show();
    }

    /***
     * Toast 居中提示
     *
     * @param msg
     */
    public void toCentershow(String msg) {
        Toast toast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    /***
     * Toast顶部显示
     *
     * @param msg
     */
    public void toTopshow(String msg) {
        Toast toast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }

    /***
     * 只有一个按钮的自己定义的系统式弹出框
     *
     * @param msg 提示信息
     */
    public void toDialog(String msg) {
        new AlertDialog.Builder(context).setTitle("信息提示").setMessage(msg)
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    /***
     * 只有一个按钮的自己定义的系统式弹出框
     *
     * @param title 标题
     * @param msg   提示信息
     */
    public void MyDialogOneButton(String title, String msg) {
        new AlertDialog.Builder(context).setTitle(title).setMessage(msg)
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    /***
     * 只有一个按钮的自己定义的系统式弹出框
     *
     * @param title     标题
     * @param msg       提示信息
     * @param canceFlag 是否能Cancelable
     */
    public void MyDialogOneButton(String title, String msg, boolean canceFlag) {
        new AlertDialog.Builder(context).setCancelable(canceFlag)
                .setTitle(title).setMessage(msg)
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    /***
     * 转圈提示框
     *
     * @param title     标题
     * @param msg       提示信息
     * @param canceFlag 是否能Cancelable
     */
    public ProgressDialog myShowDialog2(String title, String msg,
                                        boolean canceFlag) {
        return ProgressDialog.show(context, "".equals(title) ? "数据加载中" : title,
                "".equals(msg) ? "请稍后..." : msg, true, canceFlag);
    }

    /***
     * 登陆超时提示框
     */
    public void checkDialog() {
        new AlertDialog.Builder(context).setTitle("消息提示")
                .setMessage("登陆超时，请重新登陆!")
                .setNegativeButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        // context.startActivity(new Intent(context,
                        // LoginActivity.class));
                        // ((Activity) context).finish();
                    }
                }).setCancelable(false).show();
    }
}
