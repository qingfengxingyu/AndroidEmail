package com.team.cores.dbmode.bean;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

/**
 * Description:下载数据库实体
 * Data: 2017/9/26 0026 下午 1:55
 *
 * @author: xmf
 */
@Entity
public class DownloadDBEntity {
//    private String downloadId;//唯一标识
    @Id
    private String packName;//包名--唯一标识
    private long toolSize;//文件总大小
    private long completedSize;//当前下载大小
    private String url;//文件下载地址
    private String saveDirPath;//sd卡存放地址
    private String fileName;//文件名称
    private Integer downloadStatus;//下载状态
    private String versioncode;//升级版本号
    private String versionName;//显示版本版本号
    private int appType;//应用类型 1---zip
    private String appName;//应用名称
    private String iconUrl;//应用图标地址
    private String appContent;//应用简介
    private String appStoreDir;//应用详情描述
    private int categorytype;//应用类型 0 安装任务 1 已安装任务 2 需要更新的任务  3显示不同的单个布局
    private String totaldowncount;//下载次数
    public String getTotaldowncount() {
        return this.totaldowncount;
    }
    public void setTotaldowncount(String totaldowncount) {
        this.totaldowncount = totaldowncount;
    }
    public int getCategorytype() {
        return this.categorytype;
    }
    public void setCategorytype(int categorytype) {
        this.categorytype = categorytype;
    }
    public String getAppStoreDir() {
        return this.appStoreDir;
    }
    public void setAppStoreDir(String appStoreDir) {
        this.appStoreDir = appStoreDir;
    }
    public String getAppContent() {
        return this.appContent;
    }
    public void setAppContent(String appContent) {
        this.appContent = appContent;
    }
    public String getIconUrl() {
        return this.iconUrl;
    }
    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }
    public String getAppName() {
        return this.appName;
    }
    public void setAppName(String appName) {
        this.appName = appName;
    }
    public int getAppType() {
        return this.appType;
    }
    public void setAppType(int appType) {
        this.appType = appType;
    }
    public String getVersionName() {
        return this.versionName;
    }
    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }
    public String getVersioncode() {
        return this.versioncode;
    }
    public void setVersioncode(String versioncode) {
        this.versioncode = versioncode;
    }
    public Integer getDownloadStatus() {
        return this.downloadStatus;
    }
    public void setDownloadStatus(Integer downloadStatus) {
        this.downloadStatus = downloadStatus;
    }
    public String getFileName() {
        return this.fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public String getSaveDirPath() {
        return this.saveDirPath;
    }
    public void setSaveDirPath(String saveDirPath) {
        this.saveDirPath = saveDirPath;
    }
    public String getUrl() {
        return this.url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public long getCompletedSize() {
        return this.completedSize;
    }
    public void setCompletedSize(long completedSize) {
        this.completedSize = completedSize;
    }
    public long getToolSize() {
        return this.toolSize;
    }
    public void setToolSize(long toolSize) {
        this.toolSize = toolSize;
    }
    public String getPackName() {
        return this.packName;
    }
    public void setPackName(String packName) {
        this.packName = packName;
    }
    @Generated(hash = 881901452)
    public DownloadDBEntity(String packName, long toolSize, long completedSize,
            String url, String saveDirPath, String fileName,
            Integer downloadStatus, String versioncode, String versionName,
            int appType, String appName, String iconUrl, String appContent,
            String appStoreDir, int categorytype, String totaldowncount) {
        this.packName = packName;
        this.toolSize = toolSize;
        this.completedSize = completedSize;
        this.url = url;
        this.saveDirPath = saveDirPath;
        this.fileName = fileName;
        this.downloadStatus = downloadStatus;
        this.versioncode = versioncode;
        this.versionName = versionName;
        this.appType = appType;
        this.appName = appName;
        this.iconUrl = iconUrl;
        this.appContent = appContent;
        this.appStoreDir = appStoreDir;
        this.categorytype = categorytype;
        this.totaldowncount = totaldowncount;
    }
    @Generated(hash = 1143139915)
    public DownloadDBEntity() {
    }


}
