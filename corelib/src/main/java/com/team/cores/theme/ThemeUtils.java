package com.team.cores.theme;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * 主题色控制
 */
public class ThemeUtils {
    /***
     * 默认主题色
     */
    public static final String baseColor = "#2684cf";//国网绿4eae3c 支付宝蓝2684cf
    /**
     * 主题换色
     */
    public static final String SHARED_ZHUTICOLOR = "narimiptheme_zhuticolor";
    public static SharedPreferences shared;
    public static Context context;


    public static int getBaseColor() {
        if (null != shared) {
            return Color.parseColor(shared.getString(SHARED_ZHUTICOLOR, baseColor));
        }
        return Color.parseColor(baseColor);
    }


    /***
     * 对TextView设置不同状态时其文字颜色。
     *
     * @param normal 默认颜色
     * @param unable 选中颜色
     * @return
     */
    public static ColorStateList createColorStateList(int normal, int unable) {
        int[] colors = new int[]{unable, normal};
        int[][] states = new int[2][];
        states[0] = new int[]{android.R.attr.state_checked};
        states[1] = new int[]{};
        ColorStateList colorList = new ColorStateList(states, colors);
        return colorList;
    }


    /***
     * 获得按钮圆角背景
     *
     * @param colorValue
     * @return
     */
    public static GradientDrawable getBtnBg(String colorValue) {
        GradientDrawable gd = new GradientDrawable();// 创建drawable
        // int strokeWidth = 3; //  边框宽度
        final int pageMargin = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 2, context.getResources()
                        .getDisplayMetrics());
        // int strokeColor = Color.parseColor("#78BD50");// 边框颜色
        gd.setColor(Color.parseColor(colorValue));// 内部填充颜色
        gd.setCornerRadius(pageMargin);
        // gd.setStroke(strokeWidth, strokeColor);//设置边框
        return gd;
    }

    /***
     * 设置ProgressBar样式
     *
     * @param progressBar
     */
    public static void setProgressBar(ProgressBar progressBar) {
        ClipDrawable d = new ClipDrawable(ThemeUtils.getBtnBg(shared.getString(ThemeUtils.SHARED_ZHUTICOLOR, ThemeUtils.baseColor)), Gravity.LEFT,
                ClipDrawable.HORIZONTAL);
        progressBar.setProgressDrawable(d);
    }

    /***
     * 设置Textview字色
     *
     * @param view
     */
    public static void setTextViewText(TextView view) {
        view.setTextColor(ThemeUtils.getBaseColor());
    }

    /***
     * 设置View背景色
     *
     * @param view
     */
    public static void setTextViewBag(View view) {
        view.setBackgroundColor(ThemeUtils.getBaseColor());
    }


    /**
     * 设置边框,和背景
     */
    public static GradientDrawable createShape(int strokeWidth, int roundRadius, int strokeColor, int fillColor) {
        //设置背景
//        button.setBackgroundDrawable(addStateDrawable(this, R.color.white, createShape(5, 15, Color.parseColor("#2E3135"), Color.parseColor("#DFDFE0")),
//                createShape(5, 15, Color.parseColor("#2E3135"), Color.parseColor("#DFDFE0"))));

        //设置颜色
//        button.setTextColor(createColorStateList(0xffffffff, 0xffffff00,
//                0xff0000ff, 0xffff0000));


//		int strokeWidth = 5; // 3dp 边框宽度
//		int roundRadius = 15; // 8dp 圆角半径
//		int strokeColor = Color.parseColor("#2E3135");// 边框颜色
//		int fillColor = Color.parseColor("#DFDFE0");// 内部填充颜色

        GradientDrawable gd = new GradientDrawable();// 创建drawable
        gd.setColor(fillColor);
        gd.setCornerRadius(roundRadius);
        gd.setStroke(strokeWidth, strokeColor);

        return gd;
    }


    /**
     * 对TextView设置不同状态时其文字颜色。
     */
    public static ColorStateList createColorStateList(int normal, int pressed,
                                                      int focused, int unable) {
        int[] colors = new int[]{pressed, focused, normal, focused, unable,
                normal, pressed};
        int[][] states = new int[7][];
        states[0] = new int[]{android.R.attr.state_pressed,
                android.R.attr.state_enabled};
        states[1] = new int[]{android.R.attr.state_enabled,
                android.R.attr.state_focused};
        states[2] = new int[]{android.R.attr.state_enabled};
        states[3] = new int[]{android.R.attr.state_checked};
        states[4] = new int[]{android.R.attr.state_checked};
        states[5] = new int[]{};
        states[6] = new int[]{android.R.attr.state_checked};
        ColorStateList colorList = new ColorStateList(states, colors);
        return colorList;

    }

    /**
     * 对View设置不同状态时其背景颜色。
     */
    public static StateListDrawable addStateDrawable(Context context, GradientDrawable idNormal,
                                                     GradientDrawable idPressed, GradientDrawable idFocused) {
        StateListDrawable sd = new StateListDrawable();
        Drawable normal = idNormal;
        Drawable pressed = idPressed;
        Drawable focus = idFocused;
        // 注意该处的顺序，只要有一个状态与之相配，背景就会被换掉
        // 所以不要把大范围放在前面了，如果sd.addState(new[]{},normal)放在第一个的话，就没有什么效果了
        sd.addState(new int[]{android.R.attr.state_enabled,
                android.R.attr.state_focused}, focus);
        sd.addState(new int[]{android.R.attr.state_pressed,
                android.R.attr.state_enabled}, pressed);
        sd.addState(new int[]{android.R.attr.state_focused}, focus);
        sd.addState(new int[]{android.R.attr.state_pressed}, pressed);
        sd.addState(new int[]{android.R.attr.state_checked}, idFocused);
        sd.addState(new int[]{android.R.attr.state_enabled}, normal);
        sd.addState(new int[]{}, normal);
        return sd;
    }

    public static StateListDrawable getDefaultStateListDrawable(Context context) {
        return ThemeUtils.addStateDrawable(context, ThemeUtils.createShape(0, 0, Color.parseColor("#00000000"), ThemeUtils.getBaseColor()), ThemeUtils.createShape(0, 0, Color.parseColor("#00000000"), Color.parseColor("#ECECEC")), ThemeUtils.createShape(0, 0, Color.parseColor("#00000000"), Color.parseColor("#ECECEC")));
    }

    /***
     * 防止按钮暴力点击
     *
     * @param v
     */
    public static void singleCheck(final View v) {
        v.setClickable(false);
        v.postDelayed(new Runnable() {
            @Override
            public void run() {
                v.setClickable(true);
            }
        }, 1000);
    }

    // 应用详情图片尺寸
    public static int Game_Detial_Width = 354;
    public static int Game_Detial_Height = 630;

    /***
     * 获得实际应用详情图片的高度
     *
     * @return
     */
    public static int getApp_Detail_HH(int ww) {
        return ww * Game_Detial_Height / Game_Detial_Width;
    }
}
