package com.xing.email.utils

import android.util.Log

/**
 *用于在专用后台线程上运行块的实用方法，用于IO/DATA工作
 * 只有一个单线程
 */
fun tryFun(f: () -> Unit) {
    try {
        f
    } catch (e: Exception) {
        if (null != e) {
            Log.e("xmf", "" + e.toString())
        }
    }

}