package com.team.cores.mvputil;

/**
 * 首页更多页面接口
 * Created by wfx on 2017/6/23.
 *
 */


public interface DragAdapterInterface {
	void reOrder(int startPosition, int endPosition);
}
