package com.team.cores.filedowm.cores;

import android.os.Handler;

import com.team.cores.dbmode.bean.DownloadDBEntity;
import com.team.cores.dbmode.download.DownloadStatus;
import com.team.cores.filedowm.ShopFileDownManager;
import com.team.cores.filedowm.configs.FileDownConfig;
import com.team.cores.filedowm.imp.HttpDownCore;
import com.team.cores.filedowm.impl.ShopHttpDwon;
import com.team.cores.filedowm.litening.ShopFileTaskListening;
import com.team.cores.filedowm.utils.DaoFileUtils;
import com.team.cores.utils.ConfigTools;
import com.team.cores.utils.LogTools;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

import okhttp3.OkHttpClient;

/**
 * author:xmf
 * date:2018/5/23 0023
 * description:专用于类似应用商店文件下载类和数据库绑定使用，UI通过Eventbus更新
 */
public class ShopFileTask implements Runnable {
    private final String TAG = "ShopFileTask_xmf";
    private DownloadDBEntity mBean;
    private ShopFileTaskListening mListening;//下载监听
    private OkHttpClient mOkHttpClient;
    private int erroCode = -1000;//下载线程出错错误码
    private String erroMsg;//下载线程出错原因描述
    private HttpDownCore mHttpDownCore = new ShopHttpDwon();//默认使用okhttp下载文件
    private FileDownConfig mFileDownConfig;
    private Handler mHandler = new Handler();
    private int downloadSpeed = 0;// 下载的 平均速度

    public ShopFileTask(DownloadDBEntity mBean) {
        this.mBean = mBean;
    }

    @Override
    public void run() {
        if (mBean.getDownloadStatus() != DownloadStatus.DOWNLOAD_STATUS_DOWNLOADING) {
            return;
        }
        try {
            File outputFile = new File(mBean.getSaveDirPath());
            LogTools.printLog(TAG, "run___downId___" + mBean.getPackName() + "--sd--" + mBean.getSaveDirPath() + "---sql_toolsize is:" + mBean.getToolSize());
            if (!outputFile.exists()) {
                outputFile.createNewFile();
            }
            if (outputFile.length() < mBean.getCompletedSize()) {//防止文件被删除，下载的文件长度以实际文件长度为主，数据库只是辅助记录
                mBean.setCompletedSize(outputFile.length());
            }
            //判断这个文件是不是已经下载完成了
            if (mBean.getToolSize() > 0) {
                File mFile = new File(mBean.getSaveDirPath());
                if (null != mFile && mFile.exists() && mFile.length() == mBean.getToolSize()) {
                    mBean.setDownloadStatus(DownloadStatus.DOWNLOAD_STATUS_COMPLETED);
                    mBean.setCompletedSize(mBean.getToolSize());
                    DaoFileUtils.getInstance().addDB(mBean);
                    onCompleted();
                    return;
                }
            }
            if (0 == mBean.getToolSize() || mBean.getCompletedSize() == 0) {
                outputFile.delete();
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                }
            }
            mHttpDownCore.setDownFlag(true);
            mHttpDownCore.httpDown(mOkHttpClient, ShopFileTask.this);//进入网络请求断点下载文件
        } catch (Exception ex) {
            LogTools.printLog(TAG, "run___downId___" + mBean.getPackName() + "___erro___" + ex.toString());
            mBean.setDownloadStatus(DownloadStatus.DOWNLOAD_STATUS_ERROR);
            setErroCode(DownloadStatus.DOWNLOAD_ERROR_CODE_1);
            setErroMsg(ex.toString());
        } finally {
            switch (mBean.getDownloadStatus()) {
                case DownloadStatus.DOWNLOAD_STATUS_CANCEL:
                case DownloadStatus.DOWNLOAD_STATUS_PAUSE:
                case DownloadStatus.DOWNLOAD_STATUS_UPDATE:
                    break;
                case DownloadStatus.DOWNLOAD_STATUS_ERROR:
                    onError();
                    break;
                default:
                    if (0 != mBean.getToolSize() && mBean.getCompletedSize() != 0 && mBean.getToolSize() <= mBean.getCompletedSize()) {
                        onCompleted();
                    } else {
                        onPause();
                    }
                    break;
            }
            LogTools.printLog(TAG, "___downId:___" + mBean.getPackName() + "___downName:___" + mBean.getAppName() == null ? "" : mBean.getAppName() + "___finally___" + DownloadStatus.getType(mBean.getDownloadStatus()));
        }

    }

    public void onDownloading() {
        if (null != mListening) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mListening.onDownloading(ShopFileTask.this);
                }
            });
        }
        EventBus.getDefault().post(this);
    }

    public void onPause() {
        mHttpDownCore.setDownFlag(false);
        mBean.setDownloadStatus(DownloadStatus.DOWNLOAD_STATUS_PAUSE);
        DaoFileUtils.getInstance().updateDownStatues(mBean.getPackName(), mBean.getDownloadStatus());
        if (null != mListening) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mListening.onPause(ShopFileTask.this);
                }
            });
        }
        EventBus.getDefault().post(this);
    }

    public void onCancel() {
        mHttpDownCore.setDownFlag(false);
        mBean.setDownloadStatus(DownloadStatus.DOWNLOAD_STATUS_CANCEL);
        if (null != mListening) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mListening.onCancel(ShopFileTask.this);
                }
            });
        }
        EventBus.getDefault().post(this);
    }

    public void onCompleted() {
        mHttpDownCore.setDownFlag(false);
        mBean.setDownloadStatus(DownloadStatus.DOWNLOAD_STATUS_COMPLETED);
        DaoFileUtils.getInstance().updateDownStatues(mBean.getPackName(), mBean.getDownloadStatus());
        if (null != mListening) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mListening.onCompleted(ShopFileTask.this);
                }
            });
        }
        EventBus.getDefault().post(this);
        ShopFileDownManager.getInstance().getmTaskList().remove(mBean.getPackName());
    }

    public void onError() {
        mHttpDownCore.setDownFlag(false);
        mBean.setDownloadStatus(DownloadStatus.DOWNLOAD_STATUS_ERROR);
        DaoFileUtils.getInstance().updateDownStatues(mBean.getPackName(), mBean.getDownloadStatus());
        if (null != mListening) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mListening.onError(ShopFileTask.this);
                }
            });
        }
        EventBus.getDefault().post(this);
        ShopFileDownManager.getInstance().getmTaskList().remove(mBean.getPackName());
    }

    public void setListening(ShopFileTaskListening mListening) {
        this.mListening = mListening;
    }

    public void setHttpClient(OkHttpClient mOkHttpClient) {
        if (null == this.mOkHttpClient) {
            this.mOkHttpClient = mOkHttpClient;
        }
    }

    public int getErroCode() {
        return erroCode;
    }

    public void setErroCode(int erroCode) {
        this.erroCode = erroCode;
    }

    public String getErroMsg() {
        return erroMsg;
    }

    public void setErroMsg(String erroMsg) {
        this.erroMsg = erroMsg;
    }

    public DownloadDBEntity getmBean() {
        return mBean;
    }

    public void setmBean(DownloadDBEntity mBean) {
        this.mBean = mBean;
    }

    public HttpDownCore getmHttpDownCore() {
        return mHttpDownCore;
    }

    public void setmHttpDownCore(HttpDownCore mHttpDownCore) {
        this.mHttpDownCore = mHttpDownCore;
    }

    /***
     * 获得进度数
     * @return
     */
    public float getPercent() {
        return mBean.getCompletedSize() * 100 / mBean.getToolSize();
    }

    public void setFileDownConfig(FileDownConfig mFileDownConfig) {
        this.mFileDownConfig = mFileDownConfig;
    }

    /***
     * 获得下载速度
     * @return
     */
    public int getDownloadSpeed() {
        return downloadSpeed;
    }

    public void setDownloadSpeed(int downloadSpeed) {
        this.downloadSpeed = downloadSpeed;
    }
}
