package com.team.cores.filedowm;

import com.team.cores.filedowm.bean.FileDownBean;
import com.team.cores.filedowm.configs.FileDownConfig;
import com.team.cores.filedowm.cores.FileTask;
import com.team.cores.filedowm.litening.FileTaskListening;
import com.team.cores.filedowm.utils.DaoFileUtils;
import com.team.cores.utils.LogTools;
import com.team.cores.utils.tools.LogTeamUtils;
import com.team.cores.utils.tools.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * author:xmf
 * date:2018/5/23 0023
 * description:普通文件下载管理类（不存数据库）
 */
public class FileDownManager {
    final static String TAG = "FileDownManager_xmf";
    private Map<String, FileTask> mTaskList = new HashMap<>();
    FileDownConfig mFileDownConfig;

    private FileDownManager() {
    }

    public void init(FileDownConfig mFileDownConfig) {
        this.mFileDownConfig = mFileDownConfig;

    }

    private static class FileLoaderHolder {
        private static final FileDownManager mInstance = new FileDownManager();
    }


    public static FileDownManager getInstance() {
        return FileLoaderHolder.mInstance;
    }


    /***
     * 添加新的任务
     * @param mBean
     */
    public void addTask(FileDownBean mBean) {
        addTask(mBean, null);
    }

    /***
     * 添加新的任务
     * @param mBean
     */
    public void addTask(FileDownBean mBean, FileTaskListening mListening) {
        if (null == mBean) {
            LogTeamUtils.e(TAG, "createNewTask___TaskDownBean___不能为空");
            return;
        }
        if (StringUtils.isEmpty(mBean.getDownloadId())) {
            LogTeamUtils.e(TAG, "createNewTask___DownloadId___不能为空");
            return;
        }
        if (StringUtils.isEmpty(mBean.getUrl())) {
            LogTeamUtils.e(TAG, "createNewTask___Url___不能为空");
            return;
        }
        FileTask mTask = mTaskList.get(mBean.getDownloadId());
        if (null == mTask) {
            mTask = new FileTask(mBean);
        }
        mTask.setHttpClient(mFileDownConfig.getOkHttpClient());
        mTask.setListening(mListening);
        mTask.onDownloading();
        if (null != mFileDownConfig.mExecutorService) {
            mTaskList.put(mBean.getDownloadId(), mTask);
            mFileDownConfig.mExecutorService.submit(mTask);
        }
    }

    /***
     * 暂停下载任务
     * @param downloadId
     */
    public void pauseTask(String downloadId) {
        if (null == downloadId) {
            LogTeamUtils.e(TAG, "pauseTask___downloadId不能为空");
            return;
        }
        FileTask mTask = mTaskList.get(downloadId);
        if (null != mTask) {
            mTask.onPause();
            if (null != mFileDownConfig) {
                mTaskList.remove(downloadId);
            }
        }

    }


    /***
     * 取消或者删除下载任务
     * @param downloadId
     */
    public void cancelOrDelTask(String downloadId) {
        if (null == downloadId) {
            LogTeamUtils.e(TAG, "cancelOrDelTask___downloadId不能为空");
            return;
        }
        DaoFileUtils.getInstance().delDB(downloadId);
        FileTask mTask = mTaskList.get(downloadId);
        if (null != mTask) {
            mTask.onCancel();
            mTaskList.remove(downloadId);
        }
    }

    public Map<String, FileTask> getmTaskList() {
        return mTaskList;
    }
}
