package com.team.cores.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * 表示不允许滚动的 ListView（也不显示滚动条）。
 */
public class NoScrollListView extends ListView
{
    /**
     * @see ListView#ListView(Context)
     * @param context
     */
    public NoScrollListView(Context context)
    {
        super(context);
    }

    /**
     * @see ListView#ListView(Context, AttributeSet)
     * @param context
     * @param attrs
     */
    public NoScrollListView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    /**
     * @see ListView#ListView(Context, AttributeSet, int)
     * @param context
     * @param attrs
     * @param defStyle
     */
    public NoScrollListView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}