package com.xing.email.utils.emailtool

import com.xing.email.app.MyApp
import java.io.File
import javax.mail.*

/**
 *author:xmf
 *date:2018/11/8 0008
 *description:邮件工具类,用于收发邮件、登录验证
 */
object MailTools {
    /**
     * 连接邮箱,登入操作
     *
     * @return
     */
    fun login() {
        //判断是否要登入验证
        var authenticator: MyAuthenticator? = null
        if (MyApp.mailInfo.isValidate) {
            //创建一个密码验证器
            authenticator = MyAuthenticator(MyApp.mailInfo.userName, MyApp.mailInfo.password)
        }
        // 根据邮件会话属性和密码验证器构造一个发送邮件的session
        val sendMailSession = Session.getDefaultInstance(MyApp.mailInfo.properties, authenticator)
        try {
            val transport = sendMailSession.getTransport("smtp")
            transport.connect(MyApp.mailInfo.mailServerHost, MyApp.mailInfo.userName, MyApp.mailInfo.password)
            MyApp.session = sendMailSession
        } catch (e: MessagingException) {
            e.printStackTrace()
            MyApp.session = null
        }
    }


    fun send(fileList: List<File>, toAdd: String, subject: String, content: String) {
        creatMail(toAdd, subject, content)
        val sms = MailSender()
        Thread(Runnable { sms.sendFileMail(MyApp.mailInfo, fileList) }).start()
    }

    fun send(toAdd: String, subject: String, content: String) {
        creatMail(toAdd, subject, content)
        val sms = MailSender()
        Thread(Runnable { sms.sendTextMail(MyApp.mailInfo) }).start()
    }

    private fun creatMail(toAdd: String, subject: String, content: String) {
        MyApp.mailInfo.toAddress = toAdd // 发到哪个邮件去
        MyApp.mailInfo.subject = subject// 邮件主题
        MyApp.mailInfo.content = content // 邮件文本
    }

    fun getEmail() {
        val protocol = "pop3"
        val session = Session.getDefaultInstance(MyApp.mailInfo.POP3_properties)
        var store: Store? = null
        var folder: Folder? = null
        try {
            store = session.getStore(protocol)
            store!!.connect(MyApp.mailInfo.userName, MyApp.mailInfo.password)

            folder = store.getFolder("INBOX")
            folder!!.open(Folder.READ_ONLY)

            val size = folder.messageCount
            val message = folder.getMessage(size)

            val from = message.from[0].toString()
            val subject = message.subject
            val date = message.sentDate

            println("From: $from")
            println("Subject: $subject")
            println("Date: $date")
        } catch (e: NoSuchProviderException) {
            e.printStackTrace()
        } catch (e: MessagingException) {
            e.printStackTrace()
        } finally {
            try {
                folder?.close(false)
                store?.close()
            } catch (e: MessagingException) {
                e.printStackTrace()
            }

        }
        println("接收完毕！")
    }
}