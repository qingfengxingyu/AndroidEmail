package com.team.cores.dbmode.download;

import android.app.DownloadManager;

public class DownloadStatus {
    /***
     * 初始状态---没有下载也没有安装
     */
    public static final int DOWNLOAD_STATUS_INIT = -1;
    /***
     * 应用需要更新---除了显示不一样，操作等同初始状态
     */
    public static final int DOWNLOAD_STATUS_UPDATE = 111;
    /***
     * 下载中
     */
    public static final int DOWNLOAD_STATUS_DOWNLOADING = DownloadManager.STATUS_RUNNING;
    /***
     * 暂停下载
     */
    public static final int DOWNLOAD_STATUS_PAUSE = DownloadManager.STATUS_PENDING;
    /***
     * 取消下载
     */
    public static final int DOWNLOAD_STATUS_CANCEL = DownloadManager.STATUS_PAUSED;
    /***
     * 下载出错
     */
    public static final int DOWNLOAD_STATUS_ERROR = DownloadManager.STATUS_FAILED;
    /***
     * 下载完成
     */
    public static final int DOWNLOAD_STATUS_COMPLETED = DownloadManager.STATUS_SUCCESSFUL;


    public static final int DOWNLOAD_ERROR_CODE_1 = 1;

    public static String getType(int nums) {
        String typeName = "";
        switch (nums) {
            case DOWNLOAD_STATUS_DOWNLOADING:
                typeName = "下载中";
                break;
            case DOWNLOAD_STATUS_CANCEL:
                typeName = "取消下载";
                break;
            case DOWNLOAD_STATUS_PAUSE:
                typeName = "暂停下载";
                break;
            case DOWNLOAD_STATUS_ERROR:
                typeName = "下载出错";
                break;
            case DOWNLOAD_STATUS_COMPLETED:
                typeName = "完成下载";
                break;
            default:
                typeName = "未下载";
                break;
        }
        return typeName;
    }
}
