package com.team.cores.views;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * 创建人;sijia
 * 创建时间:2017/7/6  9:39
 * 自适应高度LinearLayoutManager
 */

public class HeightLinearLayoutManager extends LinearLayoutManager {
    int tCount=0;
    /*
    * count为item的个数
    * */
    public HeightLinearLayoutManager(Context context,int count) {
        super(context);
        tCount=count;
    }

    @Override
    public void onMeasure(RecyclerView.Recycler recycler, RecyclerView.State state, int widthSpec, int heightSpec) {
        View view = recycler.getViewForPosition(0);
        if(view != null){
            measureChild(view, widthSpec, heightSpec);
            int measuredWidth = View.MeasureSpec.getSize(widthSpec);
            int measuredHeight = view.getMeasuredHeight();
            setMeasuredDimension(measuredWidth, measuredHeight*tCount);
        }
    }
}
