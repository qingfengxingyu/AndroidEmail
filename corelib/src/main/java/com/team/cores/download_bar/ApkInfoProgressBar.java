package com.team.cores.download_bar;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.team.cores.R;

public class ApkInfoProgressBar extends LinearLayout {

    private Context mContext;

    private AnimDownloadProgressButton animBtn;

    private ImageView imgCancel;

    private TextView txtCancel;

    public ApkInfoProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    public ApkInfoProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        mContext = context;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.bar_apk_download, null);
        addView(view, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        animBtn = (AnimDownloadProgressButton) findViewById(R.id.anim_btn);
        imgCancel = (ImageView) findViewById(R.id.img_cancel);
        txtCancel = (TextView) findViewById(R.id.txt_cancel);
        animBtn.initAttrs(mContext, attrs);
    }

    /**
     * 下载暂停按钮的点击监听
     *
     * @param listener
     */
    public void setAnimBtnClickListener(OnClickListener listener) {
        animBtn.setOnClickListener(listener);
    }

    /**
     * 设置取消图标的点击事件
     *
     * @param listener
     */
    public void setCancelListener(OnClickListener listener) {
        imgCancel.setOnClickListener(listener);
        txtCancel.setOnClickListener(listener);
    }

    /**
     * 重置下载暂停按钮状态
     */
    public void reset() {
        animBtn.setState(AnimDownloadProgressButton.NORMAL);
        imgCancel.setVisibility(GONE);
        txtCancel.setVisibility(GONE);
        animBtn.setCurrentText("下载");
    }

    /**
     * 设置进度条和显示文字
     *
     * @param progress 进度
     * @param isPause  是否为暂停状态
     */
    public void setProgressAndTxt(int progress, boolean isPause) {
        imgCancel.setVisibility(VISIBLE);
        txtCancel.setVisibility(VISIBLE);
        animBtn.setProgress(progress);
        if (progress < 100) {
            animBtn.setState(AnimDownloadProgressButton.DOWNLOADING);
            if (isPause) {
                animBtn.setCurrentText("继续");
            } else {
                animBtn.setProgressText("", progress);
            }
        } else {
            animBtn.setCurrentText("安装");
            animBtn.setTextCoverColor(getResources().getColor(R.color.white));
            animBtn.setmBackgroundColor(getResources().getColor(R.color.down_blue));
            animBtn.setState(AnimDownloadProgressButton.INSTALLING);
        }
    }

    public int getState() {
        return animBtn.getState();
    }
}
