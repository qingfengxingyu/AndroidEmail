package com.team.kotlin.util

import java.util.concurrent.Executors

/***
 * 创建一个单线程的线程池。
 * 这个线程池只有一个线程在工作，也就是相当于单线程串行执行所有任务。
 * 如果这个唯一的线程因为异常结束，那么会有一个新的线程来替代它。
 * 此线程池保证所有任务的执行顺序按照任务的提交顺序执行。
 */
private val IO_EXECUTOR = Executors.newSingleThreadExecutor()
/****
 * 创建固定大小的线程池。
 * 每次提交一个任务就创建一个线程，直到线程达到线程池的最大大小。
 * 线程池的大小一旦达到最大值就会保持不变，如果某个线程因为执行异常而结束，那么线程池会补充一个新线程。
 */
private val NETWORK_IO = Executors.newFixedThreadPool(10)

/**
 *用于在专用后台线程上运行块的实用方法，用于IO/DATA工作
 * 只有一个单线程
 */
fun ioThread(f: () -> Unit) {
    IO_EXECUTOR.execute(f)
}

/**
 *用于在专用后台线程上运行块的实用方法，用于IO/DATA工作
 * 10个线程
 */
fun netWorkIOThread(f: () -> Unit) {
    NETWORK_IO.execute(f)
}
