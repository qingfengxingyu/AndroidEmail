package com.xing.email.utils.emailtool

/**
 * author:xmf
 * date:2018/11/6 0006
 * description:邮件发送类
 */

import com.team.cores.utils.tools.LogTeamUtils
import com.xing.email.app.MyApp
import com.xing.email.bean.MailInfo
import java.io.File
import java.util.*
import javax.activation.DataHandler
import javax.activation.FileDataSource
import javax.mail.Message
import javax.mail.MessagingException
import javax.mail.Transport
import javax.mail.internet.*
import kotlin.collections.ArrayList

/**
 * 发送器
 */
class MailSender {
    /**
     * 以文本格式发送邮件
     * @param mailInfo 待发送的邮件的信息
     */
    fun sendTextMail(mailInfo: MailInfo): Boolean {
        try {
            // 根据session创建一个邮件消息
            val mailMessage = MimeMessage(MyApp.session)
            // 创建邮件发送者地址
            val from = InternetAddress(mailInfo.fromAddress)
            // 设置邮件消息的发送者
            mailMessage.setFrom(from)
            // 创建邮件的接收者地址，并设置到邮件消息中
            val to = InternetAddress(mailInfo.toAddress)
            mailMessage.setRecipient(Message.RecipientType.TO, to)
            // 设置邮件消息的主题
            mailMessage.subject = mailInfo.subject
            // 设置邮件消息发送的时间
            mailMessage.sentDate = Date()

            // 设置邮件消息的主要内容
            val mailContent = mailInfo.content
            mailMessage.setText(mailContent)
            // 发送邮件
            Transport.send(mailMessage)
            return true
        } catch (ex: MessagingException) {
            ex.printStackTrace()
        }

        return false
    }


    /**
     * 发送多个带附件的邮件
     * @param info
     * @return
     */
    fun sendFileMail(info: MailInfo, fileList: List<File>): Boolean {
        val attachmentMail = createAttachmentMail(info, fileList)
        try {
            Transport.send(attachmentMail!!)
            return true
        } catch (e: MessagingException) {
            e.printStackTrace()
            return false
        }

    }

    /**
     * 发送单个带附件的邮件
     * @param info
     * @return
     */
    fun sendFileMail(info: MailInfo, file: File): Boolean {
        var fileList: ArrayList<File> = ArrayList()
        fileList.add(file)
        val attachmentMail = createAttachmentMail(info, fileList)
        try {
            Transport.send(attachmentMail!!)
            return true
        } catch (e: MessagingException) {
            e.printStackTrace()
            return false
        }

    }

    /**
     * 创建带有附件的邮件---可以多附件上传
     * @return
     */
    private fun createAttachmentMail(info: MailInfo, fileList: List<File>): Message? {
        //创建邮件
        var message: MimeMessage? = null
        try {
            message = MimeMessage(MyApp.session)
            // 设置邮件的基本信息
            //创建邮件发送者地址
            val from = InternetAddress(info.fromAddress)
            //设置邮件消息的发送者
            message.setFrom(from)
            //创建邮件的接受者地址，并设置到邮件消息中
            val to = InternetAddress(info.toAddress)
            //设置邮件消息的接受者, Message.RecipientType.TO属性表示接收者的类型为TO
            message.setRecipient(Message.RecipientType.TO, to)
            //邮件标题
            message.subject = info.subject

            //------------------------- 创建邮件正文-----，为了避免邮件正文中文乱码问题，需要使用CharSet=UTF-8指明字符编码
            val text = MimeBodyPart()
            text.setContent(info.content, "text/html;charset=UTF-8")

            // 创建容器描述数据关系
            val mp = MimeMultipart()
            mp.addBodyPart(text)
            // -----------------------创建邮件附件------Begin
            if (null != fileList && fileList.size > 0) {
                for (file in fileList) {
                    //第一个附件
                    val attach1 = MimeBodyPart()
                    val ds1 = FileDataSource(file)
                    val dh1 = DataHandler(ds1)
                    attach1.dataHandler = dh1
                    attach1.fileName = MimeUtility.encodeText(dh1.name)
                    mp.addBodyPart(attach1)
                }
            }
//
//
//            //第二个附件
//            val attach2 = MimeBodyPart()
//            val ds2 = FileDataSource(file)
//            val dh2 = DataHandler(ds2)
//            attach2.dataHandler = dh2
//            attach2.fileName = MimeUtility.encodeText(dh2.name)
//            mp.addBodyPart(attach2)
            // -----------------------创建邮件附件------End
            mp.setSubType("mixed")
            message.setContent(mp)
            message.saveChanges()

        } catch (e: Exception) {
            LogTeamUtils.e("TAG", "创建带附件的邮件失败" + e.toString())
        }

        // 返回生成的邮件
        return message
    }

    companion object {

        /**
         * 以HTML格式发送邮件
         * @param mailInfo 待发送的邮件信息
         */
        fun sendHtmlMail(mailInfo: MailInfo): Boolean {
            try {
                // 根据session创建一个邮件消息
                val mailMessage = MimeMessage(MyApp.session)
                // 创建邮件发送者地址
                val from = InternetAddress(mailInfo.fromAddress)
                // 设置邮件消息的发送者
                mailMessage.setFrom(from)
                // 创建邮件的接收者地址，并设置到邮件消息中
                val to = InternetAddress(mailInfo.toAddress)
                // Message.RecipientType.TO属性表示接收者的类型为TO
                mailMessage.setRecipient(Message.RecipientType.TO, to)
                // 设置邮件消息的主题
                mailMessage.subject = mailInfo.subject
                // 设置邮件消息发送的时间
                mailMessage.sentDate = Date()
                // MiniMultipart类是一个容器类，包含MimeBodyPart类型的对象
                val mainPart = MimeMultipart()
                // 创建一个包含HTML内容的MimeBodyPart
                val html = MimeBodyPart()
                // 设置HTML内容
                html.setContent(mailInfo.content, "text/html; charset=utf-8")
                mainPart.addBodyPart(html)
                // 将MiniMultipart对象设置为邮件内容
                mailMessage.setContent(mainPart)
                // 发送邮件
                Transport.send(mailMessage)
                return true
            } catch (ex: MessagingException) {
                LogTeamUtils.e("TAG", "sendHtmlMail:" + ex.toString())
            }

            return false
        }
    }
}

