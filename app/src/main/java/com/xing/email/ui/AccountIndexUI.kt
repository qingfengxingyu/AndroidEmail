package com.xing.email.ui

import android.content.Intent
import android.os.Bundle
import com.xing.email.R
import com.xing.email.app.BaseUI
import com.xing.email.app.MyApp
import com.xing.email.views.ConfirmPopWindow
import kotlinx.android.synthetic.main.ui_account_index.*

/**
 *author:xmf
 *date:2018/11/7 0007
 *description:账户首页
 */
class AccountIndexUI : BaseUI() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ui_account_index)
        initAll()

    }

    override fun initData() {
        myTopBar.setTitle(MyApp.mailInfo.userName)
        myTopBar.showRightImageBtn(R.drawable.jia_icon)

    }

    override fun initEven() {
        myTopBar.right_action.setOnClickListener {
            ConfirmPopWindow(this).showAtBottom(myTopBar.right_action);
        }
        shoujian.setOnClickListener {
        }
        caogao.setOnClickListener {
        }
        yifasong.setOnClickListener {
            startActivity(Intent(mContext, SendEmailUI::class.java))
        }
        yishanchu.setOnClickListener {
            startActivity(Intent(mContext, SendEmailUI::class.java))
        }
        lajixiang.setOnClickListener {
            startActivity(Intent(mContext, SendEmailUI::class.java))
        }
    }

}
