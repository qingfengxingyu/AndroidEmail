package com.team.cores.utils;

import android.content.Context;

import com.team.cores.utils.tools.LogTeamUtils;

import java.io.File;


/**
 * author:xmf
 * date:2018/10/11 0011
 * description:用于存放txt、img、apk等文件的工具集合类
 */
public class FileSaveTool {
    private String baseFile = null;
    private Context mContext;
    private final String baseFileName = "mip";
    private final String TAG = FileSaveTool.class.getName();

    private FileSaveTool() {
    }

    private static class Hodler extends FileSaveTool {
        private static final FileSaveTool mInstance = new FileSaveTool();
    }

    public static FileSaveTool getInstance() {
        return Hodler.mInstance;
    }

    /***
     * 初始化工具
     * @param mContext
     */
    public void initContext(Context mContext) {
        this.mContext = mContext;
        getBaseFile();
    }

    /***
     * 获得最原始的存储路径
     * @return
     */
    public String getBaseFile() {
        if (baseFile == null) {
            boolean hasSDCard = android.os.Environment.MEDIA_MOUNTED
                    .equals(android.os.Environment.getExternalStorageState());
            // 优先存储SD卡上,如果SD卡不存在,则存储在平台的私有存储目录下：/data/data/包名
            try {
                if (hasSDCard) {
                    File path = mContext.getExternalFilesDir(baseFileName);
                    //兼容部分机型无Android/data目录的问题
                    if (path == null) {
                        baseFile = android.os.Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + baseFileName;
                    } else {
                        baseFile = path.getAbsolutePath();
                    }
                } else {
                    baseFile = mContext.getPackageManager().getApplicationInfo(mContext.getPackageName(), 0).dataDir + File.separator + baseFileName;
                }
            } catch (Exception e) {
                LogTeamUtils.e(TAG, "创建平台外部存储目录失败: " + e.getMessage());
            }
        }
        if (!FileUtils.isFileExists(baseFile)) {
            FileUtils.createOrExistsDir(baseFile);
        }
        return baseFile;
    }

    /**
     * SD卡--日志目录
     */
    public String getLogSd() {
        String sd = getBaseFile() + File.separator + "logs";
        if (!FileUtils.isFileExists(sd)) {
            FileUtils.createOrExistsDir(sd);
        }
        return sd;
    }

    /**
     * SD卡--Main日志目录
     */
    public String getMainLogSd() {
        String sd = getBaseFile() + File.separator + "mainlogs";
        if (!FileUtils.isFileExists(sd)) {
            FileUtils.createOrExistsDir(sd);
        }
        return sd;
    }

    /**
     * SD卡--图片存放目录
     */
    public String getImgSd() {
        String sd = getBaseFile() + File.separator + "imgs";
        if (!FileUtils.isFileExists(sd)) {
            FileUtils.createOrExistsDir(sd);
        }
        return sd;
    }

    /**
     * SD卡-存在下载的文件
     */
    public String getDownFileSd() {
        String sd = getBaseFile() + File.separator + "downfiles";
        if (!FileUtils.isFileExists(sd)) {
            FileUtils.createOrExistsDir(sd);
        }
        return sd;
    }

    /**
     * SD卡-存在apk和zip下载的文件
     */
    public String getApkSd() {
        String sd = getDownFileSd() + File.separator + "apks";
        if (!FileUtils.isFileExists(sd)) {
            FileUtils.createOrExistsDir(sd);
        }
        return sd;
    }

    /**
     * SD卡-存在zip解压后的文件
     */
    public String getZipSd() {
        String sd = getDownFileSd() + File.separator + "zips";
        if (!FileUtils.isFileExists(sd)) {
            FileUtils.createOrExistsDir(sd);
        }
        return sd;
    }

    /**
     * SD卡-存放crash的文件
     */
    public String getCrashSd() {
        String sd = getBaseFile() + File.separator + "crash";
        if (!FileUtils.isFileExists(sd)) {
            FileUtils.createOrExistsDir(sd);
        }
        return sd;
    }
}
