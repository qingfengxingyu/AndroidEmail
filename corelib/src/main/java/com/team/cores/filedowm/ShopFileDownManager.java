package com.team.cores.filedowm;

import com.team.cores.dbmode.bean.DownloadDBEntity;
import com.team.cores.dbmode.download.DownloadStatus;
import com.team.cores.dbmode.tools.GreenDaoHelper;
import com.team.cores.filedowm.configs.FileDownConfig;
import com.team.cores.filedowm.cores.ShopFileTask;
import com.team.cores.filedowm.litening.ShopFileTaskListening;
import com.team.cores.filedowm.utils.DaoFileUtils;
import com.team.cores.utils.ConfigTools;
import com.team.cores.utils.FileSaveTool;
import com.team.cores.utils.LogTools;
import com.team.cores.utils.tools.CleanUtils;
import com.team.cores.utils.tools.LogTeamUtils;
import com.team.cores.utils.tools.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mygreendao.DownloadDBEntityDao;

/**
 * author:xmf
 * date:2018/5/23 0023
 * description:商店文件下载专用管理类
 */
public class ShopFileDownManager {
    final static String TAG = "ShopFileDownManager_xmf";
    private Map<String, ShopFileTask> mTaskList = new HashMap<>();
    FileDownConfig mFileDownConfig;

    private ShopFileDownManager() {
    }

    public void init(FileDownConfig mFileDownConfig) {
        this.mFileDownConfig = mFileDownConfig;

    }

    private static class FileLoaderHolder {
        private static final ShopFileDownManager mInstance = new ShopFileDownManager();
    }


    public static ShopFileDownManager getInstance() {
        return FileLoaderHolder.mInstance;
    }


    /***
     * 添加新的任务
     * @param mBean
     */
    public void addTask(DownloadDBEntity mBean) {
        addTask(mBean, null);
    }

    /***
     * 添加新的任务
     * @param mBean
     */
    public void addTask(DownloadDBEntity mBean, ShopFileTaskListening mListening) {
        if (null == mBean) {
            LogTeamUtils.e(TAG, "createNewTask___TaskDownBean___不能为空");
            return;
        }
        if (StringUtils.isEmpty(mBean.getPackName())) {
            LogTeamUtils.e(TAG, "createNewTask___DownloadId___不能为空");
            return;
        }
        if (StringUtils.isEmpty(mBean.getUrl())) {
            LogTeamUtils.e(TAG, "createNewTask___Url___不能为空");
            return;
        }
        ShopFileTask mTask = mTaskList.get(mBean.getPackName());
        if (null == mTask) {
            mTask = new ShopFileTask(mBean);
        }
        mTask.setHttpClient(mFileDownConfig.getOkHttpClient());
        mTask.setListening(mListening);
        mTask.setFileDownConfig(mFileDownConfig);
        mTask.getmBean().setDownloadStatus(DownloadStatus.DOWNLOAD_STATUS_DOWNLOADING);
        mTask.setmBean(DaoFileUtils.getInstance().addDB(mBean));
        mTask.onDownloading();
        if (null != mFileDownConfig.mExecutorService) {
            mTaskList.put(mBean.getPackName(), mTask);
            mFileDownConfig.mExecutorService.submit(mTask);
        }
    }

    /***
     * 暂停下载任务
     * @param downloadId 应用的包名
     */
    public void pauseTask(String downloadId) {
        if (null == downloadId) {
            LogTeamUtils.e(TAG, "pauseTask___downloadId不能为空");
            return;
        }
        ShopFileTask mTask = mTaskList.get(downloadId);
        if (null != mTask) {
            mTask.onPause();
            if (null != mFileDownConfig) {
                mTaskList.remove(downloadId);
            }
        }

    }


    /***
     * 取消或者删除下载任务
     * @param downloadId 应用的包名
     */
    public void cancelOrDelTask(String downloadId) {
        if (null == downloadId) {
            LogTeamUtils.e(TAG, "cancelOrDelTask___downloadId不能为空");
            return;
        }
        DaoFileUtils.getInstance().delDB(downloadId);
        ShopFileTask mTask = mTaskList.get(downloadId);
        if (null != mTask) {
            mTask.onCancel();
            mTaskList.remove(downloadId);
        }

    }

    public Map<String, ShopFileTask> getmTaskList() {
        return mTaskList;
    }

    public FileDownConfig getmFileDownConfig() {
        return mFileDownConfig;
    }

    /***
     * 下载所有进行中的下载任务
     */
    public void runLastAllDown() {
        List<DownloadDBEntity> dataList = GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().queryBuilder().where(DownloadDBEntityDao.Properties.DownloadStatus.eq(DownloadStatus.DOWNLOAD_STATUS_DOWNLOADING)).list();
        if (null != dataList && dataList.size() > 0) {
            for (DownloadDBEntity iBean : dataList) {
                ShopFileDownManager.getInstance().pauseTask(iBean.getPackName());
                ShopFileDownManager.getInstance().addTask(iBean);
                LogTeamUtils.e("task_xmf", "downloadTask is:" + iBean.getAppName());
            }
        }
    }

    /***
     * 清楚下载数据库以及下载文件夹
     */
    public void clearAllDB() {
        try {
            GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().deleteAll();
            CleanUtils.cleanCustomCache(FileSaveTool.getInstance().getApkSd());//清楚存放下载文件夹
            CleanUtils.cleanCustomCache(FileSaveTool.getInstance().getZipSd());//清楚存放下载文件夹
        } catch (Exception e) {
            LogTeamUtils.e(e.toString());
        }
    }
}
