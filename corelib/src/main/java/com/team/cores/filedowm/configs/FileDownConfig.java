package com.team.cores.filedowm.configs;

import android.content.Context;

import com.team.cores.configs.ProjectConfigs;
import com.team.cores.dbmode.tools.GreenDaoHelper;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import okhttp3.OkHttpClient;

/**
 * author:xmf
 * date:2018/5/23 0023
 * description:文件管理初始化类
 */
public class FileDownConfig {
    public final int mPoolSize = 5;
    private int nowPoolSize;
    private OkHttpClient mOkHttpClient;
    private boolean md5;
    public ExecutorService mExecutorService;
    private Context mContext;
    /***
     * true 添加https认证 false不添加https认证 默认false
     */
    boolean isHttps;

    private FileDownConfig() {
    }

    public static final class Builder {
        boolean isHttps;
        int nowPoolSize;
        OkHttpClient mOkHttpClient;
        Context mContext;
        boolean md5;

        public Builder() {//可以在这初始化一些数据
        }

        /***
         * https认证开启关闭
         * @param isHttps true 添加https认证 false不添加https认证 默认false
         * @return
         */
        public Builder setHttps(boolean isHttps) {
            this.isHttps = isHttps;
            return this;
        }

        /***
         * 设置线程池数量
         * @param nowPoolSize
         */
        public Builder setThreadCount(int nowPoolSize) {
            this.nowPoolSize = nowPoolSize;
            return this;
        }

        /***
         * 自定义OkHttpClient初始化设置
         * @param mOkHttpClient
         * @return
         */
        public Builder setOkHttpClient(OkHttpClient mOkHttpClient) {
            this.mOkHttpClient = mOkHttpClient;
            return this;
        }

        /***
         * 设置文件名称是否使用md5加密
         * @param md5 true 加密 false 不加密 （默认不加密)
         * @return
         */
        public Builder setMd5(boolean md5) {
            this.md5 = md5;
            return this;
        }

        public Builder setContext(Context mContext) {
            this.mContext = mContext;
            return this;
        }

        public void applyConfig(FileDownConfig mConfig) {
            mConfig.isHttps = this.isHttps;
            mConfig.nowPoolSize = this.nowPoolSize;
            mConfig.mOkHttpClient = this.mOkHttpClient;
            mConfig.md5 = this.md5;
            mConfig.mContext = this.mContext;
        }

        public FileDownConfig create() {
            FileDownConfig fileDownConfig = new FileDownConfig();
            applyConfig(fileDownConfig);
            fileDownConfig.initFileLoader();
            return fileDownConfig;
        }
    }

    /***
     * 初始化配置
     */
    private void initFileLoader() {
        GreenDaoHelper.getInstance().initDatabase(mContext);
        if (null == mOkHttpClient) {
            OkHttpClient.Builder buider = new OkHttpClient.Builder();
            buider.connectTimeout(20, TimeUnit.SECONDS);
            buider.writeTimeout(20, TimeUnit.SECONDS);
            buider.readTimeout(90, TimeUnit.SECONDS);
            if (isHttps) {
                final HostnameVerifier hostnameVerifier = new HostnameVerifier() {
                    @Override
                    public boolean verify(final String hostname,
                                          final SSLSession session) {
                        return true;
                    }
                };
                buider.hostnameVerifier(hostnameVerifier);
                buider.sslSocketFactory(initCertificates());
            }
            mOkHttpClient = buider.build();
        }
        if (null != mExecutorService) {
            mExecutorService.shutdown();
        }
        mExecutorService = null;
        if (nowPoolSize > 0) {
            mExecutorService = Executors.newFixedThreadPool(nowPoolSize);
        } else {
            mExecutorService = Executors.newFixedThreadPool(mPoolSize);
        }

    }


    /***
     * 添加iss证书
     * @return
     */
    public static SSLSocketFactory initCertificates() {
        CertificateFactory certificateFactory;
        SSLContext sslContext = null;
        try {
            certificateFactory = CertificateFactory.getInstance("X.509");
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(null);
            String certificateAlias = Integer.toString(0);
            keyStore.setCertificateEntry(
                    certificateAlias,
                    certificateFactory.generateCertificate(ProjectConfigs.getInstance().getApp().getAssets().open("mip_server.cer")));// 拷贝好的证书
            sslContext = SSLContext.getInstance("TLS");

            TrustManagerFactory trustManagerFactory = TrustManagerFactory
                    .getInstance(TrustManagerFactory.getDefaultAlgorithm());

            trustManagerFactory.init(keyStore);
            sslContext.init(null, trustManagerFactory.getTrustManagers(),
                    new SecureRandom());
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (sslContext != null) {
            return sslContext.getSocketFactory();
        }
        return null;
    }

    public OkHttpClient getOkHttpClient() {
        return mOkHttpClient;
    }

    public int getNowPoolSize() {
        return nowPoolSize;
    }


    public boolean getMd5() {
        return md5;
    }


    public boolean getHttps() {
        return isHttps;
    }

    public Context getmContext() {
        return mContext;
    }

}
