package com.team.cores.utils.tools;

import android.text.Html;
import android.text.Spanned;

/**
 * author:xmf
 * date:2018/6/13 0013
 * description:html相关的方法类
 */
public class HtmlUtils {
    /***
     * html代码显示在TextView
     * @param mValues
     * @param flags  FROM_HTML_MODE_COMPACT：html块元素之间使用一个换行符分 FROM_HTML_MODE_LEGACY：html块元素之间使用两个换行符分隔
     * @return
     */
    public static Spanned fromHtml(String mValues, int flags) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            if (StringUtils.isEmpty(mValues)) {
                return Html.fromHtml("", flags);
            }
            return Html.fromHtml(mValues, flags);
        } else {
            if (StringUtils.isEmpty(mValues)) {
                return Html.fromHtml("");
            }
            return Html.fromHtml(mValues);
        }
    }

    /***
     * html代码显示在TextView
     * @param mValues
     * @return
     */
    public static Spanned fromHtml(String mValues) {
        return fromHtml(mValues, Html.FROM_HTML_MODE_LEGACY);
    }

}
