package com.xing.email.views;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xing.email.R;


/***
 * 头部公共View
 *
 * @author xmf
 */
public class TopBarCommonView extends LinearLayout {

    public Context mContext;
    public LinearLayout tv_back;//返回
    public TextView top_title;//标题
    private TextView tv_close;//关闭按钮
    public TextView right_btn;//右边按钮
    public TextView left_btn;//左边按钮
    public LinearLayout right_action;
    private TextView right_action_image;

    public TopBarCommonView(Context context, AttributeSet attr) {
        super(context, attr);
        initView(context, attr);
    }

    public TopBarCommonView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView(context, attrs);
    }

    public TopBarCommonView(Context context) {
        super(context);
        this.mContext = mContext;
        initView(context);
    }

    private void initView(Context context) {
        init(context);
        initEvent();
    }


    private void initView(Context context, AttributeSet attrs) {
        mContext = context;
        init(context);
        initData(attrs);
        initEvent();

    }

    public void init(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.topbar_view, null);
        this.addView(view, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        tv_back = findViewById(R.id.tv_back);
        top_title = findViewById(R.id.top_title);
        tv_close = findViewById(R.id.tv_close);
        right_btn = findViewById(R.id.right_btn);
        right_action = findViewById(R.id.right_action);
        left_btn = findViewById(R.id.left_btn);
        right_action_image = findViewById(R.id.right_action_image);
    }

    private void initData(AttributeSet attrs) {
        TypedArray a = mContext.obtainStyledAttributes(attrs,
                R.styleable.TopBarCommonView);
        String title_name = a.getString(R.styleable.TopBarCommonView_titlename);
        boolean backShow = a.getBoolean(R.styleable.TopBarCommonView_backshow, false);
        if (null != title_name && title_name.length() > 0) {
            top_title.setText(title_name);
        } else {
            top_title.setText("");
        }
        if (backShow) {
            tv_back.setVisibility(VISIBLE);
        } else {
            tv_back.setVisibility(GONE);
        }
    }

    private void initEvent() {
        tv_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Activity) mContext).finish();
            }
        });
        tv_close.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Activity) mContext).finish();
            }
        });

    }

    /**
     * 设置标题
     *
     * @param title
     */
    public void setTitle(String title) {
        top_title.setText(title);
    }


    /***
     * 显示关闭按钮
     */
    public void showClose() {
        tv_close.setVisibility(VISIBLE);
    }

    /***
     * 显示返回图标按钮
     */
    public void showLeftBack() {
        tv_back.setVisibility(View.VISIBLE);
    }

    /***
     * 左边文字按钮
     * @param values
     */
    public void showLeftBtn(String values) {
        if (!TextUtils.isEmpty(values)) {
            left_btn.setText(values);
        }
        left_btn.setVisibility(View.VISIBLE);
    }

    /***
     * 显示右边文字按钮
     * @param values
     */
    public void showRightTextBtn(String values) {
        if (!TextUtils.isEmpty(values)) {
            right_btn.setText(values);
        }
        right_action.setVisibility(View.VISIBLE);
        right_btn.setVisibility(View.VISIBLE);
    }

    /***
     * 显示右边图片按钮
     * @param imgId
     */
    public void showRightImageBtn(int imgId) {
        if (imgId > 0) {
            right_action_image.setBackgroundResource(imgId);
        }
        right_action.setVisibility(View.VISIBLE);
        right_action_image.setVisibility(View.VISIBLE);
    }

}
