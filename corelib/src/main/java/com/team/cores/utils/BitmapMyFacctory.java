package com.team.cores.utils;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;

import java.io.File;
import java.io.FileInputStream;

@TargetApi(Build.VERSION_CODES.DONUT)
public class BitmapMyFacctory {
    @SuppressWarnings("resource")
    @SuppressLint("NewApi")
    public static Bitmap getBitmapfromfile(String fileName) {
        BitmapFactory.Options bfOptions = new BitmapFactory.Options();
        bfOptions.inDither = false;
        bfOptions.inPurgeable = true;
        bfOptions.inTempStorage = new byte[12 * 1024];
        bfOptions.inJustDecodeBounds = false;
        File file = new File(fileName);
        FileInputStream fs = null;
        try {
            fs = new FileInputStream(file);
            if (fs != null)
                return BitmapFactory.decodeFileDescriptor(fs.getFD(), null,
                        bfOptions);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /***
     * 判断图片文件是否下载完成用专用
     *
     * @param fileName
     * @return
     */
    public static Bitmap checkBitmap(String fileName) {
        File dst = new File(fileName);
        if (null != dst && dst.exists()) {
            BitmapFactory.Options opts = null;
            opts = new BitmapFactory.Options(); // 设置inJustDecodeBounds为true后，decodeFile并不分配空间，此时计算原始图片的长度和宽度
            opts.inJustDecodeBounds = true;
            opts.inPreferredConfig = Bitmap.Config.RGB_565;
            BitmapFactory.decodeFile(dst.getPath(), opts);
            opts.inSampleSize = 100;
            opts.inJustDecodeBounds = false;
            opts.inInputShareable = true;
            opts.inPurgeable = true;
            try {
                return BitmapFactory.decodeFile(dst.getPath(), opts);
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /***
     * 获取的bitmap不高于500KB
     *
     * @param files
     * @return
     */
    @SuppressLint("NewApi")
    @SuppressWarnings("resource")
    public static Bitmap getBitmapfromfile(File files) {
        BitmapFactory.Options bfOptions = new BitmapFactory.Options();
        bfOptions.inDither = false;
        bfOptions.inPurgeable = true;
        bfOptions.inPreferredConfig = Bitmap.Config.RGB_565;
        bfOptions.inTempStorage = new byte[12 * 1024];
        bfOptions.inJustDecodeBounds = false;
        FileInputStream fs = null;
        try {
            fs = new FileInputStream(files);
            if (fs != null)
                return ImageLoadUtils.compressImage(BitmapFactory
                        .decodeFileDescriptor(fs.getFD(), null, bfOptions));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Bitmap getBitmapFromFile(Context context, int id, int width,
                                           int height) {
        if (id > 0) {
            BitmapFactory.Options opts = null;
            if (width > 0 && height > 0) {
                opts = new BitmapFactory.Options();
                opts.inJustDecodeBounds = true;
                BitmapFactory.decodeResource(context.getResources(), id, opts);

                // 计算图片缩放比例
                final int minSideLength = Math.min(width, height);
                opts.inSampleSize = computeSampleSize(opts, minSideLength,
                        width * height);
                opts.inJustDecodeBounds = false;
                opts.inInputShareable = true;
                opts.inPurgeable = true;
            }
            try {
                return BitmapFactory.decodeResource(context.getResources(), id,
                        opts);
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @SuppressLint("NewApi")
    public static Bitmap getBitmapFromFile(File dst, int width, int height) {
        if (null != dst && dst.exists()) {
            BitmapFactory.Options opts = null;
            if (width > 0 && height > 0) {
                opts = new BitmapFactory.Options(); // 设置inJustDecodeBounds为true后，decodeFile并不分配空间，此时计算原始图片的长度和宽度
                opts.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(dst.getPath(), opts);
                // 计算图片缩放比例
                final int minSideLength = Math.min(width, height);
                opts.inSampleSize = computeSampleSize(opts, minSideLength,
                        width * height); // 这里一定要将其设置回false，因为之前我们将其设置成了true
                opts.inJustDecodeBounds = false;
                opts.inInputShareable = true;
                opts.inPurgeable = true;
            }
            try {
                return BitmapFactory.decodeFile(dst.getPath(), opts);
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static int computeSampleSize(BitmapFactory.Options options,
                                        int minSideLength, int maxNumOfPixels) {
        int initialSize = computeInitialSampleSize(options, minSideLength,
                maxNumOfPixels);

        int roundedSize;
        if (initialSize <= 8) {
            roundedSize = 1;
            while (roundedSize < initialSize) {
                roundedSize <<= 1;
            }
        } else {
            roundedSize = (initialSize + 7) / 8 * 8;
        }

        return roundedSize;
    }

    private static int computeInitialSampleSize(BitmapFactory.Options options,
                                                int minSideLength, int maxNumOfPixels) {
        double w = options.outWidth;
        double h = options.outHeight;

        int lowerBound = (maxNumOfPixels == -1) ? 1 : (int) Math.ceil(Math
                .sqrt(w * h / maxNumOfPixels));
        int upperBound = (minSideLength == -1) ? 128 : (int) Math.min(
                Math.floor(w / minSideLength), Math.floor(h / minSideLength));

        if (upperBound < lowerBound) {
            // return the larger one when there is no overlapping zone.
            return lowerBound;
        }

        if ((maxNumOfPixels == -1) && (minSideLength == -1)) {
            return 1;
        } else if (minSideLength == -1) {
            return lowerBound;
        } else {
            return upperBound;
        }
    }

    /**
     * 从缓存获取图片
     *
     * @return
     */
    public static Bitmap getPictureFromCache(Context context, String imgPath) {
        Bitmap bitmap = null;
        try {
            // 这里写死，在实际开发项目中要灵活使用
            // File file = new File(context.getCacheDir()
            // + "/webviewCache/10d8d5cd");
            File file = new File(context.getCacheDir() + imgPath);
            FileInputStream inStream = new FileInputStream(file);
            bitmap = BitmapFactory.decodeStream(inStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }
}