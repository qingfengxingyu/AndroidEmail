package com.team.cores.basecommon.recyclerview.base;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.team.cores.R;
import com.team.cores.utils.ImageLoadUtils;
import com.team.cores.utils.tools.StringUtils;

import java.io.File;

/**
 * Created by xmf on 16/6/22.
 * 适配器ViewHolder基类
 */
public class ViewHolder extends RecyclerView.ViewHolder {

    // 用来存放子View减少findViewById的次数
    private SparseArray<View> mViews;

    public ViewHolder(View itemView) {
        super(itemView);
        mViews = new SparseArray<>();
    }

    /**
     * 设置TextView文本
     */
    public ViewHolder setText(int viewId, CharSequence text) {
        TextView tv = getView(viewId);
        if (null != tv) {
            tv.setText(text);
        }
        return this;
    }

    /**
     * @param viewId        RecyclerView的id
     * @param layoutManager RecyclerView的LayoutManager
     * @param adapter       RecyclerView的adapter
     * @Date: 2018/6/7 14:00
     * @Description: 设置RecyclerView
     */
    public ViewHolder setRecyclerView(int viewId, RecyclerView.LayoutManager layoutManager, RecyclerView.Adapter adapter) {
        RecyclerView recyclerView = getView(viewId);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        return this;
    }

    /**
     * 通过id获取view
     */
    public <T extends View> T getView(int viewId) {
        // 先从缓存中找
        View view = mViews.get(viewId);
        if (view == null) {
            // 直接从ItemView中找
            view = itemView.findViewById(viewId);
            mViews.put(viewId, view);
        }
        return (T) view;
    }

    /**
     * 设置View的Visibility
     */
    public ViewHolder setViewVisibility(int viewId, int visibility) {
        getView(viewId).setVisibility(visibility);
        return this;
    }

    /**
     * 设置ImageView的资源
     */
    public ViewHolder setImageResource(int viewId, int resourceId) {
        ImageView imageView = getView(viewId);
        imageView.setImageResource(resourceId);
        return this;
    }

    /**
     * 设置条目点击事件
     */
    public void setOnIntemClickListener(View.OnClickListener listener) {
        itemView.setOnClickListener(listener);
    }

    /**
     * 设置条目长按事件
     */
    public void setOnIntemLongClickListener(View.OnLongClickListener listener) {
        itemView.setOnLongClickListener(listener);
    }

    /**
     * 设置图片通过路径,这里稍微处理得复杂一些，因为考虑加载图片的第三方可能不太一样
     * 也可以直接写死
     */
    public ViewHolder setImageByUrl(int viewId, HolderImageLoader imageLoader) {
        ImageView imageView = getView(viewId);
        if (imageLoader == null) {
            throw new NullPointerException("imageLoader is null!");
        }
        if (imageLoader.mValues instanceof String) {
            imageLoader.displayImage(imageView.getContext(), imageView, imageLoader.mImagePath);
        } else if (imageLoader.mValues instanceof Integer) {
            imageLoader.displayImage(imageView.getContext(), imageView, imageLoader.mResourceId);
        } else if (imageLoader.mValues instanceof File) {
            imageLoader.displayImage(imageView.getContext(), imageView, imageLoader.mFile);
        } else if (imageLoader.mValues instanceof byte[]) {
            imageLoader.displayImage(imageView.getContext(), imageView, imageLoader.mBytes);
        }

        return this;
    }

    /**
     * 图片加载统一处理
     */
    public static class HolderImageLoader {
        public String mImagePath;
        public int mResourceId;
        public File mFile;
        public byte[] mBytes;
        public Object mValues;

        public HolderImageLoader(Object values) {
            mValues = values;
            if (values instanceof String) {
                mImagePath = (String) values;
            } else if (values instanceof Integer) {
                mResourceId = (int) values;
            } else if (values instanceof File) {
                mFile = (File) values;
            } else if (values instanceof byte[]) {
                mBytes = (byte[]) values;
            }
        }


        /***
         * 加载网络图片
         *
         * @param mContext
         * @param imageView 显示View
         * @param imagePath 图片地址
         */
        public void displayImage(Context mContext, ImageView imageView, String imagePath) {
            if (StringUtils.isEmpty(imagePath)) {
                return;
            }
            ImageLoadUtils.loadImage_defualtImg(mContext, imagePath, imageView, 1);
        }

        /***
         * 加载资源图片
         *
         * @param mContext
         * @param imageView   显示View
         * @param mResourceId 图片资源ID
         */
        public void displayImage(Context mContext, ImageView imageView, int mResourceId) {
            Glide.with(mContext).load(mResourceId).placeholder(R.drawable.default_app_icon).error(R.drawable.default_app_icon).centerCrop().into(imageView);
        }

        /***
         * 加载本地文件图片
         *
         * @param mContext
         * @param imageView 显示View
         * @param mFile     图片文件
         */
        public void displayImage(Context mContext, ImageView imageView, File mFile) {
            if (null == mFile || !mFile.exists()) {
                return;
            }
            Glide.with(mContext).load(mFile).placeholder(R.drawable.default_app_icon).error(R.drawable.default_app_icon).centerCrop().into(imageView);
        }

        /***
         * 加载数组图片
         *
         * @param mContext
         * @param imageView 显示View
         * @param mBytes    byte数据
         */
        public void displayImage(Context mContext, ImageView imageView, byte[] mBytes) {
            if (null == mBytes) {
                return;
            }
            Glide.with(mContext).load(mBytes).placeholder(R.drawable.default_app_icon).error(R.drawable.default_app_icon).centerCrop().into(imageView);
        }
    }
}
