package com.xing.email.utils.emailtool

import com.xing.email.bean.MailInfo
import java.io.File
import java.util.*
import javax.mail.*

/**
 * author:xmf
 * date:2018/11/6 0006
 * description:发送邮件工具类
 */
object SendMailUtil {
    //qq
    private val HOST = "smtp.qq.com"
    private val PORT = "587"
    //    private val FROM_ADD = "1282061691@qq.com" //发送方邮箱
//    private val FROM_PSW = "retwitgvfcalfffc"//发送方邮箱授权码


    fun send(from_add: String, from_psw: String, file: File, toAdd: String, subject: String, content: String) {
        val mailInfo = creatMail(from_add, from_psw, toAdd, subject, content)
        val sms = MailSender()
        Thread(Runnable { sms.sendFileMail(mailInfo, file) }).start()
    }

    fun send(from_add: String, from_psw: String, toAdd: String, subject: String, content: String) {
        val mailInfo = creatMail(from_add, from_psw, toAdd, subject, content)
        val sms = MailSender()
        Thread(Runnable { sms.sendTextMail(mailInfo) }).start()
    }

    private fun creatMail(from_add: String, from_psw: String, toAdd: String, subject: String, content: String): MailInfo {
        val mailInfo = MailInfo()
        mailInfo.mailServerHost = HOST
        mailInfo.mailServerPort = PORT
        mailInfo.isValidate = true
        mailInfo.userName = from_add // 你的邮箱地址
        mailInfo.password = from_psw// 你的邮箱密码
        mailInfo.fromAddress = from_add // 发送的邮箱
        mailInfo.toAddress = toAdd // 发到哪个邮件去
        mailInfo.subject = subject// 邮件主题
        mailInfo.content = content // 邮件文本
        return mailInfo
    }

}
