package com.team.cores.dbmode.download;

import com.team.cores.configs.ProjectConfigs;
import com.team.cores.dbmode.bean.DownloadDBEntity;
import com.team.cores.dbmode.tools.GreenDaoHelper;
import com.team.cores.httputils.HttpUtils;
import com.team.cores.utils.tools.LogTeamUtils;

import org.apache.http.util.TextUtils;
import org.greenrobot.eventbus.EventBus;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/***
 * 下载文件线程
 *
 * @author xmf
 */
public class DownloadTask implements Runnable {
    private final String TAG = "DownloadTask_xmf";
    private DownloadDBEntity dbEntity;
    private OkHttpClient client;
    public int statuNum;
    private float oldPercent = -1;
    private long toolSize;
    private long completedSize; // Download section has been completed
    // private float percent; // Percent Complete

    private boolean isGoDown = true;// 是否支持断点下载 true支持 false不支持 默认true支持

    private RandomAccessFile file;
    private int UPDATE_SIZE = 50 * 1024; // The database is updated once every
    // 50k
    private int downloadStatus = DownloadStatus.DOWNLOAD_STATUS_INIT;
    //    private String id;//唯一标识---应用id
//    private String versionName;//应用版本号
//    private int appType;//应用类型
//    private String packName;//应用包名
//    private String url;//应用下载地址
//    private String saveDirPath;//sd卡存放目录
//    private String fileName; // 文件名称
    private List<DownloadTaskListener> listeners = new ArrayList<DownloadTaskListener>();
    private boolean flag = false;
    private InputStream inputStream = null;
    private BufferedInputStream bis = null;
    private DownloadDBEntity mDownBean;//初始下载参数实体
    private int serverCode;

    /***
     * 普通下载
     *
     * @param id          唯一标识---应用id
     * @param url         应用下载地址
     * @param saveDirPath sd卡存放目录
     * @param fileName    文件名称
     */
    public DownloadTask(String id, String url, String saveDirPath, String fileName) {
        mDownBean = new DownloadDBEntity();
        mDownBean.setPackName(id);
        mDownBean.setUrl(url);
        mDownBean.setSaveDirPath(saveDirPath);
        mDownBean.setFileName(fileName);
    }

    public DownloadTask(DownloadDBEntity mDownBean) {
        this.mDownBean = mDownBean;
    }

    @Override
    public void run() {
        if (downloadStatus == DownloadStatus.DOWNLOAD_STATUS_PAUSE) {
            return;
        }
        statuNum = DownloadStatus.DOWNLOAD_STATUS_DOWNLOADING;
        onDownloading();
        try {
            dbEntity = GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().load(mDownBean.getPackName());
            File outputFile = new File(mDownBean.getSaveDirPath() + mDownBean.getFileName());
            if (!outputFile.exists()) {
                outputFile.createNewFile();
            }
            file = new RandomAccessFile(outputFile, "rwd");
            if (dbEntity != null) {
                completedSize = dbEntity.getCompletedSize();
                toolSize = dbEntity.getToolSize();
            }
            if (file.length() < completedSize) {
                completedSize = file.length();
            }
            long fileLength = file.length();
            if (0 != toolSize && fileLength != 0 && toolSize <= fileLength) {
                downloadStatus = DownloadStatus.DOWNLOAD_STATUS_COMPLETED;
                toolSize = completedSize = fileLength;
                dbEntity = mDownBean;
                dbEntity.setCompletedSize(toolSize);
                dbEntity.setToolSize(toolSize);
                dbEntity.setDownloadStatus(downloadStatus);
//                dbEntity = new DownloadDBEntity(id, toolSize, toolSize, url,
//                        saveDirPath, fileName, downloadStatus, versionName, appType, packName);
                GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().insertOrReplace(dbEntity);
                onCompleted();
                return;
            }
            downloadStatus = DownloadStatus.DOWNLOAD_STATUS_DOWNLOADING;
            if (dbEntity == null) {
                dbEntity = mDownBean;
                dbEntity.setCompletedSize(0L);
                dbEntity.setToolSize(toolSize);
                dbEntity.setDownloadStatus(downloadStatus);
//                dbEntity = new DownloadDBEntity(id, toolSize, 0L, url,
//                        saveDirPath, fileName, downloadStatus, versionName, appType, packName);
                GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().insertOrReplace(dbEntity);
            } else {
                dbEntity.setToolSize(toolSize);
                dbEntity.setCompletedSize(completedSize);
                dbEntity.setDownloadStatus(downloadStatus);
                GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().update(dbEntity);
            }
            mDownBean = dbEntity;
            downIng();
        } catch (Exception e) {
            downloadStatus = DownloadStatus.DOWNLOAD_STATUS_ERROR;
            e.printStackTrace();
            LogTeamUtils.e(TAG, "downloadTask  run is erro is:" + e.toString());
        } finally {
            flag = true;
            dbEntity.setCompletedSize(completedSize);
            if (0 != toolSize && toolSize == completedSize)
                downloadStatus = DownloadStatus.DOWNLOAD_STATUS_COMPLETED;
            if (0 == toolSize || toolSize < 300) {
                downloadStatus = DownloadStatus.DOWNLOAD_STATUS_ERROR;
            }
            dbEntity.setDownloadStatus(downloadStatus);
            GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().update(dbEntity);
            if (bis != null)
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            if (inputStream != null)
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            if (file != null)
                try {
                    file.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            switch (downloadStatus) {
                case DownloadStatus.DOWNLOAD_STATUS_ERROR:
                    onError(DownloadTaskListener.DOWNLOAD_ERROR_FILE_NOT_FOUND);
                    break;
                case DownloadStatus.DOWNLOAD_STATUS_COMPLETED:
                    onCompleted();
                    break;
                case DownloadStatus.DOWNLOAD_STATUS_PAUSE:
                    onPause();
                    break;
                case DownloadStatus.DOWNLOAD_STATUS_CANCEL:
                    GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().delete(dbEntity);
                    File temp = new File(mDownBean.getSaveDirPath() + mDownBean.getFileName());
                    if (temp.exists())
                        temp.delete();
                    onCancel();
                    break;
            }
            if (HttpUtils.Token_GuoQi == serverCode) {
                if (null != ProjectConfigs.getInstance().getApp()) {
//                    Intent intent = new Intent(MipConstant.ACTION_SESSION_TIMEOUT);
//                    intent.putExtra("mine", true);
//                    ProjectConfigs.getInstance().getApp().sendBroadcast(intent, MipConstant.PEMISSION);
                    return;
                }
            }
        }

    }

    private void downIng() {
        try {
            flag = true;
            Request request = null;
            request = new Request.Builder()
                    .url(mDownBean.getUrl())
                    .header("Range", "bytes=" + completedSize + "-")
                    .build();
            LogTeamUtils.e(TAG, "文件下载地址is:" + mDownBean.getUrl());
            Response response = null;
            response = client.newCall(request).execute();
            serverCode = response.code();
            if (response != null) {
                LogTeamUtils.e(TAG, "response.isSuccessful():" + response.isSuccessful());
            } else {
                LogTeamUtils.e(TAG, "response is NUll:");
            }
            LogTeamUtils.e(TAG, "获取响应码 code is:" + serverCode);
            if (response != null && response.isSuccessful()) {

                ResponseBody responseBody = response.body();
                if (responseBody != null) {
                    downloadStatus = DownloadStatus.DOWNLOAD_STATUS_DOWNLOADING;
                    if (toolSize <= 0) {
                        toolSize = responseBody.contentLength();
                        dbEntity.setToolSize(toolSize);
                        mDownBean = dbEntity;
                        GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().update(dbEntity);
                    }
                    LogTeamUtils.e(TAG, "toolSize is :" + toolSize);
                    if (isGoDown) {
                        if (TextUtils.isEmpty(response.header("Content-Range"))) {
                            LogTeamUtils.e(TAG, "Content-Range is null.服务器不支持断点下载");
                            // 返回的没有Content-Range 不支持断点下载 需要重新下载
                            File alreadyDownloadedFile = new File(mDownBean.getSaveDirPath() + mDownBean.getFileName());
                            if (alreadyDownloadedFile.exists()) {
                                alreadyDownloadedFile.delete();
                            }
                            file = new RandomAccessFile(mDownBean.getSaveDirPath() + mDownBean.getFileName(),
                                    "rwd");
                            completedSize = 0;
                        }
                    } else {
                        // 不支持断点下载 需要重新下载
                        File alreadyDownloadedFile = new File(mDownBean.getSaveDirPath() + mDownBean.getFileName());
                        if (alreadyDownloadedFile.exists()) {
                            alreadyDownloadedFile.delete();
                        }
                        file = new RandomAccessFile(mDownBean.getSaveDirPath() + mDownBean.getFileName(),
                                "rwd");
                        completedSize = 0;
                    }
                    file.seek(completedSize);
                    inputStream = responseBody.byteStream();
                    bis = new BufferedInputStream(inputStream);
                    byte[] buffer = new byte[2 * 1024];
                    int length = 0;
                    int buffOffset = 0;
                    if (dbEntity == null) {
                        dbEntity = mDownBean;
                        dbEntity.setCompletedSize(0L);
                        dbEntity.setToolSize(toolSize);
                        dbEntity.setDownloadStatus(downloadStatus);
                        GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().insertOrReplace(dbEntity);
                    }
                    dbEntity.setDownloadStatus(downloadStatus);
                    mDownBean = dbEntity;
                    if (toolSize != 0 && toolSize > 300) {
                        new Thread(new Runnable() {// 1秒钟并且和上次进度不一样才更新一次下载进度和下载速度

                            @Override
                            public void run() {
                                while (downloadStatus == DownloadStatus.DOWNLOAD_STATUS_DOWNLOADING) {
                                    try {
                                        onDownloading();
                                        Thread.sleep(500);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                            }
                        }).start();
                        while ((length = bis.read(buffer)) > 0
                                && downloadStatus == DownloadStatus.DOWNLOAD_STATUS_DOWNLOADING) {
                            file.write(buffer, 0, length);
                            completedSize += length;
                            buffOffset += length;
                            if (buffOffset >= UPDATE_SIZE) {
                                buffOffset = 0;
                                dbEntity.setCompletedSize(completedSize);
                                mDownBean = dbEntity;
                                GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().update(dbEntity);
                                flag = false;
                            }
                        }
                    } else {
                        LogTeamUtils.e(TAG, "当toolSize < 300 :" + new String(readInputStream(bis), "UTF-8") + ".");
                    }
//                    downloadStatus = DownloadStatus.DOWNLOAD_STATUS_COMPLETED;
                    if (downloadStatus != DownloadStatus.DOWNLOAD_STATUS_CANCEL) {
                        dbEntity.setCompletedSize(completedSize);
                        mDownBean = dbEntity;
                        GreenDaoHelper.getInstance().getDaoSession().getDownloadDBEntityDao().update(dbEntity);
                        flag = true;
                    }
                } else {
                    LogTeamUtils.e(TAG, "responseBody is null");
                }
            } else {
                downloadStatus = DownloadStatus.DOWNLOAD_STATUS_ERROR;
            }
        } catch (Exception e) {
            LogTeamUtils.e(TAG, e.toString());
            e.printStackTrace();
            String msg = e.toString();
            if (msg.indexOf("unexpected end of stream") > 0) {
                if (downloadStatus == DownloadStatus.DOWNLOAD_STATUS_DOWNLOADING) {
                    downIng();
                }
            } else {
                downloadStatus = DownloadStatus.DOWNLOAD_STATUS_ERROR;
            }
            LogTeamUtils.e(TAG, "downloadTask downImg is erro is:" + e.toString());
        }
    }


    public float getPercent() {
        return toolSize == 0 ? 0 : completedSize * 100 / toolSize;
    }

    public long getToolSize() {
        return toolSize;
    }

    public void setTotalSize(long toolSize) {
        this.toolSize = toolSize;
    }

    public long getCompletedSize() {
        return completedSize;
    }

    public void setCompletedSize(long completedSize) {
        this.completedSize = completedSize;
    }


    public int getDownloadStatus() {
        return downloadStatus;
    }

    public void setDownloadStatus(int downloadStatus) {
        this.downloadStatus = downloadStatus;
    }

    public DownloadDBEntity getmDownBean() {
        return mDownBean;
    }

    public void setmDownBean(DownloadDBEntity mDownBean) {
        this.mDownBean = mDownBean;
    }

    public void setDbEntity(DownloadDBEntity dbEntity) {
        this.dbEntity = dbEntity;
    }

    /***
     * 是否支持断点下载 true支持 false不支持 默认true支持
     *
     * @param isGoDown
     */
    public void setisGoDown(boolean isGoDown) {
        this.isGoDown = isGoDown;
    }

    public void setHttpClient(OkHttpClient client) {
        if (null == this.client) {
            this.client = client;
        }
    }


    public void cancel() {
        statuNum = DownloadStatus.DOWNLOAD_STATUS_CANCEL;
        setDownloadStatus(DownloadStatus.DOWNLOAD_STATUS_CANCEL);
        EventBus.getDefault().post(this);
        File temp = new File(mDownBean.getSaveDirPath() + mDownBean.getFileName());
        if (temp.exists())
            temp.delete();
    }

    public void pause() {
        statuNum = DownloadStatus.DOWNLOAD_STATUS_PAUSE;
        setDownloadStatus(DownloadStatus.DOWNLOAD_STATUS_PAUSE);
        EventBus.getDefault().post(this);
    }

    private void onDownloading() {
        if (oldPercent != getPercent() || flag) {
            oldPercent = getPercent();
            EventBus.getDefault().post(this);
            for (DownloadTaskListener listener : listeners) {
                if (null != listener) {
                    listener.onDownloading(this);
                }
            }
        }
    }

    private void onCompleted() {
        statuNum = DownloadStatus.DOWNLOAD_STATUS_COMPLETED;
        EventBus.getDefault().post(this);
        for (DownloadTaskListener listener : listeners) {
            if (null != listener) {
                listener.onCompleted(this);
            }
        }
    }

    private void onPause() {
        statuNum = DownloadStatus.DOWNLOAD_STATUS_PAUSE;
        EventBus.getDefault().post(this);
        for (DownloadTaskListener listener : listeners) {
            if (null != listener) {
                listener.onPause(this);
            }
        }
    }

    private void onCancel() {
        statuNum = DownloadStatus.DOWNLOAD_STATUS_CANCEL;
        EventBus.getDefault().post(this);
        for (DownloadTaskListener listener : listeners) {
            if (null != listener) {
                listener.onCancel(this);
            }
        }
    }

    private void onError(int errorCode) {
        statuNum = DownloadStatus.DOWNLOAD_STATUS_ERROR;
        EventBus.getDefault().post(this);
        for (DownloadTaskListener listener : listeners) {
            if (null != listener) {
                listener.onError(this, errorCode);
            }
        }
    }

    public void addDownloadListener(DownloadTaskListener listener) {
        listeners.add(listener);
    }

    /**
     * if listener is null,clear all listener
     *
     * @param listener
     */
    public void removeDownloadListener(DownloadTaskListener listener) {
        if (listener != null) {
            listeners.remove(listener);
        }
    }

    public void removeAllDownloadListener() {
        this.listeners.clear();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        DownloadTask that = (DownloadTask) o;

        if (mDownBean.getPackName() != null ? !mDownBean.getPackName().equals(that.mDownBean.getPackName()) : that.mDownBean.getPackName() != null)
            return false;
        return mDownBean.getUrl() != null ? mDownBean.getUrl().equals(that.mDownBean.getUrl()) : that.mDownBean.getUrl() == null;

    }


    @Override
    public int hashCode() {
        return 0;
    }

    public static DownloadTask parse(DownloadDBEntity entity) {
        DownloadTask task = new DownloadTask(entity);
        task.setDownloadStatus(entity.getDownloadStatus());
        task.setCompletedSize(entity.getCompletedSize());
        task.setTotalSize(entity.getToolSize());
        task.setDbEntity(entity);
        return task;
    }


    public static byte[] readInputStream(BufferedInputStream inStream)
            throws Exception {
        // 构造一个ByteArrayOutputStream
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        // 设置一个缓冲区
        byte[] buffer = new byte[1024];
        int len = 0;
        // 判断输入流长度是否等于-1，即非空
        while ((len = inStream.read(buffer)) != -1) {
            // 把缓冲区的内容写入到输出流中，从0开始读取，长度为len
            outStream.write(buffer, 0, len);
        }
        // 关闭输入流
        inStream.close();
        return outStream.toByteArray();
    }

}
