package com.xing.email.ui

import android.content.Intent
import android.os.Bundle
import com.xing.email.R
import com.xing.email.app.BaseUI

/**
 *author:xmf
 *date:2018/11/5 0005
 *description:启动页面，检查用户有没有设置过默认账户，没有就跳转到设置页面，有就自动登录页面
 */
class StartUI : BaseUI() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ui_start)
//        startActivity(Intent(this@StartUI, NewBuildAccountUI::class.java))
        startActivity(Intent(this@StartUI, LoginUI::class.java))
        finish()
    }

    override fun initData() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun initEven() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
