package com.team.cores.utils;

import android.annotation.SuppressLint;
import android.util.Log;

import com.team.cores.utils.tools.LogTeamUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressLint("SimpleDateFormat")
public class LogTools {
    /***
     * 默认关闭，不答应日志
     */
    public static boolean isPringLog = true;
    public static SimpleDateFormat sdk = new SimpleDateFormat("yyyyMMddHH");

    public static void printLog(String Tag, String msg) {
        if (isPringLog) {
            if (null == msg || "".equals(msg)) {
                return;
            }
            LogTeamUtils.w(Tag, msg);
        }
    }

    public static void printLog_E(String Tag, String msg) {
        if (isPringLog) {
            if (null == msg || "".equals(msg)) {
                return;
            }
            LogTeamUtils.e(Tag, msg);
        }
    }

    public static void printLog(String msg) {
        if (isPringLog) {
            if (null == msg || "".equals(msg)) {
                return;
            }
            LogTeamUtils.w("tools_teams_xmf", msg);
        }
    }

    /**
     * 写入一般日志错误
     */
    public static void writeLog(String msg) {
        if (isPringLog) {
            if (null == msg || "".equals(msg)) {
                return;
            }
            printLog("writeLog_teams_xmf", "写入日志到文件下:" + msg);
            try {
                // 获取当前事件 生成文件名
                String fileName = sdk.format(new Date());
//                    String allName = ConfigTools.BASE_DIR_LOG + "/" + fileName
//                            + ".txt";
                String allName = FileSaveTool.getInstance().getLogSd() + File.separator + fileName
                        + ".txt";
                File f = new File(allName);
                if (!f.exists()) {
                    f.createNewFile();
                }
                OutputStreamWriter osw = new OutputStreamWriter(
                        new FileOutputStream(f, true));
                BufferedWriter bw = new BufferedWriter(osw);
                bw.write(msg);
                bw.newLine();
                bw.flush();
                bw.close();
                printLog("writeLog_teams_xmf", allName + ":保存" + msg + "成功！");
            } catch (Exception ex) {
                ex.printStackTrace();

            }

        }
    }

    /**
     * 写入重要错误日志
     */
    public static void writeMainLog(String msg) {
        if (null == msg || "".equals(msg)) {
            return;
        }
        printLog_E("writeLog", "写入日志到文件下:" + msg);
        try {
            String fileName = sdk.format(new Date());
//                String allName = ConfigTools.BASE_DIR_MAIN_LOG + "/" + fileName
//                        + ".txt";
            String allName = FileSaveTool.getInstance().getMainLogSd() + File.separator + fileName
                    + ".txt";
            File f = new File(allName);
            if (!f.exists()) {
                f.createNewFile();
            }
            OutputStreamWriter osw = new OutputStreamWriter(
                    new FileOutputStream(f, true));
            BufferedWriter bw = new BufferedWriter(osw);
            bw.write(msg);
            bw.newLine();
            bw.flush();
            bw.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
