package com.team.cores.utils.tools;

import android.content.Context;
import android.os.Environment;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/***
 * author: xmf
 * desc  : String相关工具类
 */
public class StringUtils {
    /**
     * 主表ID.
     *
     * @return the string
     */
    public static String uuid32len() {
        return java.util.UUID.randomUUID().toString().replace("-", "");
    }

    /***
     * 判断List是否为空
     *
     * @param s
     * @return true 为空 false 为非空
     */
    public static boolean isList(List<?> s) {
        if (s == null) {
            return true;
        }
        if (s.size() <= 0) {
            return true;
        }
        return false;
    }

    /***
     * 判断字符串是否为空
     *
     * @param s
     * @return true 为空 false 为非空
     */
    public static boolean isEmpty(String s) {
        if (s == null) {
            return true;
        }
        if (s.equals("")) {
            return true;
        }
        return false;
    }

    public static String checkSd() {
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED_READ_ONLY)) {
            return "对不起，你的sd没有插入或者没有音乐文件。";
        }
        if (status.equals(Environment.MEDIA_SHARED)) {
            return "对不起，当你连接USB的时候程序不能运行。";
        }
        if (!status.equals(Environment.MEDIA_MOUNTED)) {
            return "对不起，如果你没有sd卡，程序将不能运行。";
        }
        return "";
    }

    /***
     * 获得List集合
     *
     * @param js
     * @return
     */
    public static List<?> getDataList(JSONObject js, Type listType) {
        try {
            JSONArray ja = js.getJSONArray("dataList");
            Gson gs = new Gson();
            return gs.fromJson(ja.toString(), listType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }


    /**
     * 字符串为NULL时赋值""
     *
     * @param ziduan
     * @return
     */
    public static String toMode(String ziduan) {
        return null != ziduan ? ziduan : "";

    }

    /**
     * 字符串为空时时赋值"0"
     *
     * @param msg
     * @return
     */
    public static String toMode_Int(String msg) {
        if (StringUtils.isEmpty(msg)) {
            return "0";
        }
        return msg;

    }

    /**
     * 判断是否是电话号码
     *
     * @param mobiles
     * @return
     */
    public static boolean isMobileNO(String mobiles) {

//		Pattern p = Pattern
//				.compile("^((13[0-9])|(15[0-9])|(18[0-9]))\\d{8}$");

        Pattern p = Pattern
                .compile("^1\\d{10}$");
        Matcher m = p.matcher(mobiles);

        System.out.println(m.matches() + "---");

        return m.matches();
    }

    /**
     * 判断是否是数字
     *
     * @param str
     * @return
     */
    public static boolean isNumeric(String str) {
        Pattern pattern = Pattern.compile("[0-9]*");
        Matcher isNum = pattern.matcher(str);
        if (!isNum.matches()) {
            return false;
        }
        return true;
    }

    /**
     * 判断是否是价格
     *
     * @param str
     * @return
     */
    public static boolean isPrice(String str) {
        Pattern pattern = Pattern.compile("[0-9]*.[0-9]*");
        Matcher isNum = pattern.matcher(str);
        if (!isNum.matches()) {
            return false;
        }
        return true;
    }

    /**
     * 将jsonarray数组转成 String 如：#ffffff
     */
    public static String toHexEncoding(JSONArray jsona) throws JSONException {
        String R, G, B;
        StringBuffer sb = new StringBuffer();
        R = Integer.toHexString(jsona.getInt(0));
        G = Integer.toHexString(jsona.getInt(1));
        B = Integer.toHexString(jsona.getInt(2));
        R = R.length() == 1 ? "0" + R : R;
        G = G.length() == 1 ? "0" + G : G;
        B = B.length() == 1 ? "0" + B : B;
        sb.append("#");
        sb.append(R.toUpperCase());
        sb.append(G.toUpperCase());
        sb.append(B.toUpperCase());
        return sb.toString();
    }


    /**
     * 隐藏软键盘
     *
     * @param context
     */
    public static void hideSoftInput(Context context, EditText editText) {
        InputMethodManager inputMethodManager = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager
                .hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    /**
     * 判断字符是否是中文
     */
    public static boolean isChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {
            return true;
        }
        return false;
    }

    /***
     * js交互传参转义字符
     *
     * @param values
     * @return
     */
    public static String jsToString(String values) {
        if (StringUtils.isEmpty(values)) {
            return "";
        }
        values = values.replace("\"", "\\\"").replace("'", "\\\'");
        return values;
    }

    private StringUtils() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    /**
     * 判断字符串是否为null或长度为0
     *
     * @param s 待校验字符串
     * @return {@code true}: 空<br> {@code false}: 不为空
     */
    public static boolean isEmpty(CharSequence s) {
        return s == null || s.length() == 0;
    }

    /**
     * 判断字符串是否为null或全为空格
     *
     * @param s 待校验字符串
     * @return {@code true}: null或全空格<br> {@code false}: 不为null且不全空格
     */
    public static boolean isSpace(String s) {
        return (s == null || s.trim().length() == 0);
    }

    /**
     * 判断两字符串是否相等
     *
     * @param a 待校验字符串a
     * @param b 待校验字符串b
     * @return {@code true}: 相等<br>{@code false}: 不相等
     */
    public static boolean equals(CharSequence a, CharSequence b) {
        if (a == b) return true;
        int length;
        if (a != null && b != null && (length = a.length()) == b.length()) {
            if (a instanceof String && b instanceof String) {
                return a.equals(b);
            } else {
                for (int i = 0; i < length; i++) {
                    if (a.charAt(i) != b.charAt(i)) return false;
                }
                return true;
            }
        }
        return false;
    }

    /**
     * 判断两字符串忽略大小写是否相等
     *
     * @param a 待校验字符串a
     * @param b 待校验字符串b
     * @return {@code true}: 相等<br>{@code false}: 不相等
     */
    public static boolean equalsIgnoreCase(String a, String b) {
        return (a == b) || (b != null) && (a.length() == b.length()) && a.regionMatches(true, 0, b, 0, b.length());
    }

    /**
     * null转为长度为0的字符串
     *
     * @param s 待转字符串
     * @return s为null转为长度为0字符串，否则不改变
     */
    public static String null2Length0(String s) {
        return s == null ? "" : s;
    }

    /**
     * 返回字符串长度
     *
     * @param s 字符串
     * @return null返回0，其他返回自身长度
     */
    public static int length(CharSequence s) {
        return s == null ? 0 : s.length();
    }

    /**
     * 首字母大写
     *
     * @param s 待转字符串
     * @return 首字母大写字符串
     */
    public static String upperFirstLetter(String s) {
        if (isEmpty(s) || !Character.isLowerCase(s.charAt(0))) return s;
        return String.valueOf((char) (s.charAt(0) - 32)) + s.substring(1);
    }

    /**
     * 首字母小写
     *
     * @param s 待转字符串
     * @return 首字母小写字符串
     */
    public static String lowerFirstLetter(String s) {
        if (isEmpty(s) || !Character.isUpperCase(s.charAt(0))) {
            return s;
        }
        return String.valueOf((char) (s.charAt(0) + 32)) + s.substring(1);
    }

    /**
     * 反转字符串
     *
     * @param s 待反转字符串
     * @return 反转字符串
     */
    public static String reverse(String s) {
        int len = length(s);
        if (len <= 1) return s;
        int mid = len >> 1;
        char[] chars = s.toCharArray();
        char c;
        for (int i = 0; i < mid; ++i) {
            c = chars[i];
            chars[i] = chars[len - i - 1];
            chars[len - i - 1] = c;
        }
        return new String(chars);
    }

    /**
     * 转化为半角字符
     *
     * @param s 待转字符串
     * @return 半角字符串
     */
    public static String toDBC(String s) {
        if (isEmpty(s)) {
            return s;
        }
        char[] chars = s.toCharArray();
        for (int i = 0, len = chars.length; i < len; i++) {
            if (chars[i] == 12288) {
                chars[i] = ' ';
            } else if (65281 <= chars[i] && chars[i] <= 65374) {
                chars[i] = (char) (chars[i] - 65248);
            } else {
                chars[i] = chars[i];
            }
        }
        return new String(chars);
    }

    /**
     * 转化为全角字符
     *
     * @param s 待转字符串
     * @return 全角字符串
     */
    public static String toSBC(String s) {
        if (isEmpty(s)) {
            return s;
        }
        char[] chars = s.toCharArray();
        for (int i = 0, len = chars.length; i < len; i++) {
            if (chars[i] == ' ') {
                chars[i] = (char) 12288;
            } else if (33 <= chars[i] && chars[i] <= 126) {
                chars[i] = (char) (chars[i] + 65248);
            } else {
                chars[i] = chars[i];
            }
        }
        return new String(chars);
    }

}