package com.team.cores.views;

import android.content.Context;
import android.text.method.ScrollingMovementMethod;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.team.cores.R;

/**
 * Description:错误提示公共页面
 * Data: 2017/7/26 0026 上午 10:54
 *
 * @author: xmf
 */
public class ErroView extends LinearLayout {

    private TextView erro_msg;
    private TextView myBtn;
    private ImageView imageView;
    /***
     * 1加载失败 2、3 没有网络 4无数据
     */
    public int typeNum;

    public ErroView(Context context) {
        super(context);
        initView(context);
    }

    public ErroView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView(context);
    }

    public ErroView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    private void initView(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.errorview_layout, null);
        addView(view, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        erro_msg = (TextView) findViewById(R.id.erro_msg);
        myBtn = (TextView) findViewById(R.id.myBtn);
        imageView = (ImageView) findViewById(R.id.imageView);
        erro_msg.setMovementMethod(ScrollingMovementMethod.getInstance());
    }

    /***
     * 显示中间的图片和按钮显示
     *
     * @param typeNum 1加载失败 2、3 没有网络 4无数据
     */
    public void showUI(int typeNum) {
        this.typeNum = typeNum;
        switch (typeNum) {
            case 1:
                imageView.setImageResource(R.drawable.erro1);
                myBtn.setVisibility(View.VISIBLE);
                myBtn.setText("刷新重试");
                break;
            case 2:
            case 3:
                imageView.setImageResource(R.drawable.no_net1);
                myBtn.setVisibility(View.VISIBLE);
                myBtn.setText("重新加载");
                break;
            case 4:
                imageView.setImageResource(R.drawable.no_data1);
                myBtn.setVisibility(View.GONE);
                break;
        }
    }

    /***
     * 显示错误提示信息
     *
     * @param msg
     */
    public void showErro_msg(String msg) {
        erro_msg.setText(msg);
    }
}
