package com.xing.email.app

import android.app.Activity
import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.team.cores.configs.ProjectConfigs
import com.team.cores.utils.InitAllTool
import com.xing.email.bean.MailInfo
import java.util.*
import javax.mail.Session

class MyApp : Application() {
    private val activityList = LinkedList<Activity>()

    companion object {
        var session: Session? = null
        var mailInfo = MailInfo()//当前登录邮箱配置信息
        private var mApp: MyApp? = null
        fun getInstance(): MyApp? {
            if (mApp == null) {
                synchronized(MyApp::class.java) {
                    if (mApp == null) {
                        mApp = MyApp()
                    }
                }
            }
            return mApp
        }
    }

    override fun onCreate() {
        super.onCreate()
        MyCrashHandler.getInstance().init()//初始化崩溃日志捕获
        ProjectConfigs.getInstance().setApp(this)
        InitAllTool.initAll(this)
        var mRequestManager = Glide.with(this)
    }


    /***
     * 添加Activity 到容器中
     *
     * @param activity
     */
    fun addActivity(activity: Activity) {
        activityList.add(activity)
    }

    /***
     * 删除容器中Activity
     *
     * @param activity
     */
    fun removeActivity(activity: Activity) {
        activityList.remove(activity)
    }

    /***
     * 遍历所有Activity 并finish
     */
    fun exit() {
        for (activity in activityList) {
            activity.finish()
        }
    }

    /***
     * 关闭指定的activity
     */
    fun closeOneActivity(msg: String) {
        for (i in activityList.indices) {
            if (activityList[i].javaClass.simpleName == msg) {
                activityList[i].finish()
                activityList.removeAt(i)
            }
        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this);
    }
}