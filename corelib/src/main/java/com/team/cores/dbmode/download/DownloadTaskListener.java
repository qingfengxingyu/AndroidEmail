package com.team.cores.dbmode.download;

public interface DownloadTaskListener {
	void onDownloading(DownloadTask downloadTask);

	void onPause(DownloadTask downloadTask);

	void onCancel(DownloadTask downloadTask);

	void onCompleted(DownloadTask downloadTask);

	void onError(DownloadTask downloadTask, int errorCode);

	int DOWNLOAD_ERROR_FILE_NOT_FOUND = -1;
	int DOWNLOAD_ERROR_IO_ERROR = -2;

}
