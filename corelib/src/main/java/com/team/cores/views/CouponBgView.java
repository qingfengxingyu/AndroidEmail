package com.team.cores.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.LinearLayout;

import com.team.cores.R;


/**
 * 画圆行边和三角边
 * Created by xmf on 2017/1/17.
 */
public class CouponBgView extends LinearLayout {
    private int mVerticalCount, mVerticalInitSize, mHorizontalCount, mHorizontalInitSize;
    private Paint mPaint;
    private int vertical_style, horizontal_style;
    private int mRadius;//圆角
    private int mGapSize;//间距


    public CouponBgView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    public CouponBgView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);//去除画笔锯齿
        mPaint.setStyle(Paint.Style.FILL);//设置风格为实线
        TypedArray ta = context.obtainStyledAttributes(attrs,
                R.styleable.CouponStyle);
        mPaint.setColor(ta.getColor(R.styleable.CouponStyle_m_paint_colore, Color.WHITE));
        vertical_style = ta.getInt(R.styleable.CouponStyle_vertical_style, 0);
        horizontal_style = ta.getInt(R.styleable.CouponStyle_horizontal_style, 0);
        mGapSize = ta.getDimensionPixelOffset(R.styleable.CouponStyle_m_gapSize, 0);
        mRadius = ta.getDimensionPixelOffset(R.styleable.CouponStyle_m_radius, -1);
        if (-1 == mRadius) {
            mRadius = (int) TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP, 6, getResources().getDisplayMetrics());
        }

    }

    /**
     * 计算垂直方向需要画圆或三角形的数量和初始偏移量
     *
     * @param gapSize 每个圆形或三角形之间的间距
     */
    private void calculateVerticalCount(float gapSize) {
        mVerticalCount = (int) ((getHeight() - gapSize) / (2 * mRadius + gapSize));
        mVerticalInitSize = (int) ((getHeight() - (2 * mRadius * mVerticalCount + (mVerticalCount + 1) * gapSize)) / 2);
    }

    /**
     * 计算水平方向上圆或三角形的数量和初始偏移量
     *
     * @param gapSize 每个圆形或三角形之间的间距
     */
    private void calculateHorizontalCount(float gapSize) {
        mHorizontalCount = (int) ((getWidth() - gapSize) / (2 * mRadius + gapSize));
        mHorizontalInitSize = (int) ((getWidth() - (2 * mRadius * mHorizontalCount + (mHorizontalCount + 1) * gapSize)) / 2);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (vertical_style == 1) {//如果垂直方向是半圆形
            drawVerticalCircle(canvas);
        } else if (vertical_style == 2) {//垂直方向是三角形
            drawVerticalTriangle(canvas);
        }
        if (horizontal_style == 1) {//如果水平方向是半圆形
            drawHorizontalCircle(canvas);
        } else if (horizontal_style == 2) {//如果水平方向是三角形
            drawHorizontalTriangle(canvas);
        }
    }

    /**
     * 在水平方向上绘制三角形
     *
     * @param canvas
     */
    private void drawHorizontalTriangle(Canvas canvas) {
        //先计算出水平方向上的数量
        calculateHorizontalCount(0);
        Path path = new Path();
        float x = 0;
        //绘制上面部分
        for (int i = 0; i < mHorizontalCount; i++) {
            path.reset();
            x = mHorizontalInitSize + i * 2 * mRadius;
            path.moveTo(x, 0);
            x += mRadius;
            path.lineTo(x, mRadius);
            x += mRadius;
            path.lineTo(x, 0);
            path.close();
            canvas.drawPath(path, mPaint);
        }
        //绘制下面部分
        x = 0;
        for (int i = 0; i < mHorizontalCount; i++) {
            path.reset();
            x = mHorizontalInitSize + i * 2 * mRadius;
            path.moveTo(x, getHeight());
            x += mRadius;
            path.lineTo(x, getHeight() - mRadius);
            x += mRadius;
            path.lineTo(x, getHeight());
            path.close();
            canvas.drawPath(path, mPaint);
        }
    }

    /**
     * 在水平方向上绘制圆形
     *
     * @param canvas
     */
    private void drawHorizontalCircle(Canvas canvas) {
        //先计算出水平方向上的数量
        calculateHorizontalCount(mGapSize);
        float x = mHorizontalInitSize + mGapSize + mRadius;
        //先绘制上面部分
        for (int i = 0; i < mHorizontalCount; i++) {
            canvas.drawCircle(x, 0, mRadius, mPaint);
            x += 2 * mRadius + mGapSize;
        }
        //再绘制下面部分
        x = mHorizontalInitSize + mGapSize + mRadius;
        for (int i = 0; i < mHorizontalCount; i++) {
            canvas.drawCircle(x, getHeight(), mRadius, mPaint);
            x += 2 * mRadius + mGapSize;
        }
    }

    /**
     * 在垂直方向绘制三角形
     *
     * @param canvas
     */
    private void drawVerticalTriangle(Canvas canvas) {
        //计算一下三角形的数量和初始距离
        calculateVerticalCount(0);
        Path path = new Path();
        float y = 0;
        //先画左边
        for (int i = 0; i < mVerticalCount; i++) {
            path.reset();
            y = mVerticalInitSize + i * 2 * mRadius;
            path.moveTo(0, y);
            y += mRadius;
            path.lineTo(mRadius, y);
            y += mRadius;
            path.lineTo(0, y);
            path.close();
            canvas.drawPath(path, mPaint);
        }
        //再画右边
        y = 0;
        for (int i = 0; i < mVerticalCount; i++) {
            path.reset();
            y = mVerticalInitSize + i * 2 * mRadius;
            path.moveTo(getWidth(), y);
            y += mRadius;
            path.lineTo(getWidth() - mRadius, y);
            y += mRadius;
            path.lineTo(getWidth(), y);
            path.close();
            canvas.drawPath(path, mPaint);
        }
    }

    /**
     * 在垂直方向绘制半圆形
     *
     * @param canvas
     */
    private void drawVerticalCircle(Canvas canvas) {
        //计算一下圆形的数量和初始偏移距离
        calculateVerticalCount(mGapSize);
        //这次使用画弧来绘制出圆形
        RectF rectF = new RectF();
        //先画左边
        for (int i = 0; i < mVerticalCount; i++) {
            rectF.left = -mRadius;
            rectF.top = mVerticalInitSize + mGapSize * (i + 1) + i * 2 * mRadius;
            rectF.right = mRadius;
            rectF.bottom = rectF.top + 2 * mRadius;
            canvas.drawArc(rectF, -90, 180, false, mPaint);
        }
        //再画右边
        for (int i = 0; i < mVerticalCount; i++) {
            rectF.left = getWidth() - mRadius;
            rectF.top = mVerticalInitSize + mGapSize * (i + 1) + i * 2 * mRadius;
            rectF.right = rectF.left + 2 * mRadius;
            rectF.bottom = rectF.top + 2 * mRadius;
            canvas.drawArc(rectF, 90, 180, false, mPaint);
        }
    }
}
