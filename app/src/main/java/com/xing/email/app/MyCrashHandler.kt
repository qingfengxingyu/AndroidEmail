package com.xing.email.app

import android.os.Process
import android.util.Log
import java.io.PrintWriter
import java.io.StringWriter

/**
 *author:xmf
 *date:2018/11/5 0005
 *description:崩溃日志捕获
 */
class MyCrashHandler : Thread.UncaughtExceptionHandler {
    companion object {
        private object Holder {
            val mInstance = MyCrashHandler()
        }

        fun getInstance(): MyCrashHandler {
            return Holder.mInstance
        }
    }

    /**
     * 初始化
     *
     * @param context
     */
    fun init() {
        //设置该CrashHandler为程序的默认处理器
        Thread.setDefaultUncaughtExceptionHandler(this)
    }

    override fun uncaughtException(t: Thread?, ex: Throwable?) {
        val stackTrace = StringWriter()
        ex?.printStackTrace(PrintWriter(stackTrace))
        Log.e("xmf", stackTrace.toString())
        try {
            Process.killProcess(Process.myPid())
        } catch (e: Exception) {
            Log.e("xmf", e.toString())
        }
        try {
            System.exit(10)
        } catch (e: Exception) {
            Log.e("xmf", e.toString())
        }

    }
}