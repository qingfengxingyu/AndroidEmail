package com.team.cores.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.team.cores.R;
import com.team.cores.dialog.info.CallBackDialog;
import com.team.cores.theme.ThemeUtils;
import com.team.cores.views.NoDoubleClickListener;

/**
 * 创建人;sijia
 * 创建时间:2017/6/30  14:24
 * 用于创建带有进度条的对话框
 */

public class Update_Dialog extends Dialog {
    private TextView ye_btn;
    private TextView no_btn;
    private ProgressBar updateProgress;
    private LinearLayout jindu_layout;
    private TextView txt_down_size;
    private TextView msg_content;
    public TextView msg_title;
    public Context context;
    private CallBackDialog callBackDialog;
    private View line1;
    private TextView progress_text;
    private LinearLayout jsjj_layout;

    public View getLine2() {
        return line2;
    }

    public void setLine2(View line2) {
        this.line2 = line2;
    }

    private View line2;

    public void setCallBackDialog(CallBackDialog callBackDialog) {
        this.callBackDialog = callBackDialog;
    }

    /***
     * 设置按钮的字值
     *
     * @param yesText 确定
     */
    public void setOne_btn(String yesText) {
        ye_btn.setText(yesText);
        ye_btn.setBackgroundResource(R.drawable.update_dialog_btn_bottom_selector);
        no_btn.setVisibility(View.GONE);
        line2.setVisibility(View.GONE);
    }

    /***
     * 设置按钮的字值
     *
     * @param yesText 确定
     * @param noText  取消
     */
    public void setTwo_btn(String yesText, String noText) {
        ye_btn.setText(yesText);
        no_btn.setText(noText);
    }

    /***
     * 设置标题
     *
     * @param myTitile
     */
    public void setMyTitile(String myTitile) {
        msg_title.setText(myTitile);
    }

    /***
     * 设置提示信息
     *
     * @param myContent
     */
    public void setMyContent(String myContent) {
        if (null == myContent || "".equals(myContent)) {
            msg_content.setText("");
            return;
        }
        msg_content.setText(myContent);
    }

    public Update_Dialog(Context context) {
        super(context, R.style.teams_dialog);
        this.context = context;
        show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View v = getLayoutInflater().inflate(R.layout.update_apk_dialog, null);
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        setContentView(v, lp);
        this.getWindow().setBackgroundDrawable(new ColorDrawable());//解决5.0dialog背景黑框问题
        progress_text=(TextView)v.findViewById(R.id.progress_text);
        jsjj_layout=(LinearLayout)v.findViewById(R.id.jsjj_layout);
        ye_btn = (TextView) v.findViewById(R.id.ye_btn);
        no_btn = (TextView) v.findViewById(R.id.no_btn);
        updateProgress = (ProgressBar) v.findViewById(R.id.up_update_progress);
        msg_title = (TextView) v.findViewById(R.id.msg_title);
        jindu_layout = (LinearLayout) v.findViewById(R.id.up_jindu_layout);
        txt_down_size = (TextView) v.findViewById(R.id.up_txt_down_size);
        line1 = (View) v.findViewById(R.id.line1);
        line2 = (View) v.findViewById(R.id.line2);
        ThemeUtils.setTextViewText(msg_title);
        ThemeUtils.setTextViewBag(line1);
        ThemeUtils.setTextViewBag(line2);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
        msg_content = (TextView) v.findViewById(R.id.msg_content);
        ye_btn.setOnClickListener(new NoDoubleClickListener() {
            @Override
            public void onNoDoubleClick(View v) {
                if (null != callBackDialog) {
                    callBackDialog.yes_btn("");
                }
            }

        });
        no_btn.setOnClickListener(new NoDoubleClickListener() {
            @Override
            public void onNoDoubleClick(View v) {
                dismiss();
                if (null != callBackDialog) {
                    callBackDialog.no_btn("");
                }
            }

        });
    }

    public void showOneBtn() {
        no_btn.setBackgroundResource(R.drawable.update_dialog_btn_bottom_selector);
        ye_btn.setVisibility(View.GONE);
        line2.setVisibility(View.GONE);
    }

    public TextView getYe_btn() {
        return ye_btn;
    }

    public void setYe_btn(TextView ye_btn) {
        this.ye_btn = ye_btn;
    }

    public TextView getNo_btn() {
        return no_btn;
    }

    public TextView getMsg_content() {
        return msg_content;
    }//提示内容

    public ProgressBar getProgressBar() {
        return updateProgress;
    }

    public LinearLayout getJindu_layout() {
        return jindu_layout;
    }

    public void setJindu_layout(LinearLayout jindu_layout) {
        this.jindu_layout = jindu_layout;
    }

    public TextView getTxt_down_size() {//0M/0M
        return txt_down_size;
    }
    public TextView getProgress_text() {//下载中,请稍后
        return progress_text;
    }
    public void hideAllButton(){
        jsjj_layout.setVisibility(View.GONE);
    }
    public void setTxt_down_size(TextView txt_down_size) {
        this.txt_down_size = txt_down_size;
    }
}
