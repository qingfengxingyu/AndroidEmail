package com.xing.email.ui

import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.widget.AdapterView
import android.widget.CompoundButton
import com.xing.email.R
import com.xing.email.app.BaseUI
import com.xing.email.app.MyApp
import com.xing.email.utils.PortConfig
import com.xing.email.utils.Preference
import com.xing.email.utils.SPKeyConstants
import kotlinx.android.synthetic.main.ui_get_email_server.*


class GetEmailServerUI : BaseUI() {
    var positionType = 0;
    var account_type_value = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ui_get_email_server)
        initAll()
    }

    fun splitEmail(email: String): Array<String?> {
        val retParts = arrayOfNulls<String>(2)
        val emailParts = email.split("@".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        retParts[0] = if (emailParts.size > 0) emailParts[0] else ""
        retParts[1] = if (emailParts.size > 1) emailParts[1] else ""
        return retParts
    }

    override fun initData() {
        // 代码中得到dp值
        val pageMargin = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 40f, resources.displayMetrics).toInt()
        account_type.setDropDownVerticalOffset(pageMargin); //下拉的纵向偏移
    }

    override fun initEven() {
        var account_type_spinner = resources.getStringArray(R.array.account_type_spinner)
        account_type.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            //parent就是父控件spinner
            //view就是spinner内填充的textview,id=@android:id/text1
            //position是值所在数组的位置
            //id是值所在行的位置，一般来说与positin一致
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int,
                                        id: Long) {
                positionType = position
                account_type_value = account_type_spinner[position]
                server_name.text = account_type_value + "服务器" + " :"
                when (positionType) {
                    0 -> {
                        server_port.setText(PortConfig.imapPort)
                        server_dress.setText("imap." + splitEmail(email_sp)[1])
                    }
                    1 -> {
                        server_port.setText(PortConfig.popPort)
                        server_dress.setText("pop." + splitEmail(email_sp)[1])
                    }
                    2 -> {
                        server_port.setText(PortConfig.exchangePort)
                        server_dress.setText("exchange." + splitEmail(email_sp)[1])
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        })
        //绑定监听器
        server_ssl.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {

            override fun onCheckedChanged(arg0: CompoundButton, arg1: Boolean) {
                when (positionType) {
                    0 -> {
                        if (arg1) {
                            server_port.setText(PortConfig.imapPortSsl)
                        } else {
                            server_port.setText(PortConfig.imapPort)
                        }

                    }
                    1 -> {
                        if (arg1) {
                            server_port.setText(PortConfig.popPortSsl)
                        } else {
                            server_port.setText(PortConfig.popPort)
                        }

                    }
                    2 -> {
                        if (arg1) {
                            server_port.setText(PortConfig.exchangePortSsl)
                        } else {
                            server_port.setText(PortConfig.exchangePort)
                        }
                    }
                }
            }
        })
        back_btn.setOnClickListener {
            finish()
        }
        next_btn.setOnClickListener {
            MyApp.getInstance()?.closeOneActivity("NewBuildAccountUI")
        }
    }

}
