package com.team.cores.ui;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.team.cores.R;

/**
 * Created by xmf on 2017/06/12.
 */
public class ErrorActivity extends Activity {
    private TextView error_txt;
    private Button error_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error);
        String erro_msg = getIntent().getStringExtra("erro_msg");
        error_txt = (TextView) findViewById(R.id.error_txt);
        error_btn = (Button) findViewById(R.id.error_btn);
        error_txt.setMovementMethod(new ScrollingMovementMethod());
        error_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (null != error_txt && !"".equals(error_txt)) {
            error_txt.setText(erro_msg);
        } else {
            error_txt.setText("没有捕获到错误日志");
        }
    }

}
